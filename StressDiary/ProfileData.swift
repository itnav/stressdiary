//
//  ProfileData.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/15.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

/* 不使用のファイル */
class ProfileData: NSObject {
    
    var birth:NSDate!
    var sex:Int!
    var weight:Float!
    var height:Float!
    var area:Int!
    var baseHeartRate:Int!
    var remindFrequency:Int!
    var remindTime:Int!
//    var linkHealthKit:Bool!
//    var linkSNS:Bool!
    
}

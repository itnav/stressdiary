//
//  SettingViewController.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/10/28.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController,SettingListViewDelegate{
    
    var firstSettingFlag:Bool = false
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(firstSet:Bool) {
        super.init(nibName: nil, bundle: nil)
        
        firstSettingFlag = firstSet
        
        self.view.backgroundColor = UIColor.lightOrangeColor()
        self.tabBarItem.image = UIImage(named: "icon_menu_setting")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        self.tabBarItem.title = "設定"
    }
    
    private func setupNavigationBar() {
        // NavigationBarの下にViewが埋まってしまうことを回避
        edgesForExtendedLayout = UIRectEdge.None;
        title = "設定"
        self.navigationController?.title = "設定"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        
        let settingListView:SettingListView = SettingListView(frame: view.frame)
        settingListView.delegate = self
        
        if firstSettingFlag {
            settingListView.showOnlyProfileFlag = true
        }
        
        self.view.addSubview(settingListView)
    }
    
    func loadWebViewRequest(url: String, title: String, fileName: String) {
        /*
        let webVC:WebViewController = WebViewController()
        webVC.title = title
        webVC.setUrlString(url)
        self.navigationController?.pushViewController(webVC, animated: true)
        */
        let webVC:LocalWebViewController = LocalWebViewController()
        webVC.title = title
        webVC.fileName = fileName
        self.navigationController?.pushViewController(webVC, animated: true)
    }
    
    func setProfileDataRequest(data:UserProfileData) {
        UserProfile.updateUserProfile(data)
    }
    func pushSettingViewController(viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
    /*
    *  初回ログイン後設定終了時に呼び出されるデリゲート.
    */
    func completeFirstSetting() {
        print("completeFirstSetting")
        transitionToTutorialVC()
    }
    
    private func transitionToTutorialVC() {
        let tutorialVC = TutorialViewController()
        let tutorialNC = UINavigationController(rootViewController: tutorialVC)
        
        UITabBar.appearance().barTintColor = UIColor(white: 0.95, alpha: 1.0)
        UITabBar.appearance().tintColor = UIColor.strongOrangeColor()
        
        UINavigationBar.appearance().barTintColor = UIColor.navBarOrangeColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        
        UIApplication.sharedApplication().keyWindow?.rootViewController = tutorialNC
    }
}
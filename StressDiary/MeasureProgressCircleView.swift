//
//  MeasureProgressCircleView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/22.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

class AnimationSet {
    
    var layer:CAShapeLayer!
    var duration:NSTimeInterval!
    var repratCount:Float!
    var removedOnCompletion:Bool = false
    var identifier:String!
    var anim:CAKeyframeAnimation!
    
    var startValue:CGFloat = 0
    var lastValue:CGFloat = 0
    
    // アニメーションを何回使い回したかのカウント.
    var playCount:Int = 0
    
    init(layer:CAShapeLayer,duration:NSTimeInterval,count:Float,remove:Bool,identifier:String){
        self.layer = layer
        self.duration = duration
        self.repratCount = count
        self.removedOnCompletion = remove
        self.identifier = identifier
        self.anim = CAKeyframeAnimation(keyPath: "path")
    }
    
    func setAnimationValue(start start:CGFloat,end:CGFloat){
        self.startValue = start
        self.lastValue = end
    }
    func addPlayCount(){
        playCount++
    }
}

protocol MeasureProgressCircleDelegate:class {
    func completeProgress()
}
class MeasureProgressCircleView: UIView{
    
    @IBOutlet weak var progressLabel:UILabel!
    
    var delegate:MeasureProgressCircleDelegate?
    
    let mainLayer:CAShapeLayer = CAShapeLayer()
    let subLayer:CAShapeLayer = CAShapeLayer()
    let subLayer2:CAShapeLayer = CAShapeLayer()
    
    var mainSet:AnimationSet!
    var subSet:AnimationSet!
    var subSet2:AnimationSet!
    
    var timer:NSTimer!
    
    var count:Double = 0
    
    // 測定が終了するであろう目安時間.
    var completeTime:Float = 0.0
    // 100%の前でstopしたように見せる値.
    var stopValue:Float = 0.0
    
    // プログレスがstopする前に計測が終了してしまった場合を保存しておくためのBool.
    var completeMeasureFlag:Bool = false
    var stopProgressFlag:Bool = false
    
    // タイマーの秒数.
    let sec:NSTimeInterval = 0.3
    
    var stopTime:[Float] = [2.0,4.0,6.0]
    
    var anim_center:CGPoint!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        completeTime = getRandomNumber(Min: 8.0, Max: 10.0)
        stopValue = getRandomNumber(Min: 85.0, Max: 95.0)
    }
    
    override func awakeFromNib() {
        anim_center = CGPointMake(UIScreen.mainScreen().bounds.width/2,UIScreen.mainScreen().bounds.width/2)
        
        progressLabel.font = UIFont.meterDisplayFont(30)
        
        let attr = NSMutableAttributedString(attributedString: NSAttributedString.decoratedOrangeAttributedString(0))
        attr.appendAttributedString(getPercentText())
        progressLabel.attributedText = attr
        
        timer = NSTimer.scheduledTimerWithTimeInterval(sec, target: self, selector: Selector("updateTimer"), userInfo: nil, repeats: true)

        mainSet = AnimationSet(layer: mainLayer, duration: NSTimeInterval(completeTime), count: 1, remove: false, identifier:"main")
        
        mainSet.setAnimationValue(start: 0.0, end: CGFloat(360.0*(stopValue/100)))
        
        subSet = AnimationSet(layer: subLayer, duration: NSTimeInterval(completeTime/5), count: 1, remove: false, identifier:"sub")
        subSet.setAnimationValue(start: 0, end: 360)
        
        subSet2 = AnimationSet(layer: subLayer2, duration: 1.5, count: 1, remove: false, identifier: "sub2")
        subSet2.setAnimationValue(start: 0, end: 360)
        
        addCircleProgress(mainSet, radius: 120,linewidth:20.0,center: anim_center, color: UIColor.strongOrangeColor())
        addCircleProgress(subSet, radius: 100,linewidth:20.0,center: anim_center, color: UIColor.lightOrangeColor())
        addCircleProgress(subSet2, radius: 90, linewidth:1,center: anim_center, color: UIColor.strongOrangeColor())
        
    }
    
    func updateTimer(){
        count++
        let attr = NSMutableAttributedString()
        let newCount:Float = Float(stopValue/completeTime)*Float(count*sec)
        
        if !completeMeasureFlag {
            // まだ測定が終わっていない時は、stopValueをこしたら一度止めておく.
            if newCount > stopValue {
                timer.invalidate()
            }
        }else if !stopProgressFlag{
            // 測定が終わっていて、プログレスが終わっていない場合は100までカウントして超えたら止める.
            if newCount >= 100 {
                timer.invalidate()
            }
        }
        
        if newCount <= 100 {
            attr.appendAttributedString(NSAttributedString.decoratedOrangeAttributedString(newCount))
            attr.appendAttributedString(getPercentText())
            progressLabel.attributedText = attr
        }
    }
    // 計測終了時に呼び出す関数.
    internal func completeMeasure() {
        completeMeasureFlag = true
        // プログレスがすでに止まっている場合、アニメーションをこちらから動かす。
        if stopProgressFlag {
            lastUpdateProgress()
        }
    }
    func getRandomNumber(Min _Min : Float, Max _Max : Float)->Float {
        
        return ( Float(arc4random_uniform(UINT32_MAX)) / Float(UINT32_MAX) ) * (_Max - _Min) + _Min
    }
    func addCircleProgress(animSet:AnimationSet,radius:CGFloat,linewidth:CGFloat,center:CGPoint,color:UIColor){
        
        let keynum:Double = 100
        
        var keys:[CGPathRef] = []
        
        for var i:Double=0; i<keynum; i++ {
            keys.append(UIBezierPath(arcCenter:center, radius: radius, startAngle: -90.0*CGFloat(M_PI/180.0), endAngle: (animSet.startValue+CGFloat(i)*(animSet.lastValue-animSet.startValue)/CGFloat(keynum)-90.0)*CGFloat(M_PI/180.0), clockwise: true).CGPath)
        }
        keys.append(UIBezierPath(arcCenter: center, radius: radius, startAngle:  -90.0*CGFloat(M_PI/180.0), endAngle: (animSet.lastValue-90.0)*CGFloat(M_PI/180.0), clockwise: true).CGPath)
        
        animSet.anim.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        animSet.anim.values = keys
        
        animSet.anim.duration = animSet.duration
        animSet.anim.removedOnCompletion = animSet.removedOnCompletion
        animSet.anim.repeatCount = animSet.repratCount
        animSet.anim.fillMode = kCAFillModeForwards
        
        animSet.anim.delegate = self
        
        animSet.layer.lineWidth = linewidth
        animSet.layer.fillColor = UIColor.clearColor().CGColor
        animSet.layer.strokeColor = color.CGColor
        
        animSet.addPlayCount()
        
        animSet.layer.addAnimation(animSet.anim, forKey:animSet.identifier)
        self.layer.addSublayer(animSet.layer)
    }
    
    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        if anim == mainLayer.animationForKey("main") {
            stopProgressFlag = true
            if completeMeasureFlag {
                // アニメーションが終わる時点で測定が終了してしまっていたら、残りも動かす.
                lastUpdateProgress()
            }
        }else if anim == subLayer.animationForKey("sub"){
            if subSet.playCount < 5 {
                if subSet.playCount == 4 {
                    subSet.lastValue = CGFloat(360.0*(stopValue/100))/5
                    addCircleProgress(subSet, radius: 100, linewidth:20, center:anim_center ,color: UIColor.lightOrangeColor())
                }else {
                    addCircleProgress(subSet, radius: 100,linewidth:20, center:anim_center ,color: UIColor.lightOrangeColor())
                }
            }
        }else if anim == subLayer2.animationForKey("sub2"){
            addCircleProgress(subSet2, radius: 90, linewidth:1,center: anim_center, color: UIColor.strongOrangeColor())
        }else if anim == mainLayer.animationForKey("last"){
            let attr = NSMutableAttributedString()
            attr.appendAttributedString(NSAttributedString.decoratedOrangeAttributedString(100))
            attr.appendAttributedString(getPercentText())
            progressLabel.attributedText = attr
            
            delegate?.completeProgress()
        }
    }
    func lastUpdateProgress(){
        
        mainSet.setAnimationValue(start: CGFloat(360.0*(stopValue/100)), end: 360)
        mainSet.duration = 3.0
        mainSet.identifier = "last"
        addCircleProgress(mainSet, radius: 120,linewidth:20.0,center: anim_center, color: UIColor.strongOrangeColor())
        
        subSet.setAnimationValue(start: CGFloat(360.0*(stopValue/100))/5, end: 360)
        subSet.duration = 3.0
        subSet.identifier = "last"
        addCircleProgress(subSet, radius: 100,linewidth:20, center:anim_center ,color: UIColor.lightOrangeColor())
        timer = NSTimer.scheduledTimerWithTimeInterval(sec, target: self, selector: Selector("updateTimer"), userInfo: nil, repeats: true)
    }
    private func getPercentText()->NSAttributedString{
        
        let attr = [NSForegroundColorAttributeName:UIColor.brownColor()]
        
        return NSAttributedString(string: "%", attributes: attr)
    }
}

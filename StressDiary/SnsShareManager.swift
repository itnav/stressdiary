//
//  SnsShareManager.swift
//  StressDiary
//
//  Created by derushio on 2016/09/06.
//  Copyright © 2016年 itnav. All rights reserved.
//

import UIKit
import Social

class SnsShareManager: NSObject {
    
    /**
     * Twitterに投稿
     **/
    static internal func postToTwitter(parentView:UIViewController, msg:String!, image:UIImage!) {
        let vc: SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        if (msg != nil) {
            vc.setInitialText(msg!);
        }
        if (image != nil){
            vc.addImage(image!);
        }
        
        parentView.presentViewController(vc, animated:true, completion:nil)
    }
    
    /**
     * Facebookに投稿
     **/
    static internal func postToFacebook(parentView:UIViewController, msg:String!, image:UIImage!) {
        let vc: SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        if (msg != nil) {
            vc.setInitialText(msg!);
        }
        if (image != nil){
            vc.addImage(image!);
        }
        
        parentView.presentViewController(vc, animated:true, completion:nil)
    }
}

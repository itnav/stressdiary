//
//  HeartBeatLog.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/18.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import SVProgressHUD

/* 心拍データ関連の実処理
詳しくはWeb API資料を参照。
*/
struct HeartBeatLog {
    
    static func Add(new_data:HeartbeatLogData) -> Promise<Bool>{
        return Promise { fulfill, reject in
            let request = Alamofire.request(HeartBeatLogRouter.Add(data: new_data))
            request.response { request, response, data, error in
                print("heartbeatLog.add")
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                
                if error != nil {
                    ErrorProcess.communicationError()
                    fulfill(false)
                }else {
                    
                    do {
                        let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                        //json.setValue(nil, forKey: "error")
                        
                        let _error = json.objectForKey("error") as? NSDictionary
                        
                        if _error?.objectForKey("message") != nil {
                            ErrorProcess.serverError()
                            return
                        }else {
                            
                            // もしも更新しようとしたデータが登録されている最新のデータよりも新しかったら登録しておく.
                            if let log = BaseHeartRateManager.getLatestHeartRate() {
                                if log.measureDatetime.compare(new_data.measureDatetime) == NSComparisonResult.OrderedAscending {
                                    BaseHeartRateManager.setLatestHeartRate(new_data)
                                }
                            }
                            fulfill(true)
                        }
                    }
                }
            }
        }
    }
    
    static func AddAll(datas:[[String: AnyObject]]) -> Promise<Bool> {
        return Promise { fulfill, reject in
            let request = Alamofire.request(HeartBeatLogRouter.AddAll(datas: datas))
            request.response { request, response, data, error in
                print("heartbeatLog.addAll")
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                
                if error != nil {
                    ErrorProcess.communicationError()
                    fulfill(false)
                }else {
                    
                    do {
                        let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                        //json.setValue(nil, forKey: "error")
                        
                        let _error = json.objectForKey("error") as? NSDictionary
                        
                        if _error?.objectForKey("message") != nil {
                            ErrorProcess.serverError()
                            return
                        }else {
                            fulfill(true)
                        }
                    }
                }
            }
        }
    }
    
    static func Delete(id:Int) -> Promise<Bool>{
        
        return Promise { fulfill, reject in
            let request = Alamofire.request(HeartBeatLogRouter.Delete(id: id))
            request.response { request, response, data, error in
                print("heartbeatLog.delete")
                //                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                
                if error != nil {
                    ErrorProcess.communicationError()
                    fulfill(false)
                }else {
                    do {
                        let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                        //json.setValue(nil, forKey: "error")
                        
                        let _error = json.objectForKey("error") as? NSDictionary
                        
                        if _error?.objectForKey("message") != nil {
                            ErrorProcess.serverError()
                            return
                        }else {
                            fulfill(true)
                        }
                    }
                }
            }
        }
    }
    
    static func UpdateTags(id:Int, tags:[String]) -> Promise<Bool>{
        
        return Promise { fulfill, reject in
            let request = Alamofire.request(HeartBeatLogRouter.UpdateTags(id: id, tags: tags))
            request.response { request, response, data, error in
                print("heartbeatLog.updateTags")
                //                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                
                if error != nil {
                    ErrorProcess.communicationError()
                    fulfill(false)
                }else {
                    
                    do {
                        let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                        //json.setValue(nil, forKey: "error")
                        
                        let _error = json.objectForKey("error") as? NSDictionary
                        
                        if _error?.objectForKey("message") != nil {
                            ErrorProcess.serverError()
                            return
                        }else {
                            fulfill(true)
                        }
                    }
                }
            }
        }
    }
    
    static func UpdateMemo(id:Int, memo:String) -> Promise<Bool>{
        
        return Promise { fulfill, reject in
            let request = Alamofire.request(HeartBeatLogRouter.UpdateMemo(id: id, memo: memo))
            request.response { request, response, data, error in
                print("heartbeatLog.updateMemo")
                //                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                
                if error != nil {
                    ErrorProcess.communicationError()
                    fulfill(false)
                }else {
                    
                    do {
                        let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                        //json.setValue(nil, forKey: "error")
                        
                        let _error = json.objectForKey("error") as? NSDictionary
                        
                        if _error?.objectForKey("message") != nil {
                            ErrorProcess.serverError()
                            return
                        }else {
                            fulfill(true)
                        }
                    }
                }
            }
        }
    }
    
    static func Find(id:Int) -> Promise<HeartbeatLogData>{
        
        return Promise { fulfill, reject in
            let heartbeatLogData:HeartbeatLogData = HeartbeatLogData()
            let request = Alamofire.request(HeartBeatLogRouter.Find(id:id))
            request.response { request, response, data, error in
                print("heartbeatLog.find")
                //                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                
                if let _ = error {
                    ErrorProcess.communicationError()
                    reject(CommunicationError.NoCommunicationLine)
                    return
                }
                
                
                do {
                    let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                    //json.setValue(nil, forKey: "error")
                    
                    let _error = json.objectForKey("error") as? NSDictionary
                    
                    if _error?.objectForKey("message") != nil {
                        ErrorProcess.serverError()
                        return
                    }else {
                        if let result = json.objectForKey("result") as? NSDictionary {
                            heartbeatLogData.setHeartbeatLog(result)
                        }
                    }
                }
                fulfill(heartbeatLogData)
            }
        }
    }
    
    static func FindLast() -> Promise<HeartbeatLogData> {
        return Promise { fulfill, reject in
            
            var resultData:HeartbeatLogData = HeartbeatLogData(date: NSDate(), bpm: 60, tags: [], memo: "")
            let request = Alamofire.request(HeartBeatLogRouter.FindLast())
            request.response { request, response, data, error in
                print("heartbeatLog.findLast")
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                
                if let _ = error {
                    ErrorProcess.communicationError()
                    reject(CommunicationError.NoCommunicationLine)
                    return
                }
                
                
                do {
                    let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                    print("AAAJSON: \(json.description)")
                    //json.setValue(nil, forKey: "error")
                    
                    let _error = json.objectForKey("error") as? NSDictionary
                    
                    if _error?.objectForKey("message") != nil {
                        ErrorProcess.serverError()
                        return
                    }else {
                        if let result = json.objectForKey("result") as? NSDictionary {
                            let interval:NSTimeInterval = result.objectForKey("measureDatetime") as! NSTimeInterval
                            resultData.measureDatetime = convertAPIDateToNSDate(interval)
                            resultData.rateBpm = result.objectForKey("rateBpm") as! Int
                            resultData.stressIndex = result.objectForKey("stressIndex") as! Double
                        }
                    }
                }
                print("resultData = \(resultData)")
                fulfill(resultData)
            }
        }
    }
    
    static func FindBy(from:NSDate?,to:NSDate?,tags:[String]) -> Promise<SearchResultData> {
        
        return Promise { fulfill, reject in
            
            let resultData:SearchResultData = SearchResultData()
            let request = Alamofire.request(HeartBeatLogRouter.FindBy(from: from!, to: to!, tags: tags))
            request.response { request, response, data, error in
                print("heartbeatLog.findBy")
                //                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                
                if let _ = error {
                    ErrorProcess.communicationError()
                    reject(CommunicationError.NoCommunicationLine)
                    return
                }
                
                do {
                    let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                    print("JSON: \(json)")
                    print("from: \(from!.description) to: \(to!.description)")
                    //json.setValue(nil, forKey: "error")
                    
                    let _error = json.objectForKey("error") as? NSDictionary
                    
                    if _error?.objectForKey("message") != nil {
                        ErrorProcess.serverError()
                        return
                    }else {
                        if let result = json.objectForKey("result") as? NSDictionary {
                            resultData.setSearchInfoData(result)
                        }
                    }
                }
                fulfill(resultData)
            }
        }
    }
    static func FindAll() -> Promise<SearchResultData>{
        
        return Promise { fulfill, reject in
            
            let resultData:SearchResultData = SearchResultData()
            let request = Alamofire.request(HeartBeatLogRouter.FindAll())
            
            request.response { request, response, data, error in
                print("heartbeatLog.findBy(All)")
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                
                if let _ = error {
                    ErrorProcess.communicationError()
                    reject(CommunicationError.NoCommunicationLine)
                    return
                }
                
                do {
                    let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                    //json.setValue(nil, forKey: "error")
                    
                    let _error = json.objectForKey("error") as? NSDictionary
                    
                    if _error?.objectForKey("message") != nil {
                        ErrorProcess.serverError()
                        return
                    }else {
                        if let result = json.objectForKey("result") as? NSDictionary {
                            resultData.setSearchInfoData(result)
                        }
                    }
                }
                fulfill(resultData)
            }
        }
    }
    
    static func FindByCursor(cursor:String,filter:String,sorts:String)->Promise<SearchResultData>{
        
        return Promise { fulfill, reject in
            
            let resultData:SearchResultData = SearchResultData()
            let request = Alamofire.request(HeartBeatLogRouter.FindByCursor(cursor: cursor, filter: filter, sorts: sorts))
            request.response { request, response, data, error in
                print("heartbeatLog.findByCursor")
                //                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                
                if let _ = error {
                    ErrorProcess.communicationError()
                    reject(CommunicationError.NoCommunicationLine)
                    return
                }
                
                do {
                    let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                    //json.setValue(nil, forKey: "error")
                    
                    let _error = json.objectForKey("error") as? NSDictionary
                    
                    if _error?.objectForKey("message") != nil {
                        ErrorProcess.serverError()
                        return
                    }else {
                        if let result = json.objectForKey("result") as? NSDictionary {
                            resultData.setSearchInfoData(result)
                        }
                    }
                    fulfill(resultData)
                }
            }
        }
    }
}

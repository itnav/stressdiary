//
//  WebViewController.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/19.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

class WebViewController: UIViewController {
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(){
        super.init(nibName: nil, bundle: nil)
    }

    internal func setUrlString(url:String){
        (self.view as! WebView).url = url
        (self.view as! WebView).webview.loadRequest(NSURLRequest(URL: NSURL(string: url)!))
    }
    
    
}

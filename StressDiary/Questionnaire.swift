//
//  Questionnaire.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/19.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import SVProgressHUD

/* アンケートの実処理 */
struct Questionnaire {
    static func FindRandomOne()->Promise<QuestionnaireData>{
        
        return Promise { fulfill, reject in
            let questionnaireData:QuestionnaireData = QuestionnaireData()
            let request = Alamofire.request(QuestionnaireRouter.FindRandomOne())
            request.response { request, response, data, error in
                print("questionnaire.findRandomOne")
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                
                if let _ = error {
                    ErrorProcess.communicationError()
                    reject(CommunicationError.NoCommunicationLine)
                }
                
                do {
                    let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                    
                    if json.objectForKey("error") as? String == nil {
                        if let result = json.objectForKey("result") as? NSDictionary {
                            questionnaireData.setQuestionnaireData(result)
                        }
                    }
                    fulfill(questionnaireData)
                }
            }
        }
    }
    
    static func Answer(id:Int,answerNo:Int) {
        let request = Alamofire.request(QuestionnaireRouter.Answer(id: id, answerNo: answerNo))
        request.response { request, response, data, error in
            print("questionnaire.answer")
            print(NSString(data: data!, encoding: NSUTF8StringEncoding))
            
            if let _ = error {
                ErrorProcess.communicationError()
            }
        }
    }

}

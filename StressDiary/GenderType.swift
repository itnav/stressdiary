//
//  GenderType.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/19.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation

enum GenderType{
    
    case male
    case female
    case unknown
    
    func getGenderKey()->String{
        switch self {
        case .male:
            return "MALE"
        case .female:
            return "FEMALE"
        case .unknown:
            return "UNKNOWN"
        }
    }
    
    func getGenderLabel()->String {
        switch self {
        case .male:
            return "男性"
        case .female:
            return "女性"
        case .unknown:
            return "不明"
        }
    }
}

internal func getGenderType(gender:String)->GenderType {
    switch gender {
    case "男性","MALE":
        return GenderType.male
    case "女性","FEMALE":
        return GenderType.female
    case "不明","UNKNOWN":
        return GenderType.unknown
    default:
        return GenderType.unknown
    }
}
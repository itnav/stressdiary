//
//  HelpSettingHealthKitViewController.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/16.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

class HelpSettingHealthKitViewController: UIViewController {
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        setupNavigationBar()
    }
    
    // MARK: -setup
    private func setupNavigationBar() {
        // NavigationBarの下にViewが埋まってしまうことを回避
        edgesForExtendedLayout = UIRectEdge.None;
        
        title = "ヘルスケア"
    }
    
}
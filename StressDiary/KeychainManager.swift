//
//  UserDefaultsManager.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/11/11.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import KeychainAccess

/* Keychainの管理
ログインIDの管理をしている。
*/
struct KeychainManager {
    // MARK: -
    // MARK: -setter
    static func setOriginalID(originalID: String) {
        let keychain = Keychain(service: "com.StressDiary")
        keychain["OriginalID"] = originalID
    }
    
    static func setFacebookID(facebookID: String) {
        let keychain = Keychain(service: "com.StressDiary")
        keychain["FacebookID"] = facebookID
    }
    
    static func setTwitterID(twitterID: String) {
        let keychain = Keychain(service: "com.StressDiary")
        keychain["TwitterID"] = twitterID
    }
    
    static func setCurrentAuthType(currentAuthType: String) {
        let keychain = Keychain(service: "com.StressDiary")
        keychain["CurrentAuthType"] = currentAuthType
    }
    
    static func setCurrentAuthID(currentAuthID: String) {
        let keychain = Keychain(service: "com.StressDiary")
        keychain["CurrentAuthID"] = currentAuthID
    }
    
    // MARK: -
    // MARK: -getter
    static func getOriginalID() -> String? {
        let keychain = Keychain(service: "com.StressDiary")
        return keychain["OriginalID"]
    }
    
    static func getFacebookID() -> String? {
        let keychain = Keychain(service: "com.StressDiary")
        return keychain["FacebookID"]
    }
    
    static func getTwitterID() -> String? {
        let keychain = Keychain(service: "com.StressDiary")
        return keychain["TwitterID"]
    }
    
    static func getCurrentAuthType() -> String? {
        let keychain = Keychain(service: "com.StressDiary")
        return keychain["CurrentAuthType"]
    }
    
    static func getCurrentAuthID() -> String? {
        let keychain = Keychain(service: "com.StressDiary")
        return keychain["CurrentAuthID"]
    }
    
    // MARK: -
    // MARK: -remover
    static func removeOriginalID() {
        let keychain = Keychain(service: "com.StressDiary")
        keychain["OriginalID"] = nil
    }
    
    static func removeFacebookID() {
        let keychain = Keychain(service: "com.StressDiary")
        keychain["FacebookID"] = nil
    }
    
    static func removeTwitterID() {
        let keychain = Keychain(service: "com.StressDiary")
        keychain["TwitterID"] = nil
    }
    
    static func removeCurrentAuthType() {
        let keychain = Keychain(service: "com.StressDiary")
        keychain["CurrentAuthType"] = nil
    }
    
    static func removeAllID() {
        let keychain = Keychain(service: "com.StressDiary")
        keychain["OriginalID"] = nil
        keychain["FacebookID"] = nil
        keychain["TwitterID"] = nil
        keychain["CurrentAuthType"] = nil
    }
}
//
//  HistoryViewCell.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/11/20.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit

class HistoryViewCell: UITableViewCell {
    
    @IBOutlet weak var marginLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var heartRateLabel: UILabel!
    @IBOutlet weak var stressDegreeLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    var heartbeatLogData: HeartbeatLogData!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

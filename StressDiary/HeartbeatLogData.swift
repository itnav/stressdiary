//
//  HeartbeatLogModel.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/18.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import HealthKit

/* 心拍データのデータモデル */
class HeartbeatLogData {
    
    internal var id:Int!
    internal var measureDatetime:NSDate!
    internal var rateBpm:Int!
    internal var tags:[String]!
    internal var memo:String!
    internal var stressIndex:Double!
    
    var datas: [HeartbeatLogData]?
    
    /*
    * データの初期化処理.
    */
    init(){
        self.id = 0
        self.measureDatetime = NSDate()
        self.tags = []
        self.memo = ""
        self.rateBpm = 0
        self.stressIndex = 0.0
    }
    
    init(date:NSDate,bpm:Int,tags:[String],memo:String){
        self.id = 0
        self.measureDatetime = date
        self.tags = tags
        self.memo = memo
        self.rateBpm = bpm
        self.stressIndex = HeartbeatLogData.calculationStressindex(rateBpm)
    }
    
    convenience init(sample: HKQuantitySample?) {
        self.init()
        if let _ = sample{
            self.measureDatetime = sample!.startDate
            self.rateBpm = sample!.heartRate()
        }else{
            self.measureDatetime = NSDate()
            self.rateBpm = 0
        }
        
        self.stressIndex = HeartbeatLogData.calculationStressindex(self.rateBpm)
    }
    
    internal func setHeartbeatLog(data:NSDictionary){
        self.id = data.objectForKey("id") as! Int
        
        let interval:NSTimeInterval = data.objectForKey("measureDatetime") as! NSTimeInterval
        self.measureDatetime = convertAPIDateToNSDate(interval)
        self.rateBpm = data.objectForKey("rateBpm") as! Int
        self.stressIndex = data.objectForKey("stressIndex") as! Double
        self.tags = data.objectForKey("tags") as! [String]
        self.memo = data.objectForKey("memo") as! String
    }

    static func calculationStressindex(rateBpm: Int)->Double{
        let baseBpm:Int = BaseHeartRateManager.getBaseHeartRate()!
        
        // もし基本心拍数が0で返ってくると計算できないので、0を返す
        if baseBpm == 0 {
            return 0.0
        } else {
            let max = Double(baseBpm) * 0.2 // calc number of bpm when stress index is 100pt
            var index = Double(rateBpm - baseBpm) / max
            if index < 0 {
                index = 0
            }
            return index * 100 // convert to percent
        }
    }
}

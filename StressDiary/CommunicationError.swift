//
//  CommunicationError.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/12/01.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation

/* 通信エラー */
enum CommunicationError: ErrorType {
    case NoCommunicationLine
    case InvalidJSON
}

//noFacebookAccountError
//noTwitterAccountError
//communicationError
//fbAuthenticationError
//twitterAuthenticationError
//fbSocialAuthenticationError
//noLineAppError
//detachError
//accountDuplicationError
//
//  QuestionnaireRouter.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/19.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import Alamofire

/* アンケートのルーター */
enum QuestionnaireRouter: URLRequestConvertible {
    
    case FindRandomOne()
    case Answer(id:Int,answerNo:Int)
    
    private var URLString: String {
        return "http://stress-diary.appspot.com/rpc.json"
    }
    
    private var HTTPBody: NSData? {
        switch self {
        case .FindRandomOne():
            let values = [
                "method": "questionnaire.findRandomOne",
                "id": 1,
                "params":[[]
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
        
        case .Answer(let id,let answerNo):
            let values = [
                "method": "questionnaire.answer",
                "id": 1,
                "params":[
                    [ id ],[ answerNo ]
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
        }
    }
    
    // TODO: HeaderはUserDefaults管理。currentAuthIDからとってくる！
    private var HTTPHeaderValue: String? {
        switch self {
        default:
            let currentAuthType = KeychainManager.getCurrentAuthType()!
            var currentAuthID: String = ""
            
            if currentAuthType == "DEVICE" {
                currentAuthID = KeychainManager.getOriginalID()!
            } else if currentAuthType == "FACEBOOK" {
                currentAuthID = KeychainManager.getFacebookID()!
            } else if currentAuthType == "TWITTER" {
                currentAuthID = KeychainManager.getTwitterID()!
            }
            
            let values = [
                "authType": currentAuthType,
                "id": currentAuthID,
                "authHash": "H2mRVD7C\(currentAuthType)Adpy_jEA\(currentAuthID)bgjsewn2".sha256String().sha256String().sha256String()
            ]
            let JSON = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
            return String(data: JSON, encoding: NSUTF8StringEncoding)
        }
    }
    
    var URLRequest: NSMutableURLRequest {
        let mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: URLString)!)
        mutableURLRequest.HTTPMethod = Alamofire.Method.POST.rawValue
        mutableURLRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        mutableURLRequest.HTTPBody = HTTPBody
        
        if let header = HTTPHeaderValue {
            mutableURLRequest.addValue(header, forHTTPHeaderField: "X-Stress-Diary-Authentication")
        }
        
        return mutableURLRequest
    }
}

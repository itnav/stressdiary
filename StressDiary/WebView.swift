//
//  WebView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/19.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

class WebView: UIView {
    
    @IBOutlet weak var webview:UIWebView!
    
    internal var url:String = ""
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }
    override func awakeFromNib() {
        webview.loadRequest(NSURLRequest(URL: NSURL(string: url)!))
    }
}
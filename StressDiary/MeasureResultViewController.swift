//
//  MeasureResultViewController.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/12.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit
import Social
import SVProgressHUD
import WatchConnectivity

class MeasureResultViewController: UIViewController, WCSessionDelegate {
    
    var heartRateData:HeartbeatLogData = HeartbeatLogData()
    var historyData:HeartbeatLogData!
    var tags:[String] = []
    var memo:String = ""
    var isSaveSucceeded: Bool = false
    var needStressCalc = true;
    
    @IBOutlet weak var heartRateResultLabel: UILabel!
    @IBOutlet weak var stressDegreeLabel: UILabel!
    @IBOutlet weak var stressMeterView:StressMeterView!
    @IBOutlet weak var adviceMessageLabel:UILabel!
    @IBOutlet weak var dateTimeLabel:UILabel!
    @IBOutlet weak var addTagButton:UIButton!
    @IBOutlet weak var addMemoButton:UIButton!
    @IBOutlet weak var smallAddTagButton:UIButton!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.view.backgroundColor = UIColor.whiteColor()
    }
    
    private func setupNavigationBar() {
        // NavigationBarの下にViewが埋まってしまうことを回避
        edgesForExtendedLayout = UIRectEdge.None;
        title = "計測結果"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        stressMeterView.meterImage.image = UIImage(named: "meter01")
        
        setupNavigationBar()
        
        let saveButton = UIBarButtonItem(title: "保存", style: .Plain, target: self, action: "saveMeasureResult")
        saveButton.tintColor = UIColor.blackColor()
        navigationItem.rightBarButtonItem = saveButton
        
        // アドバイスを取得.
        AdviceMessage.FindByStressIndex(HeartbeatLogData.calculationStressindex(heartRateData.rateBpm)).then { data in
            self.setAdviceMessage(data)
        }
        
        if (WCSession.isSupported()) {
            let session = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
        }
        
        //ボタンの角調整
        self.addTagButton.layer.cornerRadius = 10.0
        self.addMemoButton.layer.cornerRadius = 10.0
        self.smallAddTagButton.layer.borderColor = UIColor.strongOrangeColor().CGColor
        self.smallAddTagButton.layer.borderWidth = 2
        self.smallAddTagButton.layer.cornerRadius = 15.0
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let index:Int = (self.navigationController?.viewControllers.count)!-2
        
        if let _:HistoryViewController = self.navigationController?.viewControllers[index] as? HistoryViewController {
            navigationItem.setHidesBackButton(false, animated: false)
        }else {
            navigationItem.setHidesBackButton(true, animated: false)
        }
        
    }
    
    func setAdviceMessage(advice:AdviceMessageData){
        adviceMessageLabel.text = advice.message
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        // validate stress index value
        
        if(historyData == nil){
            var stress = HeartbeatLogData.calculationStressindex(heartRateData.rateBpm)
            if(stress > 100) {
                stress = 100
            } else if (stress < 0) {
                stress = 0
            }
            heartRateData.stressIndex = stress
            heartRateResultLabel.attributedText = NSAttributedString.decoratedLightGrayAttributedString(Float(heartRateData.rateBpm))
            stressDegreeLabel.attributedText = NSAttributedString.decoratedOrangeAttributedString(Float(stress))
        } else {
            heartRateResultLabel.attributedText = NSAttributedString.decoratedLightGrayAttributedString(Float(historyData.rateBpm))
            stressDegreeLabel.attributedText = NSAttributedString.decoratedOrangeAttributedString(Float(Int(historyData.stressIndex)))
        }
        
        
    }
    
    internal func setHeartRateResult(rateBpm:Int,date:NSDate){
        heartRateData.rateBpm = rateBpm
        heartRateData.measureDatetime = date
        print("setHeartRateResult.date = \(date)")
        print("heartRateData: \(heartRateData)")
        
        let dateformat:NSDateFormatter = NSDateFormatter()
        dateformat.locale = NSLocale(localeIdentifier: "en")
        dateformat.dateFormat = "MM/dd EEE HH:mm"
        self.dateTimeLabel.text = "\(dateformat.stringFromDate(date).uppercaseString)"
        
        //メーター値を設定
        if(historyData == nil){
            stressMeterView.setMeterValue(HeartbeatLogData.calculationStressindex(heartRateData.rateBpm), animation: true, duration: 2.0)
        
            // アドバイスを取得.
            AdviceMessage.FindByStressIndex(HeartbeatLogData.calculationStressindex(heartRateData.rateBpm)).then { data in
                self.setAdviceMessage(data)
            }
        } else {
            stressMeterView.setMeterValue(historyData.stressIndex, animation: true, duration: 2.0)
            
            // アドバイスを取得.
            AdviceMessage.FindByStressIndex(historyData.stressIndex).then { data in
                self.setAdviceMessage(data)
            }
        }
    }
    
    internal func setHeartRateData(data:HeartbeatLogData){
        
        let _data = data
        
        /*if _data.stressIndex > 100 {
            _data.stressIndex = 100
        } else*/ if _data.stressIndex < 0 {
            _data.stressIndex = 0
        }
        
        heartRateData = _data
        historyData = _data
        setHeartRateResult(_data.rateBpm, date:_data.measureDatetime)
        self.memo = _data.memo
        self.tags = _data.tags
        
        let dateformat:NSDateFormatter = NSDateFormatter()
        dateformat.locale = NSLocale(localeIdentifier: "en")
        dateformat.dateFormat = "MM/dd EEE HH:mm"
        self.dateTimeLabel.text = "\(dateformat.stringFromDate(data.measureDatetime).uppercaseString)"
    }
    
    /*
    * タグ追加ボタン選択時のデリゲート.
    */
    @IBAction func goAddTagView() {
        let selectTagVC:SelectTagConditionViewController = SelectTagConditionViewController()
        selectTagVC.setSelectedTag(tags)
        self.navigationController?.pushViewController(selectTagVC, animated: true)
    }
    func setTagCondition(data:[String]){
        heartRateData.tags = data
        tags = data
    }
    
    /*
    * メモ追加ボタン選択時のデリゲート.
    */
    @IBAction func goAddMemoView() {
        let editMemoViewController:EditMemoViewController = EditMemoViewController()
        editMemoViewController.setMemoText(memo)
        self.navigationController?.pushViewController(editMemoViewController, animated: true)
    }
    
    @IBAction func tappedFBButton(sender: AnyObject) {
        SnsShareManager.postToFacebook(self, msg:"【ストレス指数 \(stressDegreeLabel.text!)pt】\n\(adviceMessageLabel.text!)", image:nil)
    }
    
    @IBAction func tappedTwitterButton(sender: AnyObject) {
        SnsShareManager.postToTwitter(self, msg:"【ストレス指数 \(stressDegreeLabel.text!)pt】\n\(adviceMessageLabel.text!)", image:nil)
    }
    
    @IBAction func tappedLineButton(sender: AnyObject) {
        let postText = "【ストレス指数 \(stressDegreeLabel.text!)pt】\n\(adviceMessageLabel.text!)"
        let url = "line://msg/text/\(postText.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet())!)"
        if UIApplication.sharedApplication().canOpenURL(NSURL(string: url)!) == true {
            UIApplication.sharedApplication().openURL(NSURL(string: url)!)
        } else {
            ErrorProcess.noLineAppError()
        }
    }
    
    func popSaveSuccess(historyVC: HistoryViewController) {
        self.endSaveMeasureResult("変更を保存しました")
        historyVC.setNewData(self.heartRateData)
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func saveMeasureResult() {
        
        SVProgressHUD.showWithStatus("保存中")
        heartRateData.memo = memo
        
        // 前の画面によって保存方法を変える.
        
        // 一つ前のVCのindexを取得
        let index:Int = (self.navigationController?.viewControllers.count)!-2
        
        if let historyVC:HistoryViewController = self.navigationController?.viewControllers[index] as? HistoryViewController {
            // 前が履歴画面だったら、上書き保存.
            
            
            // 片方の更新が終わっていたら立てるフラグ.
            var endUpdateFlag:Bool = false
            
            heartRateData.tags = tags
            
            HeartBeatLog.UpdateMemo( heartRateData.id, memo:memo).then{ success in
                self.isSaveSucceeded = success
                }.always {
                    if self.isSaveSucceeded == true {
                        if endUpdateFlag {
                            self.popSaveSuccess(historyVC)
                        }else {
                            endUpdateFlag = true
                        }
                    } else {
                        ErrorProcess.saveHeartbeatLogError()
                    }
            }
            // タグがない場合はタグの保存を無視
            if (tags.count != 0) {
                var retryCount = 3
                
                var onUpdateTags: ((Bool) -> Void)!
                onUpdateTags = { (success: Bool) in
                    if success == true {
                        if endUpdateFlag {
                            self.popSaveSuccess(historyVC)
                        } else {
                            endUpdateFlag = true
                        }
                    } else {
                        if (0 < retryCount) {
                            retryCount -= 1
                            HeartBeatLog.UpdateTags(self.heartRateData.id, tags:self.tags).then { innnerSuccess in
                                onUpdateTags(innnerSuccess)
                            }
                            return
                        }
                        ErrorProcess.saveHeartbeatLogError()
                    }
                    
                    self.isSaveSucceeded = success
                }
                
                HeartBeatLog.UpdateTags(self.heartRateData.id, tags:self.tags).then { success in
                    onUpdateTags(success)
                }
            } else {
                self.popSaveSuccess(historyVC)
            }
            
        }else if let _:MeasureProgressViewController = self.navigationController?.viewControllers[index] as? MeasureProgressViewController {
            // 前がプログレスのViewだったら追加保存.
            HeartBeatLog.Add(self.heartRateData).then { success in
                self.isSaveSucceeded = success
                }.always {
                    
                    if self.isSaveSucceeded == true {
                        self.endSaveMeasureResult("測定結果を保存しました")
                        self.navigationController?.popToRootViewControllerAnimated(true)
                    } else {
                        
                        ErrorProcess.saveHeartbeatLogError()
                    }
            }
        }
        
    }
    //インジケータ停止処理
    func endSaveMeasureResult(status : String) {
        SVProgressHUD.dismiss()
        SVProgressHUD.showSuccessWithStatus(status)
    }
}

//
//  MeasureTopView.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/10/29.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit

protocol MeasureViewDelegate:class {
    func startMeasure()
}

class MeasureTopView: UIView {
    
    @IBOutlet weak var measureButton: UIButton!
    var delegate:MeasureViewDelegate?
    
    @IBAction func touchStartMeasureButton(){
        delegate?.startMeasure()
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
}

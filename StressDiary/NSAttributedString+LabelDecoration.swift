//
//  NSAttributedString+LabelDecoration.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/11/19.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

/* 文字装飾用の拡張 */
extension NSAttributedString {
    class func decoratedOrangeAttributedString(attributeFloat: Float) -> NSAttributedString {
        let attrString = NSMutableAttributedString(string: NSString(format: "%03.0f", attributeFloat) as String)
        
        if floor(log10(attributeFloat)+1) <= 2 {
            attrString.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightOrangeColor(), range: NSMakeRange(0, 1))
            attrString.addAttribute(NSForegroundColorAttributeName, value: UIColor.strongOrangeColor(), range: NSMakeRange(1, 2))
        }else {
            attrString.addAttribute(NSForegroundColorAttributeName, value: UIColor.strongOrangeColor(), range: NSMakeRange(0, 3))
        }
        
        return attrString
    }
    
    class func decoratedLightGrayAttributedString(attributeFloat: Float) -> NSAttributedString {
        let attrString = NSMutableAttributedString(string: NSString(format: "%03.0f", attributeFloat) as String)
        
        if floor(log10(attributeFloat)+1) <= 2 {
            attrString.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGrayColor(), range: NSMakeRange(0, 1))
        }
        
        return attrString
    }
}
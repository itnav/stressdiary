//
//  LoginViewController.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/11/06.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit
import HealthKit
import SVProgressHUD

class LoginViewController: UIViewController, LoginViewDelegate, LoginDelegate {
    
    var login = Login()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        login.delegate = self
        (view as! LoginView).delegate = self
    }
    
    func didTapOriginalIdButton() {
        login.loginUsingOriginalId()
    }
    
    func didTapFacebookButton() {
        login.loginUsingFacebook()
    }
    
    func didTapTwitterButton() {
        login.loginUsingTwitter()
    }
    
    // MARK: -
    // MARK: LoginDelegate
    func didLogin() {
        requestAuthorizationToHealthStore()
    }
    
    // MARK: -
    private func transitionToMeasureBaseHeartRateVC() {
        let measureVC = MeasureViewController(isBaseHeartRate: true)
        let measureNC = UINavigationController(rootViewController: measureVC)
        
        UITabBar.appearance().barTintColor = UIColor(white: 0.95, alpha: 1.0)
        UITabBar.appearance().tintColor = UIColor.strongOrangeColor()
        
        UINavigationBar.appearance().barTintColor = UIColor.navBarOrangeColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        
        UIApplication.sharedApplication().keyWindow?.rootViewController = measureNC
    }
    
    private func requestAuthorizationToHealthStore() {
        let healthStore = HKHealthStore()
        guard HKHealthStore.isHealthDataAvailable() else {
            return
        }
        
        let dataTypes = Set([HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!])
        healthStore.requestAuthorizationToShareTypes(nil, readTypes: dataTypes, completion: { (result, error) -> Void in
            dispatch_async(dispatch_get_main_queue(), { 
                if result {
                    SVProgressHUD.show()
                    BaseHeartRateManager.updateBaseHeartRate {
                        SVProgressHUD.dismiss()
                        let settingVC = SettingViewController(firstSet: true)
                        let measureNC = UINavigationController(rootViewController: settingVC)
                        
                        UITabBar.appearance().barTintColor = UIColor(white: 0.95, alpha: 1.0)
                        UITabBar.appearance().tintColor = UIColor.strongOrangeColor()
                        
                        UINavigationBar.appearance().barTintColor = UIColor.navBarOrangeColor()
                        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
                        
                        UIApplication.sharedApplication().keyWindow?.rootViewController = measureNC
                    }
                } else {
                    self.requestAuthorizationToHealthStore()
                }
            })
        })
    }
}

//
//  DeviceInfo.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/11/19.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit

/* アプリ実行中のデバイスの判定。 */
struct DeviceInfo {
    static func iOSDevice() -> String {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            if 1.0 < UIScreen.mainScreen().scale {
                let size = UIScreen.mainScreen().bounds.size
                let scale = UIScreen.mainScreen().scale
                let result = CGSizeMake(size.width * scale, size.height * scale)
                switch result.height {
                case 960:
                    return "iPhone4/4s"
                case 1136:
                    return "iPhone5/5s/5c"
                case 1334:
                    return "iPhone6/6s"
                case 2208:
                    return "iPhone6 Plus/6s Plus"
                default:
                    return "unknown"
                }
            } else {
                return "iPhone3"
            }
        } else {
            if 1.0 < UIScreen.mainScreen().scale {
                return "iPad Retina"
            } else {
                return "iPad"
            }
        }
    }
}
//
//  BaseHeartRateInfoView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/07.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

class BaseHeartRateInfoView:UIView{
    
    @IBOutlet weak var highRateLabel: UILabel!
    @IBOutlet weak var midRateLabel: UILabel!
    @IBOutlet weak var lowRateLabel: UILabel!
    
    let xibName:String = "BaseHeartRateInfoView"
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        initView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    /*
    * xibのレイアウトファイルの読み込み.
    */
    
    private func initView(){
        print("initView")
        
        if let view:UIView = NSBundle.mainBundle().loadNibNamed(xibName, owner:self, options: nil).first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
            
            highRateLabel.attributedText = NSAttributedString.decoratedOrangeAttributedString(Float(BaseHeartRateManager.getStatisticsHeartRate(.High)))
            midRateLabel.attributedText = NSAttributedString.decoratedOrangeAttributedString(Float(BaseHeartRateManager.getStatisticsHeartRate(.Ave)))
            lowRateLabel.attributedText = NSAttributedString.decoratedOrangeAttributedString(Float(BaseHeartRateManager.getStatisticsHeartRate(.Low)))
            
            highRateLabel.font = UIFont.meterDisplayFont(18)
            midRateLabel.font = UIFont.meterDisplayFont(18)
            lowRateLabel.font = UIFont.meterDisplayFont(18)
            
            let device = DeviceInfo.iOSDevice()
            if device == "iPhone6/6s" {
                highRateLabel.font = UIFont.meterDisplayFont(22)
                midRateLabel.font = UIFont.meterDisplayFont(22)
                lowRateLabel.font = UIFont.meterDisplayFont(22)
            } else if device == "iPhone6 Plus/6s Plus" {
                highRateLabel.font = UIFont.meterDisplayFont(28)
                midRateLabel.font = UIFont.meterDisplayFont(28)
                lowRateLabel.font = UIFont.meterDisplayFont(28)
            }
        }
    }
    
    func reloadData(){
        
        highRateLabel.attributedText = NSAttributedString.decoratedOrangeAttributedString(Float(BaseHeartRateManager.getStatisticsHeartRate(.High)))
        midRateLabel.attributedText = NSAttributedString.decoratedOrangeAttributedString(Float(BaseHeartRateManager.getStatisticsHeartRate(.Ave)))
        lowRateLabel.attributedText = NSAttributedString.decoratedOrangeAttributedString(Float(BaseHeartRateManager.getStatisticsHeartRate(.Low)))
        
        highRateLabel.font = UIFont.meterDisplayFont(18)
        midRateLabel.font = UIFont.meterDisplayFont(18)
        lowRateLabel.font = UIFont.meterDisplayFont(18)
        
    }
}


//
//  HeartRateGraphView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/12.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit
import Charts

struct DataRange {
    var start:NSDate!
    var end:NSDate!
    
    init(start:NSDate,end:NSDate){
        self.start = start
        self.end = end
    }
}

class HeartRateGraphView: UIView, ChartViewDelegate {
    
    let xibName:String = "HeartRateGraphView"
    
    @IBOutlet weak var checkDayGraphButton:UIButton!
    @IBOutlet weak var checkWeekGraphButton:UIButton!
    @IBOutlet weak var checkMonthGraphButton:UIButton!
    @IBOutlet weak var outerFrame:UIView!
    
    @IBOutlet weak var baseHeartRateInfoView: BaseHeartRateInfoView!
    @IBOutlet weak var meterView:StressMeterView!
    @IBOutlet weak var adviceLabel:UILabel!
    
    @IBOutlet weak var lineChartView: LineChartView!
    
    @IBOutlet weak var stressDegreeLabel: UILabel!
    
    var day_x_axis_value:[NSObject] = []
    var week_x_axis_value:[NSObject] = []
    var month_x_axis_value:[NSObject] = []
    
    // x軸の一単位あたり何分割で点を打つか指定.
    
    // 一時間あたり6分割.
    let day_x_axis_division:Int = 6
    
    // 一日あたり12分割.
    let week_x_axis_division:Int = 12
    
    // 一日あたり2分割.
    let month_x_axis_division:Int = 2
    
    // それぞれのデータセット.
    var day_data_entries:[ChartDataEntry] = []
    var week_data_entries:[ChartDataEntry] = []
    var month_data_entries:[ChartDataEntry] = []
    
    var day_data_range:DataRange!
    var week_data_range:DataRange!
    var month_data_range:DataRange!
    
    var day_stress_index:Int = 0
    var week_stress_index:Int = 0
    var month_stress_index:Int = 0
    
    var day_avr_bpm_data:HeartbeatLogData = HeartbeatLogData()
    var week_avr_bpm_data:HeartbeatLogData = HeartbeatLogData()
    var month_avr_bpm_data:HeartbeatLogData = HeartbeatLogData()
    
    var day_advice_text:String = ""
    var week_advice_text:String = ""
    var month_advice_text:String = ""
    
    // 今表示中のグラフを登録(初期値はday).
    var showGraphType:GraphType = .Day
    
    // 基準になる今日の日付を取得.
    var toDay:NSDate = NSDate()
    
    enum GraphType {
        case Day
        case Week
        case Month
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        setupXib()
        
        // 各グラフの表示範囲を取得.
        setupGraphDataRange()
        
        // グラフのinit
//        setUpXAxis()
//        setupLineChartView()
        
        stressDegreeLabel.attributedText = NSAttributedString.decoratedOrangeAttributedString(Float(0))
        stressDegreeLabel.font = UIFont.meterDisplayFont(24)
        
        let device = DeviceInfo.iOSDevice()
        if device == "iPhone6/6s" {
            stressDegreeLabel.font = UIFont.meterDisplayFont(34)
        } else if device == "iPhone6 Plus/6s Plus" {
            stressDegreeLabel.font = UIFont.meterDisplayFont(40)
        }
        
    }
    
    @IBAction func selectDataPeriod(sender:UIButton){
        
        checkDayGraphButton.backgroundColor = UIColor.clearColor()
        checkDayGraphButton.setTitleColor(UIColor.strongOrangeColor(), forState: .Normal)
        checkWeekGraphButton.backgroundColor = UIColor.clearColor()
        checkWeekGraphButton.setTitleColor(UIColor.strongOrangeColor(), forState: .Normal)
        checkMonthGraphButton.backgroundColor = UIColor.clearColor()
        checkMonthGraphButton.setTitleColor(UIColor.strongOrangeColor(), forState: .Normal)
        
        switch sender.tag {
        case 1:
            checkDayGraphButton.backgroundColor = UIColor.whiteColor()
            checkDayGraphButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
            reloadGraph(.Day)
            showGraphType = .Day
        case 2:
            checkWeekGraphButton.backgroundColor = UIColor.whiteColor()
            checkWeekGraphButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
            reloadGraph(.Week)
            showGraphType = .Week
        case 3:
            checkMonthGraphButton.backgroundColor = UIColor.whiteColor()
            checkMonthGraphButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
            reloadGraph(.Month)
            showGraphType = .Month
        default:
            break
        }
    }
    
    func reloadGraph(type:GraphType){
        switch type {
        case .Day:
            let lineChartDataSet = LineChartDataSet(yVals: day_data_entries, label: "")
            lineChartView.data = LineChartData(xVals: day_x_axis_value, dataSet: lineChartDataSet)
            lineChartView.xAxis.setLabelsToSkip(17)    //グラフ修正時に追加　間にある空白Labelの消去用　テスト確認済
            setChartViewLayout(lineChartDataSet)
            
            setStressLabel(HeartbeatLogData.calculationStressindex(day_avr_bpm_data.rateBpm))
            meterView.setMeterValue(HeartbeatLogData.calculationStressindex(day_avr_bpm_data.rateBpm), animation: false, duration: 0)
            adviceLabel.text = day_advice_text
            break
        case .Week:
            let lineChartDataSet = LineChartDataSet(yVals: week_data_entries, label: "")
            lineChartView.data = LineChartData(xVals: week_x_axis_value, dataSet: lineChartDataSet)
            lineChartView.xAxis.setLabelsToSkip(5)    //グラフ修正時に追加　間にある空白Labelの消去用　テスト確認済
            setChartViewLayout(lineChartDataSet)
            
            setStressLabel(HeartbeatLogData.calculationStressindex(week_avr_bpm_data.rateBpm))
            meterView.setMeterValue(HeartbeatLogData.calculationStressindex(week_avr_bpm_data.rateBpm), animation: false, duration: 0)
            adviceLabel.text = week_advice_text
            break
        case .Month:
            let lineChartDataSet = LineChartDataSet(yVals: month_data_entries, label: "")
            lineChartView.data = LineChartData(xVals: month_x_axis_value, dataSet: lineChartDataSet)
            lineChartView.xAxis.resetLabelsToSkip()    //グラフ修正時に追加　こちらは自動設定　テスト確認済
            setChartViewLayout(lineChartDataSet)
            
            setStressLabel(HeartbeatLogData.calculationStressindex(month_avr_bpm_data.rateBpm))
            meterView.setMeterValue(HeartbeatLogData.calculationStressindex(month_avr_bpm_data.rateBpm), animation: false, duration: 0)
            adviceLabel.text = month_advice_text
            break
        }
    }
    
    private func setupXib(){
        if let view:UIView = NSBundle.mainBundle().loadNibNamed(xibName, owner:self, options: nil).first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
        }
        
        outerFrame.layer.borderColor = UIColor.lightOrangeColor().CGColor
        outerFrame.layer.cornerRadius = 20.0
        outerFrame.layer.borderWidth = 3
    }
    
    override func layoutSubviews() {
        setupTopCornerRadius(checkDayGraphButton, radius: 3.0)
        setupTopCornerRadius(checkWeekGraphButton, radius: 3.0)
        setupTopCornerRadius(checkMonthGraphButton, radius: 3.0)
    }
    
    private func setupTopCornerRadius(view: UIView, radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [UIRectCorner.TopLeft, UIRectCorner.TopRight], cornerRadii: CGSizeMake(radius, radius))
        
        let maskLayer = CAShapeLayer()
        maskLayer.frame = view.bounds
        maskLayer.path = maskPath.CGPath
        view.layer.mask = maskLayer
    }
    
    // グラフのデータセットを更新.
    func setupGraphDataRange(){
        
        // Dayのデータ範囲.
        // 今日の0:00~明日の0:00
        day_data_range = SetUpDataManager.sharedInstance.getDataRange(.Graph_Day)
        
        // Weekのデータ範囲.
        // 7日前の0:00 ~ 明日の0:00
        week_data_range = SetUpDataManager.sharedInstance.getDataRange(.Graph_Week)
        
        // Monthのデータ範囲.
        // 今月の1日の0:00 ~ 来月1日の0:00
        month_data_range = SetUpDataManager.sharedInstance.getDataRange(.Graph_Month)
        
    }
    private func setAdviceMessage(type:GraphType,data:String){
        switch type {
        case .Day:
            day_advice_text = data
            if showGraphType == .Day {
                adviceLabel.text = day_advice_text
            }
            break
        case .Week:
            week_advice_text = data
            if showGraphType == .Week {
                adviceLabel.text = week_advice_text
            }
            break
        case .Month:
            month_advice_text = data
            if showGraphType == .Month {
                adviceLabel.text = month_advice_text
            }
            break
        }
        
    }
    private func reloadAdviceMessage(type:GraphType){
        
        switch type {
        case .Day:
            
            var sum:Double = 0.0
            // データを読み込む度に平均値を更新
            for d in day_data_entries {
                sum += d.value
            }
            if day_data_entries.count > 0 {
                day_avr_bpm_data.rateBpm = Int(sum/Double(day_data_entries.count))
            }
            setStressLabel(HeartbeatLogData.calculationStressindex(day_avr_bpm_data.rateBpm))
            meterView.setMeterValue(HeartbeatLogData.calculationStressindex(day_avr_bpm_data.rateBpm), animation: false, duration: 0)
            
            AdviceMessage.FindByStressIndex(HeartbeatLogData.calculationStressindex(day_avr_bpm_data.rateBpm)).then{data in
                self.setAdviceMessage(.Day, data: data.message)
            }
            
            break
        case .Week:
            var sum:Double = 0.0
            // データを読み込む度に平均値を更新
            for d in week_data_entries {
                sum += d.value
            }
            if week_data_entries.count > 0 {
                week_avr_bpm_data.rateBpm = Int(sum/Double(week_data_entries.count))
            }
            setStressLabel(HeartbeatLogData.calculationStressindex(week_avr_bpm_data.rateBpm))
            meterView.setMeterValue(HeartbeatLogData.calculationStressindex(week_avr_bpm_data.rateBpm), animation: false, duration: 0)
            AdviceMessage.FindByStressIndex(HeartbeatLogData.calculationStressindex(week_avr_bpm_data.rateBpm)).then{data in
                self.setAdviceMessage(.Week, data: data.message)
            }
            break
        case .Month:
            var sum:Double = 0.0
            // データを読み込む度に平均値を更新
            for d in month_data_entries {
                sum += d.value
            }
            if month_data_entries.count > 0 {
                month_avr_bpm_data.rateBpm = Int(sum/Double(month_data_entries.count))
            }
            setStressLabel(HeartbeatLogData.calculationStressindex(month_avr_bpm_data.rateBpm))
            meterView.setMeterValue(HeartbeatLogData.calculationStressindex(month_avr_bpm_data.rateBpm), animation: false, duration: 0)
            AdviceMessage.FindByStressIndex(HeartbeatLogData.calculationStressindex(month_avr_bpm_data.rateBpm)).then{data in
                self.setAdviceMessage(.Month, data: data.message)
            }
            break
        }
    }
    func setDataEntries(type:GraphType,data:SearchResultData){
        var addedIndexes: [Int] = []
        data.result.sortInPlace { (data1, data2) -> Bool in
            return NSCalendar.currentCalendar().compareDate(data1.measureDatetime, toDate: data2.measureDatetime, toUnitGranularity: .Second) == .OrderedDescending
        }
        switch type {
        case .Day:
            var sum = 0
            var firstData:HeartbeatLogData!
            
            if data.result.count > 0 {
                
                firstData = data.result.first
                day_data_entries.removeAll()
                
//                let dateFormatter = NSDateFormatter()
//                dateFormatter.dateStyle = .MediumStyle
//                dateFormatter.timeStyle = .NoStyle
                
//                let minutesFormatter = NSDateFormatter()
//                minutesFormatter.dateFormat = "m"
//                
//                let hoursFormatter = NSDateFormatter()
//                hoursFormatter.dateFormat = "H"
                
                for d in data.result {
                    
//                    if dateFormatter.stringFromDate(d.measureDatetime) == dateFormatter.stringFromDate(NSDate()) {
                        // measureDatetime is today
//                        let minutes = Double(minutesFormatter.stringFromDate(d.measureDatetime))!
//                        let hours = Double(hoursFormatter.stringFromDate(d.measureDatetime))!
                        let hoursInterval = d.measureDatetime.timeIntervalSinceDate(NSDate(timeInterval: -60*60*24, sinceDate:toDay))
                        let minutes = Double((floor(hoursInterval/60))%60)
                        let hours = Double(floor(hoursInterval/60/60))
                        
                        
                        let tmpMinutes = round(minutes / 10.0)
                        let tmpHours = hours * 6
                        
                        print("result: \(tmpMinutes + tmpHours)")
                        print("date: \(d.measureDatetime.description)")
                        let i = Int(tmpMinutes + tmpHours)
                        if addedIndexes.indexOf(i) == nil {
                            addedIndexes.append(i)
                            day_data_entries.append(ChartDataEntry(value: Double(d.rateBpm), xIndex: i))
                        }
//                    }
                    
                    
                    sum += d.rateBpm
                    
                    if firstData.measureDatetime.compare(d.measureDatetime) == NSComparisonResult.OrderedDescending {
                        firstData = d
                    }
                }
                day_data_entries.append(ChartDataEntry(value: Double(firstData.rateBpm), xIndex: 0))
            }
            
            // アドバイスメッセージの読み込み.
            reloadAdviceMessage(.Day)
            
            if showGraphType == .Day {
                reloadGraph(.Day)
            }
            break
        case .Week:
            var sum = 0
            var firstData:HeartbeatLogData!
            
            if data.result.count > 0 {
                firstData = data.result.first
                week_data_entries.removeAll()
                
                for d in data.result {
                    let i = Int(Double(week_x_axis_value.count)*(d.measureDatetime.timeIntervalSinceDate(week_data_range.start))/week_data_range.end.timeIntervalSinceDate(week_data_range.start))
                    sum += d.rateBpm
                    
                    if addedIndexes.indexOf(i) == nil {
                        addedIndexes.append(i)
                        week_data_entries.append(ChartDataEntry(value: Double(d.rateBpm), xIndex: i))
                        print("week graph i: \(i)")
                    }
                    
                    if firstData.measureDatetime.compare(d.measureDatetime) == NSComparisonResult.OrderedDescending {
                        firstData = d
                    }
                }
                week_data_entries.append(ChartDataEntry(value: Double(firstData.rateBpm), xIndex: 0))
                
            }
            
            // アドバイスメッセージの読み込み.
            reloadAdviceMessage(.Week)
            
            if showGraphType == .Week {
                reloadGraph(.Week)
            }
            break
        case .Month:
            var sum = 0
            var firstData:HeartbeatLogData!
            
            if data.result.count > 0 {
                firstData = data.result.first
                month_data_entries.removeAll()
                
                for d in data.result {
                    let i = Int(Double(month_x_axis_value.count)*(d.measureDatetime.timeIntervalSinceDate(month_data_range.start))/month_data_range.end.timeIntervalSinceDate(month_data_range.start))
                    sum += d.rateBpm
                    
                    if addedIndexes.indexOf(i) == nil {
                        addedIndexes.append(i)
                        month_data_entries.append(ChartDataEntry(value: Double(d.rateBpm), xIndex: i))
                    }
                    
                    if firstData.measureDatetime.compare(d.measureDatetime) == NSComparisonResult.OrderedDescending {
                        firstData = d
                    }
                }
                month_data_entries.append(ChartDataEntry(value: Double(firstData.rateBpm), xIndex: 0))
            }

            // アドバイスメッセージの読み込み.
            reloadAdviceMessage(.Month)
            if showGraphType == .Month {
                reloadGraph(.Month)
            }
            break
        }
    }
    
    // グラフの横軸を用意.
    func setUpXAxis(){
        toDay = NSDate()
        let cal = NSCalendar.currentCalendar()
        day_x_axis_value = []
        week_x_axis_value = []
        month_x_axis_value = []
        
        // 日にち別グラフの横軸.(10分に1ポイント.)
        for i in 0...24 {
            for j in 0..<day_x_axis_division {
                if j == 0 {
                    let beforedate = NSDate(timeInterval: -60*60*Double(24 - i), sinceDate: toDay)
                    let hour = cal.component(.Hour, fromDate: beforedate)
                    day_x_axis_value.append("\(hour)時")
                }else{
                    day_x_axis_value.append("")
                }
            }
        }
        
//        for(var i:Int=0;i<=24;i++){
//            for(var j:Int=0; j<day_x_axis_division; j++){
//                if j == 0 {
//                    day_x_axis_value.append("\(i)時")
//                }else {
//                    day_x_axis_value.append("")
//                }
//            }
//        }
        
        // 週別グラフの横軸.(曜日)
        // 今日の曜日を取得.
        let df:NSDateFormatter = NSDateFormatter().getOnlyWeekFormat()
        // 一日あたり6のポイントを用意しておく.
        for i in 0...7 {
            for j in 0..<week_x_axis_division {
                if j == 0 {
                    let beforedate:NSDate = NSDate(timeInterval: -60*60*24*Double(7 - i), sinceDate: toDay)
                    week_x_axis_value.append(df.stringFromDate(beforedate))
                }else{
                    self.week_x_axis_value.append("")
                }
            }
        }
        
//        for(var i:Int = -6; i <= 0; i++){
//            for(var j:Int=0; j<week_x_axis_division; j++){
//                //はじめの一回目だけラベルをつけておく.
//                if j == 0{
//                    let toDay:NSDate = NSDate(timeIntervalSinceNow:Double(i*24*60*60))
//                    self.week_x_axis_value.append(df.stringFromDate(toDay))
//                }else {
//                    self.week_x_axis_value.append("")
//                }
//            }
//        }
        
        // 月別グラフの横軸.(日にち)
        // 一日あたり
        for i in 0...30 {
            for j in 0..<month_x_axis_division {
                if j == 0 {
                    let beforedate = NSDate(timeInterval: -60*60*24*Double(30 - i), sinceDate: toDay)
                    let month = cal.component(.Month, fromDate: beforedate)
                    let day = cal.component(.Day, fromDate: beforedate)
                    month_x_axis_value.append("\(month)/\(day)")
                }else{
                    month_x_axis_value.append("")
                }
            }
        }
        
//        for(var i:Int=1;i<=NSDate.getLastDayOfMonth(toDay.getYear(), month:toDay.getMonth());i++){
//            for(var j:Int=0; j<month_x_axis_division; j++){
//                if j == 0{
//                    month_x_axis_value.append("\(i)")
//                }else {
//                    month_x_axis_value.append("")
//                }
//            }
//        }
        
    }
    
    func setStressLabel(stressIndex:Double){
        
        var _stressIndex = stressIndex
        
        if _stressIndex > 100 {
            _stressIndex = 100
        } else if _stressIndex < 0 {
            _stressIndex = 0
        }
        
        stressDegreeLabel.attributedText = NSAttributedString.decoratedOrangeAttributedString(Float(_stressIndex))
        stressDegreeLabel.font = UIFont.meterDisplayFont(24)
        
        let device = DeviceInfo.iOSDevice()
        if device == "iPhone6/6s" {
            stressDegreeLabel.font = UIFont.meterDisplayFont(34)
        } else if device == "iPhone6 Plus/6s Plus" {
            stressDegreeLabel.font = UIFont.meterDisplayFont(40)
        }
    }
    
    func setupLineChartView() {
        
        // 初期はdayのデータを入れておく.
        let lineChartDataSet = LineChartDataSet(yVals: day_data_entries, label: nil)
        
        let lineChartData = LineChartData(xVals: day_x_axis_value, dataSet: lineChartDataSet)
        
        lineChartView.delegate = self
        lineChartView.gridBackgroundColor = UIColor.whiteColor()
        
        lineChartView.setScaleEnabled(true)
        
        // gridの色を指定.
        lineChartView.xAxis.gridColor = UIColor.strongOrangeColor()
        lineChartView.xAxis.labelFont = UIFont.systemFontOfSize(7.5)
        lineChartView.xAxis.gridLineWidth = 0.3
        lineChartView.leftAxis.gridColor = UIColor.strongOrangeColor()
        lineChartView.leftAxis.gridLineWidth = 0.3
        lineChartView.rightAxis.gridColor = UIColor.strongOrangeColor()
        lineChartView.rightAxis.gridLineWidth = 0.3
        
        lineChartView.descriptionText = ""
        lineChartView.data = lineChartData
        
        lineChartView.scaleXEnabled = false
        lineChartView.scaleYEnabled = true
        
        lineChartView.leftAxis.enabled = false
        lineChartView.leftAxis.customAxisMax = 200
        lineChartView.leftAxis.customAxisMin = 0
        
        lineChartView.rightAxis.customAxisMin = 0
        lineChartView.rightAxis.customAxisMax = 200
        
        lineChartView.legend.enabled = false
        
        lineChartView.drawMarkers = false
        
        lineChartView.xAxis.labelPosition = .Bottom
        
        setChartViewLayout(lineChartDataSet)
    }
    
    private func setChartViewLayout(dataSet:LineChartDataSet){
        
        // サークルは表示しない.
        dataSet.drawCirclesEnabled = false
        dataSet.lineWidth = 2
        dataSet.setColor(UIColor.strongOrangeColor())
        
        // 各ポイントに数値を表示しないようにする
        for var set in (lineChartView.data?.dataSets)! {
            set.drawValuesEnabled = false
            set.setColor(UIColor.strongOrangeColor())
        }
        
        lineChartView.setNeedsDisplay()
        
    }
    
    // MARK: -ChartViewDelegate
    func chartValueSelected(chartView: ChartViewBase, entry: ChartDataEntry, dataSetIndex: Int, highlight: ChartHighlight) {
        print("chartValueSelected")
        print("Entry = \(entry)")
        
        // pointを取得
        let markerPosition = chartView.getMarkerPosition(entry: entry, highlight: highlight)
        print(markerPosition)
    }
    
    func chartValueNothingSelected(chartView: ChartViewBase) {
        print("chartValueNothingSelected")
    }
    
}
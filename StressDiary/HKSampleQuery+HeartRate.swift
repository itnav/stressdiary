//
//  HKSampleQuery+HeartRate.swift
//  StressDiary
//
//  Created by 海崎惇志 on 2016/04/26.
//  Copyright © 2016年 itnav. All rights reserved.
//

import Foundation
import HealthKit

extension HKQuantitySample {
    /// 心拍数
    func heartRate() -> Int {
        let heartRateUnit = HKUnit(fromString: "count/min")
        let quantity = self.quantity.doubleValueForUnit(heartRateUnit)
        return Int(floor(quantity))
    }
}

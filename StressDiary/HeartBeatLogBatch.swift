//
//  HeartBeatLogBatch.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/12/11.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import HealthKit
import SVProgressHUD

/* ヘルスケアからの心拍データのバッチ処理 */
struct HeartBeatLogBatch {
    static func getOneDayHeartbeatLogs(date: NSDate, i: Int, completion: ((i: Int) -> Void)?) {
        let healthStore = HKHealthStore()
        let calendar :NSCalendar! = NSCalendar(identifier: NSCalendarIdentifierGregorian)
        
        // dateから何日前か
        let day = -1
        
        let endDate = date
        let startDate = calendar.dateByAddingUnit([.Day], value: day, toDate: date, options: .WrapComponents)
        
        let predicate = HKQuery.predicateForSamplesWithStartDate(startDate, endDate: endDate, options: [.None])
        
        let sampleType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!
        let timeSortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        
        let heartRateQuery = HKSampleQuery(sampleType: sampleType, predicate: predicate, limit: 0, sortDescriptors: [timeSortDescriptor], resultsHandler: { query, results, error in
            if let _ = error {
                print("心拍データの取得に失敗しました")
                print(error)
                dispatch_async(dispatch_get_main_queue(), { 
                    completion?(i: i)
                })
                return
            }
            
            if results?.count != 0 {
                addHeartBeatLogs(results as! [HKQuantitySample], completion: { 
                    dispatch_async(dispatch_get_main_queue(), {
                        completion?(i: i)
                    })
                })
            }else {
                print("NODATA")
                dispatch_async(dispatch_get_main_queue(), {
                    completion?(i: i)
                })
            }
        })
        
        healthStore.executeQuery(heartRateQuery)
    }
    
    // 一応作ったが特に使わない関数
    static func getAllHeartbeatLogs() {
        print("getAllHeartbeatLogs")
        let healthStore = HKHealthStore()
        
        let sampleType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!
        let timeSortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: true)
        
        let heartRateQuery = HKSampleQuery(sampleType: sampleType, predicate: nil, limit: 0, sortDescriptors: [timeSortDescriptor], resultsHandler: { query, results, error in
            if let _ = error {
                print("心拍データの取得に失敗しました")
                print(error)
                return
            }
            
            var heartRates: [Int] = []
            
            print("samples = \((results as! [HKQuantitySample]).first!.description.componentsSeparatedByString(" "))")
            print((results as! [HKQuantitySample]).first!.description.componentsSeparatedByString(" ")[5])
            print((results as! [HKQuantitySample]).first!.description.componentsSeparatedByString(" ")[6])
            print((results as! [HKQuantitySample]).first!.description.componentsSeparatedByString(" ")[7])
            // "2015-12-04", "02:08:54", "+0900"
            // [6],          [7],        [8]
            
            for result in results! {
                print("-----------------------------------------------------")
                print((result as! HKQuantitySample).description.componentsSeparatedByString(" ")[5])
                print((result as! HKQuantitySample).description.componentsSeparatedByString(" ")[6])
                print((result as! HKQuantitySample).description.componentsSeparatedByString(" ")[7])
            }
            
            if results?.count != 0 {
                for heartRate: HKQuantitySample in results as! [HKQuantitySample] {
                    let heartRate = Int(Double(heartRate.description.componentsSeparatedByString(" ")[0])! * 60)
                    heartRates.append(heartRate)
                }
            } else {
                print("NODATA")
            }
            
            print("results.count = \(results!.count)")
            print("heartRates = \(heartRates)")
        })
        
        healthStore.executeQuery(heartRateQuery)
    }
    
    static func getOldestHeartbeatDateString(completion: (() -> Void)?) {
        print("getOldestHeartbeatDate")
        let healthStore = HKHealthStore()
        
        let sampleType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!
        let timeSortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: true)
        
        let heartRateQuery = HKSampleQuery(sampleType: sampleType, predicate: nil, limit: 0, sortDescriptors: [timeSortDescriptor], resultsHandler: { query, results, error in
            if let _ = error {
                print("心拍データの取得に失敗しました")
                print(error)
                return
            }
            
            // "2015-12-04", "02:08:54", "+0900"
            // [6],          [7],        [8]
            
            if results?.count != 0 {
                
                let result = (results as! [HKQuantitySample]).first
                let oldestDate = result?.startDate
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let oldestDateString = dateFormatter.stringFromDate(oldestDate!)
                print((getDaysCountByTwoDateString(oldestDateString)))
                if oldestDate != nil {
                    let max = getDaysCountByTwoDateString(oldestDateString)
                    for var i = 0; i < max; i++ {
                        print("SAVING")
                        print(i)
                        
                        
                        
                        let startDate = NSDate(timeInterval: Double(i*24*60*60), sinceDate: oldestDate!)
                        print(startDate)
                        getOneDayHeartbeatLogs(startDate, i: i, completion: { (i) in
                            if i + 1 == max {
                                completion?()
                            }
                        })
                    }
                } else {
                    print("PARSE ERROR")
                    dispatch_async(dispatch_get_main_queue(), { 
                        completion?()
                    })
                }
                
            } else {
                print("NODATA")
                dispatch_async(dispatch_get_main_queue(), {
                    completion?()
                })
            }
        })
        
        healthStore.executeQuery(heartRateQuery)
    }
    
    static func getHeartbeatDateFromLastBatchDate() {
        getHeartbeatDateFromLastBatchDate(nil)
    }
    
    static func getHeartbeatDateFromLastBatchDate(completion: (() -> Void)?) {
        print("getHeartbeatDateFromLastBatchDate")
        let healthStore = HKHealthStore()
        
        let sampleType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!
        let timeSortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let _ = UserDefaultsManager.getLastBatchDateString() {
            let heartRateQuery = HKSampleQuery(sampleType: sampleType, predicate: HKQuery.predicateForSamplesWithStartDate(dateFormatter.dateFromString(UserDefaultsManager.getLastBatchDateString()!), endDate: NSDate(), options: [.None]), limit: 0, sortDescriptors: [timeSortDescriptor], resultsHandler: { query, results, error in
                if let _ = error {
                    print("心拍データの取得に失敗しました")
                    print(error)
                    dispatch_async(dispatch_get_main_queue(), { 
                        completion?()
                    })
                    return
                }
                
                // "2015-12-04", "02:08:54", "+0900"
                // [6],          [7],        [8]
                
                if results?.count != 0 {
                    addHeartBeatLogs(results as! [HKQuantitySample], completion: completion)
                } else {
                    print("NODATA")
                    dispatch_async(dispatch_get_main_queue(), {
                        completion?()
                    })
                }
            })
            
            healthStore.executeQuery(heartRateQuery)
        } else {
            completion?()
        }
    }
    
    static func getDaysCountByTwoDateString(fromDateString: String) -> Int {
        let cal = NSCalendar(identifier: NSCalendarIdentifierGregorian)!
        // 当日のすべてのデータは1日前
        let now = NSDate()
        let beforeOneDay = cal.dateByAddingUnit(.Day, value: 1, toDate: now, options: NSCalendarOptions())
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let fromDate = dateFormatter.dateFromString(fromDateString)
        
        let componentsByDay = cal.components(.Day, fromDate: fromDate!, toDate: beforeOneDay!, options: NSCalendarOptions())
        
        return componentsByDay.day
    }
    
    private static func addHeartBeatLogs(samples: [HKQuantitySample]) {
        addHeartBeatLogs(samples, completion: nil)

    }
    
    private static func addHeartBeatLogs(samples: [HKQuantitySample], completion: (() -> Void)?) {
        var heartbeatLogs: [[String: AnyObject]] = []
        var lastMeasureDateString = ""
        
        for result: HKQuantitySample in samples {
            
            let measureDate = result.endDate
            let heartRateUnit = HKUnit(fromString: "count/min")
            let quantity = result.quantity.doubleValueForUnit(heartRateUnit)
            let heartRate = Int(floor(quantity))
            var stressIndex = Int(HeartbeatLogData.calculationStressindex(heartRate))
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = NSTimeZone(name: "UTC")
            let measureDateString = dateFormatter.stringFromDate(measureDate)
            
            dateFormatter.dateFormat = "yyyy-MM-dd"
            lastMeasureDateString = dateFormatter.stringFromDate(measureDate)
            
            if stressIndex > 100 {
                stressIndex = 100
            } else if stressIndex < 0 {
                stressIndex = 0
            }
            
            let dic =
                [
                    "measureDatetime": measureDateString,
                    "rateBpm": heartRate,
                    "stressIndex": stressIndex,
                    "tags":[],
                    "memo":""
            ]
            
            heartbeatLogs.append(dic as! [String : AnyObject])
        }
        
        var completed: Bool = false
        HeartBeatLog.AddAll(heartbeatLogs).then { complete in
            completed = complete
            }.always {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                if completed == true && dateFormatter.dateFromString(lastMeasureDateString) != nil {
                    print("SAVED")
                    
                    // TODO: 日付の更新処理
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let nowDateString = dateFormatter.stringFromDate(NSDate())
                    UserDefaultsManager.setLastBatchDateString(nowDateString)
                } else {
                    print("SAVE ERROR")
                }
                dispatch_async(dispatch_get_main_queue(), {
                    completion?()
                })
        }
        
    }
}
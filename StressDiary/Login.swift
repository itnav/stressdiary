//
//  Authentication.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/11/06.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import PromiseKit
import Accounts
import STTwitter
import SVProgressHUD

/* ログイン処理のまとめ */
protocol LoginDelegate: class {
    func didLogin()
}

struct Login {
    
    let DeviceType = "DEVICE"
    let FacebookType = "FACEBOOK"
    let TwitterType = "TWITTER"
    
    weak var delegate: LoginDelegate?
    
    func loginUsingOriginalId() {
        let uuidString = NSUUID().UUIDString
        
        if UserDefaultsManager.isFirstLogin() == true {
            UserInfo.register(DeviceType, id: uuidString).then { completion in
                print(completion)
                }.always {
                    SVProgressHUD.dismiss()
                    
                    KeychainManager.setOriginalID(uuidString)
                    KeychainManager.setCurrentAuthID(uuidString)
                    KeychainManager.setCurrentAuthType(self.DeviceType)
                    
                    UserDefaultsManager.setFirstLogin()
                    
                    self.delegate?.didLogin()
            }
        }
        
    }
    
    func loginUsingFacebook() {
        var fbID = ""
        
        // 初回登録だった場合
        if UserDefaultsManager.isFirstLogin() == true {
            // 1. FacebookIDを取得する
            Login.getFacebookID().then { id in
                fbID = id
                
                // 2. 登録処理を行う
                }.then {
                    UserInfo.register(self.FacebookType, id: fbID)
                    
                    // 3. 通信完了時に端末に保存
                }.always { _ in
                    SVProgressHUD.dismiss()
                    if fbID != "" {
                        KeychainManager.setFacebookID(fbID)
                        KeychainManager.setCurrentAuthID(fbID)
                        KeychainManager.setCurrentAuthType(self.FacebookType)
                        
                        UserDefaultsManager.setFirstLogin()
                        
                        self.delegate?.didLogin()
                    }
            }
        }
    }
    
    func loginUsingTwitter() {
        var twitterID = ""
        
        // 初回登録だった場合
        if UserDefaultsManager.isFirstLogin() == true {
            // 1. TwitterIDを取得する
            Login.getTwitterID().then { id in
                twitterID = id
                
                // 2. 登録処理を行う
                }.then {
                    UserInfo.register(self.TwitterType, id: twitterID)
                    
                    // 3. 通信完了時に端末に保存
                }.always { _ in
                    SVProgressHUD.dismiss()
                    if twitterID != "" {
                        KeychainManager.setTwitterID(twitterID)
                        KeychainManager.setCurrentAuthID(twitterID)
                        KeychainManager.setCurrentAuthType(self.TwitterType)
                        
                        UserDefaultsManager.setFirstLogin()
                        
                        self.delegate?.didLogin()
                    }
            }
        }
    }
    
    static func getFacebookID() -> Promise<String> {
        SVProgressHUD.show()
        return Promise { fulfill, reject in
            let accountStore = ACAccountStore()
            let accountType = accountStore.accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierFacebook)
            let key = NSBundle.mainBundle().objectForInfoDictionaryKey("FacebookAppID") as! String
            let options: [NSObject: AnyObject] = [ACFacebookAppIdKey: key, ACFacebookPermissionsKey: ["email"]]
            
            
            accountStore.requestAccessToAccountsWithType(accountType, options: options, completion: { granted, error in
                if error != nil {
                    if error.localizedDescription == "The Facebook server could not fulfill this access request: The proxied app is not already installed. (408)" {
                        ErrorProcess.fbSocialAuthenticationError()
                        reject(SNSError.FacebookSocialAuthentication)
                        return
                    }
                    if error.code == -1009 {
                        ErrorProcess.communicationError()
                        reject(CommunicationError.NoCommunicationLine)
                    }
                    if error.code == 6 {
                        ErrorProcess.noFacebookAccountError()
                        reject(SNSError.NoFacebookAccount)
                    }
                    if error.code == 7 {
                        ErrorProcess.fbAuthenticationError()
                        reject(SNSError.FacebookAuthentication)
                    }
                    return
                }
                
                if !granted {
                    // error.code == 7でも抜けてくることがあるので認証もチェックする
                    ErrorProcess.fbAuthenticationError()
                    reject(SNSError.FacebookAuthentication)
                    return
                }
                
                //                print("GRANTED")
                let accounts = accountStore.accountsWithAccountType(accountType)
                
                // FacebookAccountがiPhoneに登録されていた場合
                if accounts.count > 0 {
                    let account = accounts.last as! ACAccount
                    
                    let userID: AnyObject? = account.valueForKey("properties")?.valueForKey("uid")
                    fulfill(String(userID!))
                } else {
                    ErrorProcess.noFacebookAccountError()
                    reject(SNSError.NoFacebookAccount)
                }
            })
        }
    }
    
    // ①
    static func getTwitterID() -> Promise<String> {
        return Promise { fulfill, reject in
            let accountStore = ACAccountStore()
            let accountType = accountStore.accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierTwitter)
            
            accountStore.requestAccessToAccountsWithType(accountType, options: nil, completion: { granted, error in
                
                if error != nil {
                    reject(error)
                    return
                }
                
                if !granted {
                    ErrorProcess.twitterAuthenticationError()
                    reject(SNSError.TwitterAuthentication)
                    return
                }
                
                let accounts = accountStore.accountsWithAccountType(accountType) as! [ACAccount]
                if accounts.count == 0 {
                    ErrorProcess.noTwitterAccountError()
                    reject(NSError(domain: "NoTwitterAccountError", code: 2, userInfo: nil))
                    return
                } else {
                    Login.showAccountSelectSheet(accounts).then { userID in
                        fulfill(userID)
                        }.error { error in
                            SVProgressHUD.dismiss()
                            reject(error)
                    }
                }
            })
        }
    }
    
    // ②
    static func showAccountSelectSheet(accounts: [ACAccount]) -> Promise<String> {
        return Promise { fulfill, reject in
            let alert = UIAlertController(title: "Twitterでログイン", message: "アカウントを選択してください", preferredStyle: .ActionSheet)
            
            for account in accounts {
                alert.addAction(UIAlertAction(title: account.username, style: .Default, handler: { action -> Void in
                    SVProgressHUD.show()
                    Login.loginToTwitter(account).then { userID in
                        fulfill(userID)
                        }.error { error in
                            reject(error)
                    }
                }))
            }
            
            // キャンセルボタン
            alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
                reject(NSError(domain: "アカウントの選択をキャンセル", code: 1, userInfo: nil))
            }))
            
            dispatch_async(dispatch_get_main_queue(), {
                let vc = ErrorProcess.getTopVC()
                vc.presentViewController(alert, animated: true, completion: nil)
            })
        }
    }
    
    // ③
    static func loginToTwitter(account: ACAccount) -> Promise<String> {
        return Promise { fulfill, reject in
            let twitter = STTwitterAPI(OAuthConsumerName: nil, consumerKey: "reluT1OYMsm9gdrWSCl0MUcos", consumerSecret: "fPXY82U2zvJrYLS6xNIuFJlqpFLIY7XimolHoPfpFUfVf3qWgE")
            
            twitter.postReverseOAuthTokenRequest({ authenticationHeader -> Void in
                
                let twitterAPIOS = STTwitterAPI.twitterAPIOSWithAccount(account)
                
                twitterAPIOS.verifyCredentialsWithUserSuccessBlock({ userName -> Void in
                    
                    twitterAPIOS.postReverseAuthAccessTokenWithAuthenticationHeader(authenticationHeader, successBlock: { oAuthToken, oAuthTokenSecret, userID, screenName -> Void in
                        
                        fulfill(String(userID))
                        
                        }, errorBlock: { error -> Void in
                            print("TWITTER ERROR = \(error)")
                            reject(error)
                    })
                    }, errorBlock: { error -> Void in
                        // TODO: Twitter側の認証の問題なので、そこを許可する文言に変える
                        ErrorProcess.twitterAuthenticationError()
                        reject(SNSError.TwitterAuthentication)
                })
                // 通信エラー
                }, errorBlock: { error -> Void in
                    ErrorProcess.communicationError()
                    reject(CommunicationError.NoCommunicationLine)
            })
        }
    }
}
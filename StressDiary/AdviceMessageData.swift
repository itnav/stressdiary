//
//  AdviceMessageModel.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/18.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation

/* アドバイスメッセージのデータモデル */
class AdviceMessageData{
    
    internal var stressIndexCriteria:Double!
    internal var message:String!
    
    /*
    * データの初期化処理.
    */
    init(){
        self.stressIndexCriteria = 0.0
        self.message = ""
    }
    init(data:AdviceMessageData) {
        self.stressIndexCriteria = data.stressIndexCriteria
        self.message = data.message
    }
    
    internal func setAdviceMessageData(data:NSDictionary){
        
        self.stressIndexCriteria = data.objectForKey("stressIndexCriteria") as! Double
        self.message = data.objectForKey("message") as! String
        
    }
}
//
//  LoginView.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/11/06.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit

protocol LoginViewDelegate: class {
    func didTapOriginalIdButton()
    func didTapFacebookButton()
    func didTapTwitterButton()
}

class LoginView: UIView {
    
    weak var delegate: LoginViewDelegate?
    
    @IBAction func tappedOriginalIdButton() {
        delegate?.didTapOriginalIdButton()
    }
    
    @IBAction func tappedFacebookButton() {
        delegate?.didTapFacebookButton()
    }
    
    @IBAction func tappedTwitterButton() {
        delegate?.didTapTwitterButton()
    }
}

//
//   SettingListCellView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/15.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

class SettingListCellView: UITableViewCell {
    
    private let xibName:String = "SettingListCellView"
    
    @IBOutlet weak var settingTitleLabel:UILabel!
    @IBOutlet weak var settingDetailLabel:UILabel!
    @IBOutlet weak var bgView:UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupXib()
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupXib()
    }
    
    
    /*
    * xibのレイアウトファイルの読み込み.
    */
    private func setupXib(){
        
        if let view:UIView = NSBundle.mainBundle().loadNibNamed(xibName, owner:self, options: nil).first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
        }
        
        bgView.layer.shadowColor = UIColor.navBarOrangeColor().CGColor
        bgView.layer.shadowOffset = CGSizeMake(0.5, 0.5)
        bgView.layer.shadowOpacity = 1.0
        bgView.layer.shadowRadius = 3.0
        
        self.selectionStyle = UITableViewCellSelectionStyle.None
    }
}
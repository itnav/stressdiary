//
//  UIColor+AppColors.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/04.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit

extension UIColor {
    
    /*
     ナビゲーションバーで使われているオレンジ.
    */
    class func navBarOrangeColor() -> UIColor {
        return UIColor(red: 1.00, green: 0.74, blue: 0.37, alpha: 1.0)
    }
    
    /*
     濃いめのオレンジ.
    */
    class func strongOrangeColor() -> UIColor {
        return UIColor(red: 1.00, green: 0.64, blue: 0.00, alpha: 1.0)
    }
    
    /*
     薄めのオレンジ.
    */
    class func lightOrangeColor() -> UIColor{
        return UIColor(red: 0.96, green: 0.91, blue: 0.69, alpha: 1.0)
    }
    
    /*
     薄めのグレイ
    */
    class func appGrayColor() -> UIColor {
        return UIColor(red: 0.81, green: 0.81, blue: 0.81, alpha: 1.0)
    }
    
}

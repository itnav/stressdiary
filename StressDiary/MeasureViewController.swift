//
//  MeasureViewController.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/10/28.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit
import WatchConnectivity
import HealthKit

class MeasureViewController: UIViewController, WCSessionDelegate {
    
    let session = WCSession.defaultSession()
    @IBOutlet weak var measureButton: UIButton!
    
    var isBaseHeartRate: Bool = false
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(isBaseHeartRate: Bool) {
        super.init(nibName: nil, bundle: nil)
        
        self.isBaseHeartRate = isBaseHeartRate
        
        self.tabBarItem.image = UIImage(named: "icon_menu_heart")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
    }
    
    // MARK: -setup
    private func setupNavigationBar() {
        
        // NavigationBarの下にViewが埋まってしまうことを回避
        edgesForExtendedLayout = UIRectEdge.None;
        title = "測定"
        if isBaseHeartRate == true {
            title = "基本心拍測定"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")
        
        setupNavigationBar()
        
        //        let button = UIButton(frame: CGRectMake(20, 20, 100, 100))
        //        button.backgroundColor = UIColor.blackColor()
        //        button.addTarget(self, action: "skip", forControlEvents: .TouchUpInside)
        //        view.addSubview(button)
    }
    
    @IBAction func startMeasureButtonTapped() {
        checkCanReadHealthData()
    }
    
    func startMeasure() {
        if (WCSession.isSupported()) {
            session.delegate = self
            session.activateSession()
            print(session)
        }
        
        sendStartMeasureEvent()
    }
    
    private func sendStartMeasureEvent() {
        print("sendStartMeasureEvent")
        if (WCSession.defaultSession().reachable) {
            measureButton.userInteractionEnabled = false
            let message = ["message" : "start"]
            WCSession.defaultSession().sendMessage(message,
                replyHandler: { (reply) -> Void in
                    print("reply = \(reply)")
                    if reply["reply"] as! String == "notAuthorized" {
                        
                        let healthStore = HKHealthStore()
                        let typesToRead = Set([HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!])
                        // HealthKitへのアクセス許可をユーザーへ求める
                        healthStore.requestAuthorizationToShareTypes(nil, readTypes: typesToRead, completion: { success, error in
                            
                        })
                        
                        self.measureButton.userInteractionEnabled = true
                        ErrorProcess.healthCareNotAuthorizedError()
                    } else {
                        dispatch_async(dispatch_get_main_queue(), {
                            self.measureButton.userInteractionEnabled = true
                            self.transitionToMeasureProgressVC()
                        })
                    }
                },
                errorHandler: { (error) -> Void in
                    dispatch_async(dispatch_get_main_queue(), {
                        self.measureButton.userInteractionEnabled = true
                    })
                    print("error = \(error)")
                }
            )
        }
    }
    
    //心拍数データの読み出しが許可されているか、読み出したデータの個数で判定する
    func checkCanReadHealthData() {
        let healthStore = HKHealthStore()
        
        let sampleType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!
        let timeSortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: true)
        
        let heartRateQuery = HKSampleQuery(sampleType: sampleType, predicate: nil, limit: 0, sortDescriptors: [timeSortDescriptor], resultsHandler: { query, results, error in
            if (error != nil) {
                print(error)
                
                ErrorProcess.healthCareNotAuthorizedError()
                
                return
            }else {
                
//                if (results?.count != 0) {
                    //一つ以上ヘルスケアデータが返ってきたら計測開始する
                    self.startMeasure()
//                }else {
//                    //ヘルスケアデータが返ってこない場合は、データが一つも無いか読み出し許可を切っている
//                    ErrorProcess.healthCareNotAuthorizedError()
//                }
                
            }
            
        })
        
        healthStore.executeQuery(heartRateQuery)
    }
    
    
    
    //    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
    //        if message["message"] as? String == "notAuthorized" {
    //
    //            ErrorProcess.healthCareNotAuthorizedError()
    //
    //            replyHandler(["reply":"notAuthorized"])
    //        }
    //    }
    
    private func transitionToMeasureProgressVC() {
        let measureProgressVC = MeasureProgressViewController(isBaseHeartRate: isBaseHeartRate)
        navigationController?.pushViewController(measureProgressVC, animated: true)
    }
    
    // デバッグ用のメソッド
    func skip() {
        BaseHeartRateManager.setBaseHeartRate(60)
        UIApplication.sharedApplication().keyWindow?.rootViewController = setupTabBarController(false)
    }
}
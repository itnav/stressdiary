//
//  PeriodicNotification.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/11/24.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit

/* 定期通知のLocalNotification関連 */
class PeriodicNotification {
    static func getWeekday() {
        let date: NSDate = NSDate()
        let cal: NSCalendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)!
        let comp: NSDateComponents = cal.components([NSCalendarUnit.Weekday], fromDate: date)
        let weekday: Int = comp.weekday
        
        let weekdaySymbolIndex: Int = weekday - 1
        let formatter: NSDateFormatter = NSDateFormatter()
        
        formatter.locale = NSLocale(localeIdentifier: "ja")
        
        print(formatter.shortWeekdaySymbols[weekdaySymbolIndex]) // -> 日
        print(formatter.weekdaySymbols[weekdaySymbolIndex]) // -> 日曜日
    }
    
    static func clearAllPeriodicNotification() {
        UIApplication.sharedApplication().cancelAllLocalNotifications()
        
        // TODO: 傾向からくるNotificationの削除まで行わない様にPeriodicNotificationのみを判別して削除する必要アリ
//        UIApplication.sharedApplication().cancelLocalNotification(<#T##notification: UILocalNotification##UILocalNotification#>)
    }
    
    static func registerPeriodicNotification() {
        // 登録時に以前残っていたNotificationを削除
        clearAllPeriodicNotification()
        
        let calendar = NSCalendar.currentCalendar()
        calendar.timeZone = NSTimeZone.localTimeZone()
        // Notificationを登録する数。曜日設定の場合は/7で7週分
        let repeatNum = 70
        let today = NSDate(timeIntervalSinceNow: 60*60*9)
        var date: NSDate
        var componentsForFireDate = NSDateComponents()
        
        for var i = 0; i < repeatNum; i++ {
            date = today.dateByAddingTimeInterval(Double(i) * 24.0 * 60.0 * 60.0)
            componentsForFireDate = calendar.components([NSCalendarUnit.Year, NSCalendarUnit.Month, NSCalendarUnit.Day, NSCalendarUnit.Weekday], fromDate: date)
            
            componentsForFireDate.hour = UserDefaultsManager.getPeriodicNotificationTimeHour()
            componentsForFireDate.minute = UserDefaultsManager.getPeriodicNotificationTimeMinutes()
            componentsForFireDate.second = 0
            let fireDateOfNotificaiton = calendar.dateFromComponents(componentsForFireDate)
            
            /*** 毎日 ***/
            // 曜日が登録されていなかったら毎日notificationを発火する
            if UserDefaultsManager.getPeriodicNotificationWeekday() == nil {
                // 今より未来だったら
                if NSDate().compare(fireDateOfNotificaiton!) == NSComparisonResult.OrderedAscending {
                    let notification = UILocalNotification()
                    notification.fireDate = fireDateOfNotificaiton // 年月日・曜日を指定
                    notification.timeZone = NSTimeZone.localTimeZone()
                    notification.alertBody = "今日の計測時間です。心拍を計測しましょう。"
                    notification.soundName = UILocalNotificationDefaultSoundName
                    UIApplication.sharedApplication().scheduleLocalNotification(notification)
                }
            
            /*** 曜日 ***/
            //登録してある曜日とfireDateの曜日が同じだったら
            } else if UserDefaultsManager.getPeriodicNotificationWeekday() == componentsForFireDate.weekday {
                // 今より未来だったら
                if NSDate().compare(fireDateOfNotificaiton!) == NSComparisonResult.OrderedAscending {
                    let notification = UILocalNotification()
                    notification.fireDate = fireDateOfNotificaiton // 年月日・曜日を指定
                    notification.timeZone = NSTimeZone.localTimeZone()
                    notification.alertBody = "今日の計測時間です。心拍を計測しましょう。"
                    notification.soundName = UILocalNotificationDefaultSoundName
                    UIApplication.sharedApplication().scheduleLocalNotification(notification)
                }
            }
        }
    }
}

//
//  UserInfoRouter.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/11/06.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import Alamofire

/* ログイン情報のルーター 
Routerの設計はAlamofireのサンプルから。
*/
enum UserInfoRouter: URLRequestConvertible {
    case Register(authType: String, id: String)
    case GetSelf(authType: String, id:String)
    case AttachAuthInfo(authType: String, id: String)
    case DetachAuthInfo(authType: String, id: String)
    
    private var URLString: String {
        return "http://stress-diary.appspot.com/rpc.json"
    }
    
    private var HTTPBody: NSData? {
        switch self {
        case .Register(let authType, let id):
            let values = [
                "method": "user.register",
                "id": 1,
                "params":[[
                    "authType": authType,
                    "id": id,
                    "authHash": "H2mRVD7C\(authType)Adpy_jEA\(id)bgjsewn2".sha256String().sha256String().sha256String()
                    ]
                ]
            ]

            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
        case .GetSelf(let authType, let id):
            let values = [
                "method": "user.getSelf",
                "id": 1,
                "params":[[
                    "authType": authType,
                    "id": id,
                    "authHash": "H2mRVD7C\(authType)Adpy_jEA\(id)bgjsewn2".sha256String().sha256String().sha256String()
                    ]
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
        case .AttachAuthInfo(let authType, let id):
            let values = [
                "method": "user.attachAuthInfo",
                "id": 1,
                "params":[[
                    "authType": authType,
                    "id": id,
                    "authHash": "H2mRVD7C\(authType)Adpy_jEA\(id)bgjsewn2".sha256String().sha256String().sha256String()
                    ]
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
        case .DetachAuthInfo(let authType, let id):
            let values = [
                "method": "user.detachAuthInfo",
                "id": 1,
                "params":[[
                    "authType": authType,
                    "id": id,
                    "authHash": "H2mRVD7C\(authType)Adpy_jEA\(id)bgjsewn2".sha256String().sha256String().sha256String()
                    ]
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
        }
    }
    
    // TODO: HeaderはUserDefaults管理。currentAuthIDからとってくる！
    private var HTTPHeaderValue: String? {
        switch self {
        case .Register:
            return nil
        case .GetSelf(let authType, let id):
            let values = [
                "method": "user.getSelf",
                "id": 1,
                "params":[[
                    "authType": authType,
                    "id": id,
                    "authHash": "H2mRVD7C\(authType)Adpy_jEA\(id)bgjsewn2".sha256String().sha256String().sha256String()
                    ]
                ]
            ]
            let JSON = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
            return String(data: JSON, encoding: NSUTF8StringEncoding)
            
        default:
            let currentAuthType = KeychainManager.getCurrentAuthType()!
            let currentAuthID: String = KeychainManager.getCurrentAuthID()!
            
            let values = [
                "authType": currentAuthType,
                "id": currentAuthID,
                "authHash": "H2mRVD7C\(currentAuthType)Adpy_jEA\(currentAuthID)bgjsewn2".sha256String().sha256String().sha256String()
            ]
            let JSON = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
            return String(data: JSON, encoding: NSUTF8StringEncoding)
        }
    }
    
    var URLRequest: NSMutableURLRequest {
        let mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: URLString)!)
        mutableURLRequest.HTTPMethod = Alamofire.Method.POST.rawValue
        mutableURLRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        mutableURLRequest.HTTPBody = HTTPBody
        
        if let header = HTTPHeaderValue {
            mutableURLRequest.addValue(header, forHTTPHeaderField: "X-Stress-Diary-Authentication")
        }
        
        return mutableURLRequest
    }
}
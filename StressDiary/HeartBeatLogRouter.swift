//
//  HeartBeatLogRouter.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/18.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import Alamofire

/* 心拍データ関連のルーター */
enum HeartBeatLogRouter: URLRequestConvertible {
    case Add(data:HeartbeatLogData)
    case AddAll(datas: [[String: AnyObject]])
    case Delete(id:Int)
    case UpdateTags(id:Int, tags:[String])
    case UpdateMemo(id:Int, memo:String)
    case Find(id:Int)
    case FindLast()
    case FindAll()
    case FindBy(from:NSDate,to:NSDate,tags:[String])
    case FindByCursor(cursor:String,filter:String,sorts:String)
    
    private var URLString: String {
        return "http://stress-diary.appspot.com/rpc.json"
    }
    
    private var HTTPBody: NSData? {
        switch self {
        case .Add(let data):
            let values = [
                "method": "heartbeatLog.add",
                "id": 1,
                "params":[[
                    "measureDatetime": getSendAPIDateFormat(data.measureDatetime),
                    "rateBpm": data.rateBpm,
                    "stressIndex":data.stressIndex,
                    "tags":data.tags,
                    "memo":data.memo
                    ]
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
        case .AddAll(let datas):
            let values = [
                "method": "heartbeatLog.addAll",
                "id": 1,
                "params":[datas]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
        case .Delete(let id):
            let values = [
                "method": "heartbeatLog.delete",
                "id": 1,
                "params":[
                    [ id ]
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
        case .UpdateTags(let id, let tags):
            let values = [
                "method": "heartbeatLog.updateTags",
                "id": 1,
                "params":[
                    [ id ],[ tags ]
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
        case .UpdateMemo(let id, let memo):
            let values = [
                "method": "heartbeatLog.updateMemo",
                "id": 1,
                "params":[
                    [ id ],[ memo ]
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
        case .Find(let id):
            let values = [
                "method": "heartbeatLog.find",
                "id": 1,
                "params":[
                    [ id ]
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
        case .FindLast():
            let values = [
                "method": "heartbeatLog.findLast",
                "id": 1,
                "params":[
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
        
        case .FindAll():
            let values = [
                "method": "heartbeatLog.findBy",
                "id": 1,
                "params":[
                    [],[],[]
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
        case .FindBy(let from, let to, let tags):
            let values = [
                "method": "heartbeatLog.findBy",
                "id": 1,
                "params":[
                    [ getSendAPIDateFormat(from) ],[ getSendAPIDateFormat(to) ],[ tags ]
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
        case .FindByCursor(let cursor, let filter, let sorts):
            let values = [
                "method": "heartbeatLog.findByCursor",
                "id": 1,
                "params":[
                    [ cursor ],[ filter ],[ sorts ]
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
        }
    }
    
    // TODO: HeaderはUserDefaults管理。currentAuthIDからとってくる！
    private var HTTPHeaderValue: String? {
        switch self {
        default:
            let currentAuthType = KeychainManager.getCurrentAuthType()!
            var currentAuthID: String = ""
            
            if currentAuthType == "DEVICE" {
                currentAuthID = KeychainManager.getOriginalID()!
            } else if currentAuthType == "FACEBOOK" {
                currentAuthID = KeychainManager.getFacebookID()!
            } else if currentAuthType == "TWITTER" {
                currentAuthID = KeychainManager.getTwitterID()!
            }
            
            let values = [
                "authType": currentAuthType,
                "id": currentAuthID,
                "authHash": "H2mRVD7C\(currentAuthType)Adpy_jEA\(currentAuthID)bgjsewn2".sha256String().sha256String().sha256String()
            ]
            let JSON = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
            return String(data: JSON, encoding: NSUTF8StringEncoding)
        }
    }
    
    var URLRequest: NSMutableURLRequest {
        let mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: URLString)!)
        mutableURLRequest.HTTPMethod = Alamofire.Method.POST.rawValue
        mutableURLRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        mutableURLRequest.HTTPBody = HTTPBody
        
        if let header = HTTPHeaderValue {
            mutableURLRequest.addValue(header, forHTTPHeaderField: "X-Stress-Diary-Authentication")
        }
        
        return mutableURLRequest
    }
}
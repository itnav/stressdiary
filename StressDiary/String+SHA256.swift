//
//  String+SHA256.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/11/09.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation

extension String {
    func sha256String() -> String {
        let cstr = self.cStringUsingEncoding(NSUTF8StringEncoding)
        let data = NSData(bytes: cstr!, length: self.characters.count)
        
        var digest = [UInt8](count: Int(CC_SHA256_DIGEST_LENGTH), repeatedValue: 0)
        
        CC_SHA256(data.bytes, CC_LONG(data.length), &digest)
        
        let output = NSMutableString(capacity: 64)
        for var i=0; i<32; i++ {
            output.appendFormat("%02x", digest[i])
        }
        
        return output as String
    }
}
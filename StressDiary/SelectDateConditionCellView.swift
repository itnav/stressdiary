//
//  SelectDateConditionCellView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/17.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

class SelectDateConditionCellView: UICollectionViewCell {
    
    @IBOutlet weak var dateLabel:UILabel!
    
    private let xibName:String = "SelectDateConditionCellView"
    
    internal var date:NSDate!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.selectedBackgroundView?.backgroundColor = UIColor.strongOrangeColor()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
//        if let view:UIView = NSBundle.mainBundle().loadNibNamed(xibName, owner:self, options: nil).first as? UIView {
//            view.frame = self.bounds
//            self.addSubview(view)
//        }
    }

}

//
//  AdviceMessageRouter.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/19.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import Alamofire

/* 測定結果のアドバイス取得のルーター */
enum AdviceMessageRouter: URLRequestConvertible {
    
    case FindByStressIndex(stressIndex:Double)
    
    private var URLString: String {
        return "http://stress-diary.appspot.com/rpc.json"
    }
    
    private var HTTPBody: NSData? {
        switch self {
        case .FindByStressIndex(let stressIndex):
            let values = [
                "method": "advice.findByStressIndex",
                "id": 1,
                "params":[
                    [ stressIndex ]
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
        }
    }
    
    private var HTTPHeaderValue: String? {
        switch self {
        default:
            let currentAuthType = KeychainManager.getCurrentAuthType()!
            let currentAuthID: String = KeychainManager.getCurrentAuthID()!
            
            let values = [
                "authType": currentAuthType,
                "id": currentAuthID,
                "authHash": "H2mRVD7C\(currentAuthType)Adpy_jEA\(currentAuthID)bgjsewn2".sha256String().sha256String().sha256String()
            ]
            let JSON = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
            return String(data: JSON, encoding: NSUTF8StringEncoding)
        }
    }
    
    var URLRequest: NSMutableURLRequest {
        let mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: URLString)!)
        mutableURLRequest.HTTPMethod = Alamofire.Method.POST.rawValue
        mutableURLRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        mutableURLRequest.HTTPBody = HTTPBody
        
        if let header = HTTPHeaderValue {
            mutableURLRequest.addValue(header, forHTTPHeaderField:"X-Stress-Diary-Authentication")
        }
        
        return mutableURLRequest
    }
}

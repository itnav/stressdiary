//
//  UserDefaultsManager.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/11/19.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation

/* UserDefaultsの管理
初回ログインかどうかの管理をしている。
LocalNotificationの管理をしている。
*/
struct UserDefaultsManager {
    // conatants
    private struct UserDefaultsKey {
        static let LastBaseHeartRateMeasureDate = "LastBaseHeartRateMeasureDate"
        static let BaseHeartRate = "BaseHeartRate"
    }
    
    static func isFirstLogin() -> Bool {
        let ud = NSUserDefaults.standardUserDefaults()
        if ud.objectForKey("firstLogin") == nil {
            return true
        }
        return false
    }
    
    static func setFirstLogin() {
        let ud = NSUserDefaults.standardUserDefaults()
        ud.setObject(true, forKey: "firstLogin")
    }
    
    
    // MARK: -PeriodicNotification
    static func setPeriodicNotificationWeekday(weekday: Int?) {
        let ud = NSUserDefaults.standardUserDefaults()
        ud.setObject(weekday, forKey: "PeriodicNotificationWeekday")
    }
    
    static func getPeriodicNotificationWeekday() -> Int? {
        return NSUserDefaults.standardUserDefaults().objectForKey("PeriodicNotificationWeekday") as? Int
    }
    
    static func setPeriodicNotificationTime(hour: Int, minutes: Int) {
        let ud = NSUserDefaults.standardUserDefaults()
        ud.setObject(hour, forKey: "PeriodicNotificationTimeHour")
        ud.setObject(minutes, forKey: "PeriodicNotificationTimeMinutes")
    }
    
    static func getPeriodicNotificationTimeHour() -> Int {
        return NSUserDefaults.standardUserDefaults().objectForKey("PeriodicNotificationTimeHour") as! Int
    }
    
    static func getPeriodicNotificationTimeMinutes() -> Int {
        return NSUserDefaults.standardUserDefaults().objectForKey("PeriodicNotificationTimeMinutes") as! Int
    }
    
    static func removePeriodicNotificationWeekday() {
        let ud = NSUserDefaults.standardUserDefaults()
        ud.removeObjectForKey("PeriodicNotificationWeekday")
    }
    
    static func setLastBatchDateString(dateString:String) {
        let ud = NSUserDefaults.standardUserDefaults()
        ud.setObject(dateString, forKey: "LastBatchDateString")
    }
    
    static func getLastBatchDateString() -> String? {
        return NSUserDefaults.standardUserDefaults().objectForKey("LastBatchDateString") as? String
    }
    
    /// 基本心拍を更新した日を取得
    static func getLastBaseHeartRateUpdatedDate() -> String? {
        NSUserDefaults.standardUserDefaults().registerDefaults([UserDefaultsKey.LastBaseHeartRateMeasureDate: NSDate.init(timeIntervalSince1970: 0)])
        return NSUserDefaults.standardUserDefaults().stringForKey(UserDefaultsKey.LastBaseHeartRateMeasureDate)
    }
    
    /// 基本心拍を更新した日を記録
    static func setLastBaseHeartRateUpdatedDate(date: String) {
        NSUserDefaults.standardUserDefaults().setObject(date, forKey: UserDefaultsKey.LastBaseHeartRateMeasureDate)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    /// 基本心拍を取得
    static func getBaseHeartRate() -> Int? {
        NSUserDefaults.standardUserDefaults().registerDefaults([UserDefaultsKey.BaseHeartRate: 60])
        return NSUserDefaults.standardUserDefaults().integerForKey(UserDefaultsKey.BaseHeartRate)
    }
    
    /// 基本心拍を記録
    static func setBaseHeartRate(heartRate: Int) {
        NSUserDefaults.standardUserDefaults().setInteger(heartRate, forKey: UserDefaultsKey.BaseHeartRate)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
}
//
//  EditTagButtonCell.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/21.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

class EditTagButtonCell: UICollectionViewCell {
    
    @IBOutlet weak var addNewTagButton:UIButton!
    
    @IBAction func addNewTag(){
        print("add")
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    override func awakeFromNib() {
        addNewTagButton.layer.borderColor = UIColor.strongOrangeColor().CGColor
        addNewTagButton.layer.borderWidth = 2
        addNewTagButton.layer.cornerRadius = 25.0
    }
}

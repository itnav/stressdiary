//
//  SelectDataOptionButton.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/27.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

protocol SelectDataOptionDelegate {
    func onTapButton(sender:SelectDataOptionButton)
}
class SelectDataOptionButton: UIView {
    
    @IBOutlet weak var icon:UIImageView!
    @IBOutlet weak var noSelectLabel:UILabel!
    @IBOutlet weak var collectionView:UIView!
    
    // 表示するデータのセット.
    private var data:[AnyObject] = []
    
    var delegate:SelectDataOptionDelegate?
    
    
    enum DataType {
        case calendar
        case tag
        case memo
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupXib()
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupXib()
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        delegate?.onTapButton(self)
    }
    
    private func sizeOfInfoView()->CGSize{
        return collectionView.frame.size
    }
    /*
    * xibのレイアウトファイルの読み込み.
    */
    private func setupXib(){
        
        if let view:UIView = NSBundle.mainBundle().loadNibNamed("SelectDataOptionButton", owner:self, options: nil).first as? UIView {
            view.frame = self.bounds
            view.layer.cornerRadius = self.frame.height/2
            self.addSubview(view)
        }
    }
    internal func setSelectData(type:DataType,data:[AnyObject]) {
        
        // dataを一度初期化.
        self.data = []
        
        self.data = data
        
        // collectionViewに存在するすべての子Viewを削除しておく.
        for view in collectionView.subviews {
            if view != noSelectLabel {
                view.removeFromSuperview()
            }
        }
        
        // 帰ってきたデータを種類ごとに整形する.
        switch type {
        case .calendar:
            // AnyObject -> NSDate
            // dataを一度日付順に並び替える.
            print(self.data)
            var sort_data = sortDateData(self.data)
            
            var dataStr:String = ""
            
            let label:UILabel = UILabel(frame:CGRectMake(0,0,self.collectionView.frame.width,self.collectionView.frame.height))
            label.textColor = UIColor.strongOrangeColor()
            label.textAlignment = NSTextAlignment.Center
            label.font = UIFont.systemFontOfSize(13.0)
            
            let format:NSDateFormatter = NSDateFormatter()
            
            if sort_data.count <= 2 {
                for var i:Int=0; i<sort_data.count; i++ {
                    if i == 0 {
                        format.dateFormat = "yyyy/M/d"
                        dataStr.appendContentsOf(format.stringFromDate(sort_data[i]))
                    }else {
                        if sort_data[i].getYear() != sort_data[i-1].getYear(){
                            format.dateFormat = "yyyy/M/d"
                            dataStr.appendContentsOf(format.stringFromDate(sort_data[i]))
                        }else if sort_data[i].getMonth() != sort_data[i-1].getMonth() {
                            format.dateFormat = "M/d"
                            dataStr.appendContentsOf(format.stringFromDate(sort_data[i]))
                        }else {
                            format.dateFormat = "d"
                            dataStr.appendContentsOf(format.stringFromDate(sort_data[i]))
                        }
                    }
                    if i != sort_data.count-1 && sort_data.count != 1{
                        dataStr.appendContentsOf(",")
                    }
                }
            }else {
                // 前のデータが連続だったかのフラグ.
                var continuityFlag:Bool = false
                
                
                for var i:Int=0; i<sort_data.count; i++ {
                    var editFlag:Bool = true
                    if i != 0 {
                        // iが0ではない時は、日付描画の前に記号が入る.
                        if (sort_data[i].timeIntervalSince1970 - sort_data[i-1].timeIntervalSince1970) <= 24*60*60{
                            
                            if i == sort_data.count-1{
                                if continuityFlag {
                                    dataStr.appendContentsOf("-")
                                }else {
                                    dataStr.appendContentsOf(",")
                                }
                            }else if (sort_data[i+1].timeIntervalSince1970 - sort_data[i].timeIntervalSince1970) > 24*60*60 {
                                // 次の日は連続ではない.
                                if continuityFlag {
                                    dataStr.appendContentsOf("-")
                                }else {
                                    dataStr.appendContentsOf(",")
                                }
                            }else {
                                // 次の日も連続.
                                editFlag = false
                            }
                            continuityFlag = true
                        }else {
                            // 前の日が連続の日付じゃない.
                            dataStr.appendContentsOf(",")
                            continuityFlag = false
                        }
                    }
                    
                    // 日付のデータを描画.
                    if i == 0 {
                        format.dateFormat = "yyyy/M/d"
                        dataStr.appendContentsOf(format.stringFromDate(sort_data[i]))
                        
                    }else {
                        if editFlag {
                            if sort_data[i].getYear() != sort_data[i-1].getYear(){
                                format.dateFormat = "yyyy/M/d"
                                dataStr.appendContentsOf(format.stringFromDate(sort_data[i]))
                            }else if sort_data[i].getMonth() != sort_data[i-1].getMonth() {
                                format.dateFormat = "M/d"
                                dataStr.appendContentsOf(format.stringFromDate(sort_data[i]))
                            }else {
                                format.dateFormat = "d"
                                dataStr.appendContentsOf(format.stringFromDate(sort_data[i]))
                            }
                        }
                    }
                }
            }
            
            
            label.text = dataStr
            self.collectionView.addSubview(label)
            
            break
        case .tag:
            // AnyObject -> String
            
            // 前のタグを表示したx座標をとっておくための変数.
            var pos_x:CGFloat = 0.0
            let margin:CGFloat = 15.0
            
            for_data:for d in self.data{
                
                let labelWidth = calculationLabelWidth(d as! String)
                
                if pos_x + margin + labelWidth < self.collectionView.frame.width - calculationLabelWidth("..."){
                    
                    let label:UILabel = UILabel(frame: CGRectMake(pos_x+margin,0,labelWidth+margin,self.collectionView.frame.height*0.8))
                    
                    label.layer.masksToBounds = true
                    label.layer.cornerRadius = self.collectionView.frame.height*0.4
                    
                    label.center.y = self.collectionView.frame.height/2
                    label.text = d as? String
                    label.font = UIFont.systemFontOfSize(13.0)
                    label.backgroundColor = UIColor.strongOrangeColor()
                    label.textColor = UIColor.whiteColor()
                    label.textAlignment = NSTextAlignment.Center
                    
                    self.collectionView.addSubview(label)
                    pos_x += margin+labelWidth
                    
                }else {
                    let labelWidth = calculationLabelWidth("...")
                    
                    let label:UILabel = UILabel(frame:CGRectMake(pos_x+margin,0,labelWidth,self.collectionView.frame.height*0.8))
                    label.center.y = self.collectionView.frame.height/2
                    label.textColor = UIColor.strongOrangeColor()
                    label.font = UIFont.systemFontOfSize(13.0)
                    label.text = "..."
                    self.collectionView.addSubview(label)
                    
                    break for_data
                }
            }
            break
        case .memo:
            // AnyObject -> String
            // memoだったら一つしかこないはず。
            let label:UILabel = UILabel(frame:CGRectMake(0,0,self.collectionView.frame.width,self.collectionView.frame.height))
            label.textColor = UIColor.strongOrangeColor()
            label.font = UIFont.systemFontOfSize(13.0)
            label.text = data[0] as? String
            break
        }
        
        // データがセットされている状態の時にはラベルを隠す.
        if data.count > 0 {
            noSelectLabel.hidden = true
        }else {
            noSelectLabel.hidden = false
        }
    }
    /*
    * 日付のデータを一度並び替えるためのメソッド.
    */
    func sortDateData(data:[AnyObject])->[NSDate]{
        
        var tmp_data:[NSDate] = []
        
        var max_value:NSDate!
        var min_value:NSDate!
        
        for d in data {
            
            if tmp_data.count == 0 {
                // まだtmp_dataの中にデータが存在していなかったら、データを入れる.
                tmp_data.append(d as! NSDate)
                
                max_value = d as! NSDate
                min_value = d as! NSDate
                
            }else if tmp_data.indexOf(d as! NSDate) == nil{
                
                if max_value.compare(d as! NSDate) == NSComparisonResult.OrderedAscending {
                    
                    // 最大値より未来の場合は最後に追加.
                    tmp_data.append(d as! NSDate)
                    max_value = d as! NSDate
                    
                }else if min_value.compare(d as! NSDate) == NSComparisonResult.OrderedDescending {
                    
                    // 最小値より過去の場合は0に追加.
                    tmp_data.insert(d as! NSDate, atIndex: 0)
                    min_value = d as! NSDate
                    
                }else {
                    for_sort_date:for i in 1..<tmp_data.count{
                        
                        if tmp_data[i-1].compare(d as! NSDate) == NSComparisonResult.OrderedDescending
                            && tmp_data[i].compare(d as! NSDate) == NSComparisonResult.OrderedAscending {
                                // それ以外の場合、iが過去、i+1が未来になるまでforを回し、i+1にinsertする.
                                tmp_data.insert(d as! NSDate, atIndex: i)
                                break for_sort_date
                                
                        }
                    }
                }
            }
        }
        return tmp_data
    }
    /*
    * ラベルの横幅を計算するメソッド.
    */
    func calculationLabelWidth(str:String)->CGFloat{
        let label = UILabel()
        label.font = UIFont.systemFontOfSize(13.0)
        label.text = str
        label.sizeToFit()
        return label.frame.width
    }
    
    @IBInspectable var iconImage: UIImage? {
        didSet {
            icon.image = iconImage
        }
    }
    @IBInspectable var defaultTitle:String = ""{
        didSet {
            noSelectLabel.text = defaultTitle
        }
    }
    
}

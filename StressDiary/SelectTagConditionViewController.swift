//
//  SelectTagConditionViewController.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/12.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

/* タグ選択のVC */
class SelectTagConditionViewController: UIViewController {
    
    var profile:UserProfileData = UserProfileData()
    
    @IBAction func editTagAction(){
        
        if (self.view as! SelectTagConditionView).editMode {
            
            self.navigationItem.rightBarButtonItem?.enabled = true
            // TODO: もともとデフォルトを黒にすることで対応
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.blueColor()
            
            if (self.view as! SelectTagConditionView).tagList.count > 20 {
                // 保存しようとしているリストの数か20より上の時は、アラートで警告.
                let alert:UIAlertController = UIAlertController(title:"タグの保存に失敗しました",
                    message: "タグの保存上限数は20個です.",
                    preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction:UIAlertAction = UIAlertAction(title: "ok",style: UIAlertActionStyle.Default,handler:nil)
                alert.addAction(okAction)
                self.presentViewController(alert, animated: true, completion: nil)
            }else {
                // 編集したタグを保存.
                profile.defaultTags = (self.view as! SelectTagConditionView).tagList
                
                UserProfile.updateUserProfile(profile).then{ data in
                    self.didSaveTagList(data)
                }
            }
        } else {
            self.navigationItem.rightBarButtonItem?.enabled = false
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.clearColor()
        }
        
        (self.view as! SelectTagConditionView).setEditTagMode()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        self.view.backgroundColor = UIColor.whiteColor()
        (self.view as! SelectTagConditionView).parentViewControlelr = self
        
        UserProfile.getUserProfile().then { data in
            self.setUserInfo(data)
        }
    }
    
    private func setupNavigationBar() {
        
        // NavigationBarの下にViewが埋まってしまうことを回避
        edgesForExtendedLayout = UIRectEdge.None;
        title = "タグを選択する"
        
        // NavigationBarに完了ボタンを追加.
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "完了", style: UIBarButtonItemStyle.Plain, target:self, action: "onTouchCompliteButton")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
    }
    
    override func awakeFromNib() {
        
    }
    func setUserInfo(data:UserProfileData){
        profile = data
        (self.view as! SelectTagConditionView).setUserTagList(data.defaultTags)
    }
    func didSaveTagList(success:Bool) {
        if success {
            print("保存成功")
        }else {
            // 保存失敗の時にはアラートを出す.
            let alert:UIAlertController = UIAlertController(title:"タグの保存に失敗しました",
                message: "インターネットの状況を確認して再度編集をしてみてください",
                preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction:UIAlertAction = UIAlertAction(title: "ok",style: UIAlertActionStyle.Default,handler:nil)
            alert.addAction(okAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func onTouchCompliteButton(){
        
        // 一つ前のVCのindexを取得
        let index:Int = (self.navigationController?.viewControllers.count)!-2
        
        if let historyVC:HistoryViewController = self.navigationController?.viewControllers[index] as? HistoryViewController {
            historyVC.setTagCondition((self.view as! SelectTagConditionView).selectTags)
            self.navigationController?.popToViewController(historyVC, animated: true)
        }else if let measureResultVC:MeasureResultViewController = self.navigationController?.viewControllers[index] as? MeasureResultViewController {
            measureResultVC.setTagCondition((self.view as! SelectTagConditionView).selectTags)
            self.navigationController?.popToViewController(measureResultVC, animated: true)
        }else {
            // そのほかだったら普通に戻る(設定?).
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    internal func setSelectedTag(data:[String]){
        (self.view as! SelectTagConditionView).selectTags = data
    }
}
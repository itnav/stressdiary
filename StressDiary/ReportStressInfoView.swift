//
//  ReortStressInfoView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/12.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

class ReportStressInfoView: UIView {
    
    let xibName:String = "ReportStressInfoView"
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupXib()
    }
    
    private func setupXib(){
        if let view:UIView = NSBundle.mainBundle().loadNibNamed(xibName, owner:self, options: nil).first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
        }
    }
    
}
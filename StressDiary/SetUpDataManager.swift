//
//  SetUpDataManager.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/12/06.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import SVProgressHUD

/* 大きいデータの受信を、起動時に一気に行うためのもの.
NewsVCで表示を最新のものにするため
*/
class SetUpDataManager {
    // constants
    static let notificationNameCompleteDownload = "CompleteDownload.SetUpDataManager"
    
    enum SetUpDataType {
        case Graph_Day
        case Graph_Week
        case Graph_Month
        case FindAllData
    }
    
    var graph_data_day:SearchResultData = SearchResultData()
    var graph_data_week:SearchResultData = SearchResultData()
    var graph_data_month:SearchResultData = SearchResultData()
    
    var find_all_data:SearchResultData = SearchResultData()
    
    // シングルトンで一回の起動中のみデータを管理するように.
    class var sharedInstance:SetUpDataManager{
        struct Static{
            static let instance:SetUpDataManager = SetUpDataManager()
        }
        return Static.instance
    }
    
    private init(){}
    
    // 必要なデータをダウンロード.
    func setUpData(){
        
        // 一度配列の中身をなくしておく.
        graph_data_day = SearchResultData()
        graph_data_week = SearchResultData()
        graph_data_month = SearchResultData()
        find_all_data = SearchResultData()
        
        // グラフデータのダウンロード.
        loadData(.Graph_Day)
        loadData(.Graph_Week)
        loadData(.Graph_Month)
        
        // 全件データの取得.
        HeartBeatLog.FindAll().then{ data in
            self.find_all_data.appendSearchResult(data)
        }
        
        NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(SetUpDataManager.checkSetUpdata(_:)), userInfo: nil, repeats: true)
    }
    
    func setUpNewsData() {
        // 一度配列の中身をなくしておく.
        graph_data_week = SearchResultData()
        
        // グラフデータのダウンロード.
        loadData(.Graph_Week)
        
        NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(SetUpDataManager.checkSetUpNewsData(_:)), userInfo: nil, repeats: true)
    }
    
    func isDownLoadedAllData()->Bool{
        
        // すべてのデータが揃ったか確認する.
        if graph_data_day.terminal == true && graph_data_week.terminal == true && graph_data_month.terminal == true && find_all_data.cursor != "" {
            return true
        }else {
            return false
        }
    }
    
    func appendData(type:SetUpDataType,data:SearchResultData){
        switch type {
        case .Graph_Day:
            
            self.graph_data_day.appendSearchResult(data)
            
            // もしもterminalまで取得できていなかったら、再度loadを呼びに行く.
            if data.terminal == false {
                loadData(type)
            }
            
            break
        case .Graph_Week:
            self.graph_data_week.appendSearchResult(data)
            if data.terminal == false {
                loadData(type)
            }
            break
        case .Graph_Month:
            self.graph_data_month.appendSearchResult(data)
            if data.terminal == false {
                loadData(type)
            }
            break
        case .FindAllData:
            
            break
        }
        
    }
    func getDataRange(type:SetUpDataType)->DataRange!{
        
        let toDay:NSDate = NSDate()
        switch type {
        case .Graph_Day:
            return DataRange(start: NSDate(timeIntervalSinceNow: -24*60*60), end: toDay)

//            return DataRange(start: NSDate(timeInterval: 60*60, sinceDate: NSDate.getDate(toDay.getYear(), month: toDay.getMonth(), day: toDay.getDay()-1)), end: NSDate(timeInterval: 60*60, sinceDate: NSDate.getDate(toDay.getYear(), month: toDay.getMonth(), day: toDay.getDay()+1)))

        case .Graph_Week:
            return DataRange(start: NSDate(timeInterval: -9*60*60, sinceDate: NSDate.getDate(toDay.getYear(), month: toDay.getMonth(), day: toDay.getDay()-6)), end: NSDate(timeInterval: -9*60*60, sinceDate: NSDate.getDate(toDay.getYear(), month: toDay.getMonth(), day: toDay.getDay()+1)))
//            return DataRange(start: NSDate().getDate(toDay.getYear(), month: toDay.getMonth(), day: toDay.getDay()-6), end: NSDate().getDate(toDay.getYear(), month: toDay.getMonth(), day: toDay.getDay()+1))
            
        case .Graph_Month:
            return DataRange(start: NSDate(timeInterval: -9*60*60, sinceDate: NSDate.getDate(toDay.getYear(), month: toDay.getMonth(), day: toDay.getDay()-29)), end: NSDate(timeInterval: -9*60*60, sinceDate: NSDate.getDate(toDay.getYear(), month: toDay.getMonth(), day: toDay.getDay()+1)))
            
//            return DataRange(start: NSDate(timeInterval: -9*60*60, sinceDate: NSDate.getDate(toDay.getYear(), month: toDay.getMonth(), day: 1)), end: NSDate(timeInterval: -9*60*60, sinceDate: NSDate.getDate(toDay.getYear(), month: toDay.getMonth(), day: NSDate.getLastDayOfMonth(toDay.getYear(), month: toDay.getMonth()))))
            
//            return DataRange(start: NSDate().getDate(toDay.getYear(), month: toDay.getMonth(), day: 1), end: NSDate().getDate(toDay.getYear(), month: toDay.getMonth(), day: toDay.getLastDayOfMonth(toDay.getYear(), month: toDay.getMonth())))
        case .FindAllData:
            return nil
        }
    }
    func loadData(type:SetUpDataType){
        // HeartBeatLogDataの全件を取得. -> report,history
        let data_range = getDataRange(type)
        switch type {
        case .Graph_Day:
            if graph_data_day.cursor != "" {
                HeartBeatLog.FindByCursor(graph_data_day.cursor, filter: graph_data_day.filter, sorts: graph_data_day.sorts).then{ data in
                    self.appendData(type, data: data)
                }
            }else {
                HeartBeatLog.FindBy(data_range.start, to: data_range.end, tags: []).then { data in
                    self.appendData(type, data: data)
                }
            }
            break
        case .Graph_Week:
            if graph_data_week.cursor != "" {
                HeartBeatLog.FindByCursor(graph_data_week.cursor, filter: graph_data_week.filter, sorts: graph_data_week.sorts).then{ data in
                    self.appendData(type, data: data)
                }
            }else {
                HeartBeatLog.FindBy(data_range.start, to: data_range.end, tags: []).then { data in
                    self.appendData(type, data: data)
                }
            }
            break
        case .Graph_Month:
            if graph_data_month.cursor != "" {
                HeartBeatLog.FindByCursor(graph_data_month.cursor, filter: graph_data_month.filter, sorts: graph_data_month.sorts).then{ data in
                    self.appendData(type, data: data)
                }
            }else {
                HeartBeatLog.FindBy(data_range.start, to: data_range.end, tags: []).then { data in
                    self.appendData(type, data: data)
                }
            }
            break
        case .FindAllData:
            break
        }
        
    }
    
    @objc func checkSetUpdata(sender:NSTimer){
        if SetUpDataManager.sharedInstance.isDownLoadedAllData() {
            saveStatisticsHeartRate(sender)
        }
    }
    
    @objc func checkSetUpNewsData(sender: NSTimer) {
        if SetUpDataManager.sharedInstance.graph_data_week.terminal! {
            saveStatisticsHeartRate(sender)
        }
    }
    
    private func saveStatisticsHeartRate(sender: NSTimer) {
        print("データの読み込み終了")
        // 一週間のデータから、心拍数の高低平均を算出,udに登録しておく.
        var sum:Float = 0.0
        var max_bpm:Int = 0
        var min_bpm:Int = SetUpDataManager.sharedInstance.graph_data_week.result.count > 0 ? (SetUpDataManager.sharedInstance.graph_data_week.result.first?.rateBpm)! : 0
        for d in SetUpDataManager.sharedInstance.graph_data_week.result {
            if max_bpm < d.rateBpm {
                max_bpm = d.rateBpm
            }
            if min_bpm > d.rateBpm {
                min_bpm = d.rateBpm
            }
            sum = sum+Float(d.rateBpm)
        }
        
        BaseHeartRateManager.setStatisticsHeartRate(.Low, value: min_bpm)
        BaseHeartRateManager.setStatisticsHeartRate(.High, value: max_bpm)
        
        // Int(sum/Float(SetUpDataManager.sharedInstance.graph_data_week.result.count))
        // 0割りしようとするとそれは無理なので、仮として0だった場合は1で割るようにした
        if SetUpDataManager.sharedInstance.graph_data_week.result.count == 0 {
            BaseHeartRateManager.setStatisticsHeartRate(.Ave, value: Int(sum/Float(1)))
        } else {
            BaseHeartRateManager.setStatisticsHeartRate(.Ave, value: Int(sum/Float(SetUpDataManager.sharedInstance.graph_data_week.result.count)))
        }
        
        sender.invalidate()
        SVProgressHUD.dismiss()
        
        print("load data - day : \(SetUpDataManager.sharedInstance.graph_data_day.result.count)")
        print("load data - week : \(SetUpDataManager.sharedInstance.graph_data_week.result.count)")
        print("load data - month : \(SetUpDataManager.sharedInstance.graph_data_month.result.count)")
        
        print(sender.description)
        
        NSNotificationCenter.defaultCenter().postNotificationName(SetUpDataManager.notificationNameCompleteDownload, object: self)
    }
}

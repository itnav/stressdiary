//
//  NSDateFormatter+CustomFormat.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/24.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation

extension NSDateFormatter {
    
    func getOnlyWeekFormat()->NSDateFormatter{
        self.locale = NSLocale(localeIdentifier:"en")
        self.dateFormat = "EEE"
//        self.timeZone = NSTimeZone(abbreviation: "JST")
        return self
    }
    func getFormatOnlyDay()->NSDateFormatter {
//        self.timeZone = NSTimeZone(abbreviation: "JST")
        self.dateFormat = "d"
        return self
    }
    func getMonthAndDayFormat()->NSDateFormatter{
//        self.timeZone = NSTimeZone(abbreviation: "JST")
        self.dateFormat = "MM/dd"
        return self
    }
    
    func getFormatWidthOutTime()->NSDateFormatter{
//        self.timeZone = NSTimeZone(abbreviation: "JST")
        self.dateFormat = "yyyy/MM/dd"
        return self
    }
    
    func getAllElementFormate()->NSDateFormatter{
//        self.timeZone = NSTimeZone(abbreviation: "JST")
        self.dateFormat = "yyyy/MM/dd HH:mm:ss"
        return self
    }
    func getFullTimeFormat()->NSDateFormatter {
        self.dateFormat = "HH:mm:ss"
        return self
    }
    func getTimeWidthOutSecoundFormat()->NSDateFormatter {
        self.dateFormat = "HH:mm"
        return self
    }
}
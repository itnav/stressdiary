//
//  SelectDateConditionView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/12.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

struct MonthData {
    
    var year:Int
    var month:Int
    // 何曜日から始まるか.
    var startIndex:Int
    // 何日まであるか.
    var lastDay:Int
    
    init(){
        year = 0
        month = 0
        startIndex = 0
        lastDay = 0
    }

}

/* 日付選択のカレンダーのview */
class SelectDateConditionView: UIView,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    private let xibName:String = "SelectDateConditionView"
    
    @IBOutlet weak var dateCollectionView:UICollectionView!
    
    // 今日の日付を取得しておく.
    private let toDay:NSDate = NSDate()
    
    // 表示する一番古いデータの日付.
    internal var oldDay:NSDate = NSDate()
    
    // デフォルトで表示する月の数.(最新の月が今月)
    private var sectionNum:Int = 0
    private var sectionList:[MonthData] = []
    
    
    // 選択されたIndexを保持.
    private var selectedIndexList:[NSIndexPath] = []
    internal var selectedDateList:[NSDate] = []
 
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dateCollectionView.delegate = self
        dateCollectionView.dataSource = self

        dateCollectionView.registerNib(UINib.init(nibName: "SelectDateConditionCellView" , bundle: nil), forCellWithReuseIdentifier: "cell")
        dateCollectionView.registerClass(SelectDateConditionSectionView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "Section")
        
        dateCollectionView.allowsSelection = true
        dateCollectionView.allowsMultipleSelection = true
    }
    
    func setOldDate(date:NSDate){
        
        oldDay = date
        setUpSectionDataList()

    }
    
    func setUpSectionDataList(){
        
        sectionNum += toDay.getMonth()-oldDay.getMonth()
        sectionNum += (toDay.getYear()-oldDay.getYear())*12
        
        for var i:Int=sectionNum; i>=0; i-- {
            
            var data = MonthData()
            
            data.month = toDay.getMonth()-i
            data.year = toDay.getYear()
            
            if data.month < 1 {
                data.month = 12+data.month
                data.year--
            }else if data.month > 12 {
                data.month = 1-data.month
                data.year++
            }
            
            data.startIndex = NSDate.getMonthStartIndex(data.year, month: data.month)
            
            data.lastDay = NSDate.getLastDayOfMonth(data.year, month: data.month)
            
            sectionList.append(data)
        }
        
        self.dateCollectionView.reloadData()
    }

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return sectionList.count
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sectionList[section].startIndex+sectionList[section].lastDay
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell:SelectDateConditionCellView = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! SelectDateConditionCellView
        
        if indexPath.row < sectionList[indexPath.section].startIndex || indexPath.row-sectionList[indexPath.section].startIndex >= sectionList[indexPath.section].lastDay{
            cell.date = NSDate()
            cell.dateLabel.text = ""
            cell.backgroundColor = UIColor.clearColor()
            cell.dateLabel.textColor = UIColor.blackColor()
            
        }else {
            let cellDate = getCellDay(sectionList[indexPath.section], row: indexPath.row)
            
            // もしもcellの日付データがselectedDateListと一致したら、そのindexPathを登録しておく.
            if selectedDateList.indexOf(cellDate) != nil {
                selectedIndexList.append(indexPath)
//                selectedDateList.removeAtIndex(selectedDateList.indexOf(cellDate)!)
            }
            
            if selectedIndexList.indexOf(indexPath) != nil {
                // すでに選択済みの項目だったら
                cell.backgroundColor = UIColor.strongOrangeColor()
                cell.dateLabel.textColor = UIColor.whiteColor()
                dateCollectionView.selectItemAtIndexPath(indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.None)
            }else {
                cell.backgroundColor = UIColor.whiteColor()
                cell.dateLabel.textColor = UIColor.blackColor()
            }
            
            cell.date = cellDate
            cell.dateLabel.text = "\(cellDate.getDay())"
            
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        let reusableView:SelectDateConditionSectionView = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "Section", forIndexPath: indexPath) as! SelectDateConditionSectionView
        
        reusableView.titleLabel.text = "\(sectionList[indexPath.section].year)年\(sectionList[indexPath.section].month)月"
        return reusableView
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell:SelectDateConditionCellView = collectionView.cellForItemAtIndexPath(indexPath) as! SelectDateConditionCellView
        // もしcellの中身がなかったら選択状態にしない 
        if cell.dateLabel.text == "" {
            cell.selected = false
        }else {
            cell.backgroundColor = UIColor.strongOrangeColor()
            cell.dateLabel.textColor = UIColor.whiteColor()
            
            if selectedIndexList.indexOf(indexPath) == nil {
                selectedIndexList.append(indexPath)
            }
        }
    }
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        let cell:SelectDateConditionCellView = collectionView.cellForItemAtIndexPath(indexPath) as! SelectDateConditionCellView
        
        if cell.dateLabel.text == "" {
            cell.selected = false
        }else {
            print("deselect")
            cell.backgroundColor = UIColor.whiteColor()
            cell.dateLabel.textColor = UIColor.blackColor()
            selectedIndexList.removeAtIndex(selectedIndexList.indexOf(indexPath)!)
        }
    }
    /*
    * UICollectionViewDelegateFlowLayoutのメソッド.
    */
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let width = ((dateCollectionView.frame.width+16) as CGFloat)/(8.0 as CGFloat)
        return CGSizeMake(width,width)
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(1, 1, 1, 1);
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSizeMake(self.frame.width, 50)
    }

    // sectionとrowからcellに当てはまるDateを計算.
    func getCellDay(data:MonthData,row:Int)->NSDate{

        let year = data.year
        let month = data.month        
        var day = 1

        if row > data.startIndex {
            day = row-data.startIndex+1
        }
        
        let format:NSDateFormatter = NSDateFormatter().getAllElementFormate()
        return format.dateFromString("\(year)/\(month)/\(day) 00:00:00")!
    }
    
    func getSelectedDateList()->[NSDate] {
        var data:[NSDate] = []
        for i in selectedIndexList {
            data.append(getCellDay(sectionList[i.section], row: i.row))
        }
        return data
    }
}
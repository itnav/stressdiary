//
//  QuestionnaireView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/11.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

protocol QuestionnaireViewDelegate:class {
    func selectAnswer(select:Int)
}

class QuestionnaireView: UIView {
    
    private let xibName:String = "QuestionnaireView"
    var delegate:QuestionnaireViewDelegate?
    
    
    @IBOutlet weak var ans_button_1:UIButton!
    @IBOutlet weak var ans_button_2:UIButton!
    @IBOutlet weak var ans_button_3:UIButton!
    @IBOutlet weak var ans_button_4:UIButton!
    
    private var buttons:[UIButton] = []
    private var last_selected:Int = 0
    
    @IBOutlet weak var questionLabel:UILabel!
    
    @IBAction func selectAnswer(sender:UIButton){
        
        for button in buttons {
            if button.tag != sender.tag {
                button.backgroundColor = UIColor.whiteColor()
            }
        }
        sender.backgroundColor = UIColor.lightOrangeColor()
        
        delegate?.selectAnswer(sender.tag)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupXib()
        buttons = [ans_button_1,ans_button_2,ans_button_3,ans_button_4]
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    internal func setQuestionnaireData(data:QuestionnaireData){
        ans_button_1.setTitle(data.answers[0] as? String, forState: .Normal)
        ans_button_2.setTitle(data.answers[1] as? String, forState: .Normal)
        ans_button_3.setTitle(data.answers[2] as? String, forState: .Normal)
        ans_button_4.setTitle(data.answers[3] as? String, forState: .Normal)
        
        questionLabel.text = data.question
    }
    
    /*
    * xibのレイアウトファイルの読み込み.
    */
    private func setupXib(){
        
        if let view:UIView = NSBundle.mainBundle().loadNibNamed(xibName, owner:self, options: nil).first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
        }
    }
}

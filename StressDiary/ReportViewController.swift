//
//  ReportViewController.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/10/28.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit
import SVProgressHUD

class ReportViewController: UIViewController {

    
    var data_load_flag:Bool = false
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.tabBarItem.image = UIImage(named: "icon_menu_report")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        
        setupNavigationBar()
    }
    private func setupNavigationBar() {
        
        // NavigationBarの下にViewが埋まってしまうことを回避
        edgesForExtendedLayout = UIRectEdge.None;
        
        title = "レポート"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupNavigationBar()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let view = self.view as? HeartRateGraphView {
            let btn:UIButton = UIButton()
            btn.tag = 1
            view.selectDataPeriod(btn)
            view.setUpXAxis()
            view.setupLineChartView()
        }
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ReportViewController.setUpDataDidCompleteDownload(_:)), name: SetUpDataManager.notificationNameCompleteDownload, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        SVProgressHUD.show()
        HeartBeatLogBatch.getHeartbeatDateFromLastBatchDate {
            SetUpDataManager.sharedInstance.setUpData()
        }

        if data_load_flag == false {
            data_load_flag = true
        }
    }
    
    // NSNotification
    func setUpDataDidCompleteDownload(sender: NSNotification) {
        if let view = self.view as? HeartRateGraphView {
            view.setDataEntries(.Day, data: SetUpDataManager.sharedInstance.graph_data_day)
            view.setDataEntries(.Week, data: SetUpDataManager.sharedInstance.graph_data_week)
            view.setDataEntries(.Month, data: SetUpDataManager.sharedInstance.graph_data_month)
            view.reloadGraph(.Day)
            view.showGraphType = .Day
            view.baseHeartRateInfoView.reloadData()
        }
        SVProgressHUD.dismiss()
    }
}
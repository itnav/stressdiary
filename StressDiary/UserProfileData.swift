//
//  UserProfileModel.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/18.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation

/* ユーザープロフィールのデータモデル */
class UserProfileData {
    
    internal var birthday:NSDate!
    internal var gender:String!
    internal var weightKg:Double!
    internal var heightCm:Double!
    internal var prefectureCode:String!
    internal var defaultTags:[String]!
    
    /*
    * データの初期化処理.
    */
    init(){
        self.birthday = NSDate()
        self.gender = GenderType.unknown.getGenderKey()
        self.weightKg = 0
        self.heightCm = 0
        self.prefectureCode = getPrefectureCode(prefectureCodeList[0])
        self.defaultTags = []
    }
    
    init(data:UserProfileData) {
        self.birthday = data.birthday
        self.gender = data.gender
        self.weightKg = data.weightKg
        self.heightCm = data.heightCm
        self.prefectureCode = data.prefectureCode
        self.defaultTags = data.defaultTags
    }
    
    internal func setUserProfile(data:NSDictionary){
        
        self.birthday = convertAPIDateToNSDate(data.objectForKey("birthday") as! NSTimeInterval)
        self.defaultTags = data.objectForKey("defaultTags") as! [String]
        self.gender = data.objectForKey("gender") as! String
        self.heightCm = data.objectForKey("heightCm") as! Double
        self.weightKg = data.objectForKey("weightKg") as! Double
        self.prefectureCode = data.objectForKey("prefectureCode") as! String
        
    }
}
//
//  Setup.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/12/05.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

func setupTabBarController(isFirstLoad: Bool) -> UITabBarController {
    let newsVC = NewsViewController()
    let newsNC = UINavigationController(rootViewController: newsVC)
    
    let measureVC = MeasureViewController(isBaseHeartRate: isFirstLoad)
    measureVC.title = "測定"
    let measureNC = UINavigationController(rootViewController: measureVC)
    
    let reportVC = ReportViewController()
    let reportNC = UINavigationController(rootViewController: reportVC)
    
    let historyVC = HistoryViewController()
    let historyNC = UINavigationController(rootViewController: historyVC)
    
    let settingVC = SettingViewController(firstSet: isFirstLoad)
    let settingNC = UINavigationController(rootViewController: settingVC)
    
    let tabs = [newsNC, reportNC, measureNC, historyNC, settingNC]
    
    let tabBarController = UITabBarController()
    tabBarController.setViewControllers(tabs, animated: true)
    UITabBar.appearance().barTintColor = UIColor(white: 0.95, alpha: 1.0)
    UITabBar.appearance().tintColor = UIColor.strongOrangeColor()
    
    tabBarController.tabBar.items?[2].title = "測定"
    
    UINavigationBar.appearance().barTintColor = UIColor.navBarOrangeColor()
    UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
    
    return tabBarController
}
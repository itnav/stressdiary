//
//  EditMemoView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/12.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

class EditMemoView: UIView,UITextViewDelegate{
    
    @IBOutlet weak var editMemoTextField:UITextView!
    @IBOutlet weak var bgView:UIView!
    @IBOutlet weak var remainingText:UILabel!
    @IBOutlet weak var closeKeyBoardButton:UIButton!
    
    var initMemo:String = ""
    
    var numRemainingMemo:Int = 200
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func awakeFromNib() {
        
        closeKeyBoardButton.layer.cornerRadius = 10
        bgView.layer.cornerRadius = 15.0
        editMemoTextField.delegate = self
        remainingText.text = "\(numRemainingMemo-(editMemoTextField.text as NSString).length)"
        
    }
    
    @IBAction func closeButtonAction(sender : AnyObject) {
        editMemoTextField.endEditing(true)
    }
    
    func setInitMemoTest(memo:String) {
        print("initmemo : \(memo)")
        editMemoTextField.text = memo
        remainingText.text = "\(numRemainingMemo-(editMemoTextField.text as NSString).length)"
    }
    func textViewDidChange(textView: UITextView){
        remainingText.text = "\(numRemainingMemo-(textView.text as NSString).length)"
    }
    func textViewDidBeginEditing(textView: UITextView) {
        closeKeyBoardButton.hidden = false;
    }
    func textViewDidEndEditing(textView: UITextView) {
        closeKeyBoardButton.hidden = true;
    }
    internal func getInputMemo()->String{
        return editMemoTextField.text
    }
}
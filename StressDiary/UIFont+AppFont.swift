//
//  UIFont+AppFont.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/04.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit

extension UIFont {
    
    /*
    メーターなどの数字で使われているフォント.
    */
    class func meterDisplayFont(size:CGFloat) -> UIFont {
        return UIFont(name: "FolkPro-Heavy", size: size)!
    }
    
    /*
    ボタンやグラフの切り替え表示で使われるフォント.
    */
    class func baseButtonFont(size:CGFloat) -> UIFont {
        return UIFont(name: "HiraKakuProN-W6", size: size)!
    }
    
}
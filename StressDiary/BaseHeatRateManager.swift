//
//  BaseHeatRateManager.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/22.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import HealthKit

/* BaseHeartRateInfoViewの基本心拍数の管理。
高低平均の管理をしている。
*/
struct BaseHeartRateManager {
    
    enum HeartRateType {
        case High
        case Ave
        case Low
        
        func getKey()->String{
            switch self {
            case .High:
                return "baseHeartRateDataKey-High"
            case .Ave:
                return "baseHeartRateDataKey-Ave"
            case .Low:
                return "baseHeartRateDataKey-Low"
            }
        }
    }
    // 統計のデータを設定.
    static func setStatisticsHeartRate(type:HeartRateType,value:Int) {
        NSUserDefaults.standardUserDefaults().setInteger(value, forKey: type.getKey())
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    // 統計のデータを取得.
    static func getStatisticsHeartRate(type:HeartRateType)->Int{
        
        if let value:Int = NSUserDefaults.standardUserDefaults().integerForKey(type.getKey()) {
            return value
        }else {
            return 0
        }
        
    }
    
    // 最新の心拍数データを設定.
    static func setLatestHeartRate(value:HeartbeatLogData){
        NSUserDefaults.standardUserDefaults().setObject(value, forKey: "latestHeartRateDataKey")
    }
    // 最新の心拍数データを取得.
    static func getLatestHeartRate()->HeartbeatLogData!{
        if let value:HeartbeatLogData = NSUserDefaults.standardUserDefaults().objectForKey("latestHeartRateDataKey") as? HeartbeatLogData{
            return value
        }else {
            return nil
        }
    }
    
    //基本心拍数が測定済みかどうか確認する.
    // ここのロジックは基本心拍数の扱いによって違うと思うので、適宜書き換えてください.
    static func isCompleteMeasureBaseHeartRate()->Bool {
        if let value:Int = getBaseHeartRate() {
            if value != 0 {
                return true
            }else {
                return false
            }
        }else {
            return false
        }
    }
    // MARK: -BaseHeartRate
    static func setBaseHeartRate(heartRate: Int) {
//        let ud = NSUserDefaults.standardUserDefaults()
//        ud.setObject(heartRate, forKey: "BaseHeartRate")
    }
    
    static func getBaseHeartRate() -> Int? {
        return UserDefaultsManager.getBaseHeartRate()
    }
    
    /// 基本心拍を更新(必要な場合)
    static func updateBaseHeartRate(completion: () -> Void) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = .NoStyle
        
        if UserDefaultsManager.getLastBaseHeartRateUpdatedDate() != dateFormatter.stringFromDate(NSDate()) {
            // 基本心拍の設定が当日以外に行われているので、計算する
            
            let healthStore = HKHealthStore()
            
            let sampleType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!
            let timeSortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
            
            // 1. get last 10 heart rates
            let heartRateQuery = HKSampleQuery(sampleType: sampleType, predicate: nil, limit: 10, sortDescriptors: [timeSortDescriptor], resultsHandler: { query, results, error in
                if let _ = error {
                    print("心拍データの取得に失敗しました")
                    print(error)
                    dispatch_async(dispatch_get_main_queue(), {
                        completion()
                    })
                    return
                }
                
                if results?.count > 9 {
                    var heartRates = results as? [HKQuantitySample]
                    // 2. remove max rate and min rate
                    var minHeartRate: HKQuantitySample!
                    var maxHeartRate: HKQuantitySample!
                    for result in heartRates! {
                        if minHeartRate == nil || minHeartRate?.heartRate() > result.heartRate() {
                            minHeartRate = result
                        }
                        
                        if maxHeartRate == nil || maxHeartRate?.heartRate() < result.heartRate() {
                            maxHeartRate = result
                        }
                    }
                    
                    if let _ = minHeartRate {
                        heartRates?.remove(minHeartRate!)
                    }
                    
                    if let _ = maxHeartRate {
                        heartRates?.remove(maxHeartRate!)
                    }
                    
                    // 3. calculate average (base heart rate)
                    var heartRateSum = 0
                    for heartRate in heartRates! {
                        heartRateSum += heartRate.heartRate()
                    }
                    let average = heartRateSum / heartRates!.count
                    
                    UserDefaultsManager.setBaseHeartRate(average)
                } else {
                    print("NODATA")
                    UserDefaultsManager.setBaseHeartRate(60)
                }
                UserDefaultsManager.setLastBaseHeartRateUpdatedDate(dateFormatter.stringFromDate(NSDate()))
                dispatch_async(dispatch_get_main_queue(), {
                    completion()
                })
            })
            
            healthStore.executeQuery(heartRateQuery)
        } else {
            completion()
        }
    }
    
}
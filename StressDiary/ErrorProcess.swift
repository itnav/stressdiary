//
//  ErrorProcess.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/11/17.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

/* エラー時のアラート処理 */
struct ErrorProcess {
    private static var isShowingCommunicationError = false
    
    // 最前面のViewControllerを取得する
    static func getTopVC() -> UIViewController {
        var topVC = UIApplication.sharedApplication().keyWindow?.rootViewController
        while (topVC?.presentedViewController != nil) {
            topVC = topVC?.presentedViewController
        }
        
        return topVC!
    }
    
    static func serverError() {
        let topVC = getTopVC()
        dispatch_async(dispatch_get_main_queue(), {
            
            if SVProgressHUD.isVisible() {
                SVProgressHUD.dismiss()
            }
            
            let alertController = UIAlertController(title: "サーバエラー", message: "データ通信エラーが発生しました", preferredStyle: .Alert)
            
            let okAction = UIAlertAction(title: "OK", style: .Default) {
                action in
            }
            
            alertController.addAction(okAction)
            
            topVC.presentViewController(alertController, animated: true, completion: nil)
        })
    }
    
    
    // MARK: -NoAccountError()
    static func noFacebookAccountError() {
        let topVC = getTopVC()
        
        dispatch_async(dispatch_get_main_queue(), {
            let alertController = UIAlertController(title: "アカウントエラー", message: "このiPhoneにFacebookアカウントが登録されていません。「設定」->「Facebook」からFacebookアカウントを登録してください。", preferredStyle: .Alert)
            
            let okAction = UIAlertAction(title: "OK", style: .Default) {
                action in
            }
            let configAction = UIAlertAction(title: "設定", style: .Cancel) {
                action in
                if let url = NSURL(string:"prefs:root=FACEBOOK") {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            
            alertController.addAction(okAction)
            alertController.addAction(configAction)
            
            topVC.presentViewController(alertController, animated: true, completion: nil)
        })
    }
    
    static func noTwitterAccountError() {
        let topVC = getTopVC()
        
        dispatch_async(dispatch_get_main_queue(), {
            let alertController = UIAlertController(title: "アカウントエラー", message: "このiPhoneにTwitterアカウントが登録されていません。「設定」->「Twitter」からTwitterアカウントを登録してください。", preferredStyle: .Alert)
            
            let okAction = UIAlertAction(title: "OK", style: .Default) {
                action in
            }
            let configAction = UIAlertAction(title: "設定", style: .Cancel) {
                action in
                if let url = NSURL(string:"prefs:root=TWITTER") {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            
            alertController.addAction(okAction)
            alertController.addAction(configAction)
            
            topVC.presentViewController(alertController, animated: true, completion: nil)
        })
    }
    
    // MARK: -CommunicationError
    static func communicationError() {
        
        if SVProgressHUD.isVisible() {
            SVProgressHUD.dismiss()
        }
        
        let topVC = getTopVC()
        
        dispatch_async(dispatch_get_main_queue(), {
            if !isShowingCommunicationError {
                isShowingCommunicationError = true
                let alertController = UIAlertController(title: "通信エラー", message: "通信が正しく行われませんでした。回線状況の良い場所で再度実行してください。", preferredStyle: .Alert)
                
                let okAction = UIAlertAction(title: "OK", style: .Default) {
                    action in
                    isShowingCommunicationError = false
                }
                
                alertController.addAction(okAction)
                
                topVC.presentViewController(alertController, animated: true, completion: nil)
            }
        })
    }
    
    // MARK: -AuthenticationError
    static func fbAuthenticationError() {
        let topVC = getTopVC()
        
        dispatch_async(dispatch_get_main_queue(), {
            let alertController = UIAlertController(title: "認証エラー", message: "Facebookへの認証が失敗しました。「設定」->「Facebook」からStressDiaryのアクセスを許可してください。", preferredStyle: .Alert)
            
            let okAction = UIAlertAction(title: "OK", style: .Default) {
                action in
            }
            let configAction = UIAlertAction(title: "設定", style: .Cancel) {
                action in
                if let url = NSURL(string:"prefs:root=Privacy&path=FACEBOOK") {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            
            alertController.addAction(okAction)
            alertController.addAction(configAction)
            
            topVC.presentViewController(alertController, animated: true, completion: nil)
        })
    }
    
    static func twitterAuthenticationError() {
        let topVC = getTopVC()
        
        dispatch_async(dispatch_get_main_queue(), {
            let alertController = UIAlertController(title: "認証エラー", message: "Twitterへの認証が失敗しました。「設定」->「Twitter」からStressDiaryのアクセスを許可してください。", preferredStyle: .Alert)
            
            let okAction = UIAlertAction(title: "OK", style: .Default) {
                action in
            }
            let configAction = UIAlertAction(title: "設定", style: .Cancel) {
                action in
                if let url = NSURL(string:"prefs:root=Privacy&path=TWITTER") {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            
            alertController.addAction(okAction)
            alertController.addAction(configAction)
            
            topVC.presentViewController(alertController, animated: true, completion: nil)
        })
    }
    
    // MARK: -SocialAuthenticationError
    static func fbSocialAuthenticationError() {
        let topVC = getTopVC()
        
        dispatch_async(dispatch_get_main_queue(), {
            let alertController = UIAlertController(title: "ソーシャル認証エラー", message: "Facebook本体への認証が失敗しました。", preferredStyle: .Alert)
            
            let okAction = UIAlertAction(title: "OK", style: .Default) {
                action in
            }
            
            alertController.addAction(okAction)
            
            topVC.presentViewController(alertController, animated: true, completion: nil)
        })
    }
    
    // MARK: -NoAppError
    static func noLineAppError() {
        let topVC = getTopVC()
        
        dispatch_async(dispatch_get_main_queue(), {
            let alertController = UIAlertController(title: "アプリケーションエラー", message: "このiPhoneには「Line」がインストールされていません。", preferredStyle: .Alert)
            
            let okAction = UIAlertAction(title: "OK", style: .Default) {
                action in
            }
            
            alertController.addAction(okAction)
            
            topVC.presentViewController(alertController, animated: true, completion: nil)
        })
    }
    
    // MARK: -DetachError()
    static func detachError() {
        let topVC = getTopVC()
        
        dispatch_async(dispatch_get_main_queue(), {
            let alertController = UIAlertController(title: "アカウントエラー", message: "このiPhoneに紐付けられた唯一のIDのため、連携を切ることはできません。", preferredStyle: .Alert)
            
            let okAction = UIAlertAction(title: "OK", style: .Default) {
                action in
            }
            
            alertController.addAction(okAction)
            
            topVC.presentViewController(alertController, animated: true, completion: nil)
        })
    }
    
    // MARK: -AccountDuplicationError
    static func accountDuplicationError() {
        let topVC = getTopVC()
        
        dispatch_async(dispatch_get_main_queue(), {
            let alertController = UIAlertController(title: "アカウントエラー", message: "このアカウントはすでに登録されています。", preferredStyle: .Alert)
            
            let okAction = UIAlertAction(title: "OK", style: .Default) {
                action in
            }
            
            alertController.addAction(okAction)
            
            topVC.presentViewController(alertController, animated: true, completion: nil)
        })
    }
    
    // MARK: -SaveHeartbeatLogError
    static func saveHeartbeatLogError() {
        
        if SVProgressHUD.isVisible() {
            SVProgressHUD.dismiss()
        }
        
        let topVC = getTopVC()
        dispatch_async(dispatch_get_main_queue(), {
            let alertController = UIAlertController(title: "保存エラー", message: "データの保存に失敗しました。回線状況が良いことを確認してもう一度実行してください。", preferredStyle: .Alert)
            
            let okAction = UIAlertAction(title: "OK", style: .Default) {
                action in
            }
            
            alertController.addAction(okAction)
            
            topVC.presentViewController(alertController, animated: true, completion: nil)
        })
    }
    
    // MARK: -HealthCareAccess
    static func healthCareNotAuthorizedError() {
        let topVC = getTopVC()
        
        dispatch_async(dispatch_get_main_queue(), {
            let alertController = UIAlertController(title: "認証エラー", message: "ヘルスケア内に心拍数データが存在しないか、ヘルスケアへのアクセスが許可されていません。許可されていない場合は「ヘルスケア」->「ソース」->「StressDiary」から「心拍数」をOnにしてください。読み出し許可の反映には時間がかかる場合があります。", preferredStyle: .Alert)
            
            let okAction = UIAlertAction(title: "OK", style: .Default) {
                action in
            }
            
            alertController.addAction(okAction)
            
            topVC.presentViewController(alertController, animated: true, completion: nil)
        })
    }
    
    static func commonAlert(title: String, message: String) {
        //        let topVC = getTopVC()
        
        dispatch_async(dispatch_get_main_queue(), {
            SVProgressHUD.showSuccessWithStatus(message)
        })
    }
}
//
//  TutorialViewController.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/12/03.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController, UIScrollViewDelegate{
    
    var pageControl: UIPageControl!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.whiteColor()
        
        setupNavigationBar()
        setupViews()
    }
    
    private func setupNavigationBar() {
        
        // NavigationBarの下にViewが埋まってしまうことを回避
        edgesForExtendedLayout = UIRectEdge.None;
        
        title = "チュートリアル"
        
        let nextButton = UIBarButtonItem(title: "スキップ", style: .Plain, target: self, action: "toNext")
        nextButton.tintColor = UIColor.blackColor()
        navigationItem.rightBarButtonItem = nextButton
    }
    
    private func setupViews() {
        let viewSize = view.frame.size
        let pageSize = 4
        
        pageControl = UIPageControl(frame: CGRectMake(0, self.view.frame.maxY - 100, viewSize.width, 50))
        pageControl.numberOfPages = pageSize
        pageControl.pageIndicatorTintColor = UIColor.lightGrayColor()
        pageControl.currentPageIndicatorTintColor = UIColor.blackColor()
        pageControl.currentPage = 0
        pageControl.userInteractionEnabled = false
        view.addSubview(pageControl)
        
        let scrollView = UIScrollView()
        scrollView.frame = CGRectMake(0, 0, viewSize.width, viewSize.height)
        scrollView.contentSize = CGSizeMake(viewSize.width * 4, scrollView.frame.height)
        scrollView.delegate = self
        scrollView.pagingEnabled = true
        scrollView.backgroundColor = UIColor(red: 255.0/255.0, green: 243.0/255.0, blue: 196.0/255.0, alpha: 1.0)
        view.addSubview(scrollView)
        
        for var i = 0; i < pageSize; i++ {
            // ページごとに異なるラベルを生成する.
            
            //イメージ設定
            let imageView = UIImageView(frame: CGRectMake(CGFloat(i) * viewSize.width + 10, 10, viewSize.width - 20, scrollView.frame.size.height - 300))
            imageView.contentMode = UIViewContentMode.ScaleAspectFit
            imageView.backgroundColor = UIColor.clearColor()
            
            //タイトルラベル設定
            let titleLabelRect = CGRectMake(CGFloat(i) * viewSize.width + 10, imageView.frame.size.height + 10, viewSize.width - 20, 60)
            let titleLabel = UILabel(frame: titleLabelRect)
            titleLabel.textColor = UIColor(red: 255.0/255.0, green: 189.0/255.0, blue: 95.0/255.0, alpha: 1.0)
            titleLabel.textAlignment = NSTextAlignment.Center
            titleLabel.font = UIFont.boldSystemFontOfSize(50)
            
            //メッセージラベル設定
            let messageLabelRect = CGRectMake(CGFloat(i) * viewSize.width + 10, imageView.frame.size.height + titleLabel.frame.size.height + 30, viewSize.width - 20, scrollView.frame.size.height - imageView.frame.size.height - 60)
            let messageLabel = UILabel(frame: messageLabelRect)
            messageLabel.numberOfLines = 0
            messageLabel.textColor = UIColor(red: 66.0/255.0, green: 66/255.0, blue: 66/255.0, alpha: 1.0)
            messageLabel.font = UIFont.systemFontOfSize(20)
            if i == 0 {
                imageView.image = UIImage(named: "tutorial_img_check")
                titleLabel.text = "測定"
                messageLabel.text = "AppleWatchから心拍数を自動取得しストレス状態を算出します。"
            } else if i == 1 {
                imageView.image = UIImage(named: "tutorial_img_info")
                titleLabel.text = "お知らせ"
                messageLabel.text = "ストレス状況をお知らせします。"
            } else if i == 2 {
                imageView.image = UIImage(named: "tutorial_img_confirm")
                titleLabel.text = "確認"
                messageLabel.text = "ストレス状況を蓄積しレポートを作成。\nタグやメモを付けることによって、過去のストレス状況を振り返ることも可能です。"
            } else if i == 3 {
                imageView.image = UIImage(named: "tutorial_img_know")
                titleLabel.text = "知る"
                messageLabel.text = "ストレスの状況に合わせたアドバイスを表示したり、ストレス解消に役立つ情報を紹介します。"
            }
            messageLabel.sizeToFit()
            scrollView.addSubview(imageView)
            scrollView.addSubview(titleLabel)
            scrollView.addSubview(messageLabel)
        }
        self.view.bringSubviewToFront(pageControl)
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
        // スクロール数が1ページ分になったら時.
        if fmod(scrollView.contentOffset.x, scrollView.frame.maxX) == 0 {
            // ページの場所を切り替える.
            pageControl.currentPage = Int(scrollView.contentOffset.x / scrollView.frame.maxX)
            let nextButton = navigationItem.rightBarButtonItem
            if(pageControl.currentPage == 3) {
                nextButton?.title = "次へ"
            } else {
                nextButton?.title = "スキップ"
            }
        }
    }
    
    func toNext() {
        let tabBarController = setupTabBarController()
        UIApplication.sharedApplication().keyWindow?.rootViewController = tabBarController
    }
    
    func setupTabBarController() -> UITabBarController {
        let newsVC = NewsViewController()
        let newsNC = UINavigationController(rootViewController: newsVC)
        
        let measureVC = MeasureViewController(isBaseHeartRate: false)
        let measureNC = UINavigationController(rootViewController: measureVC)
        
        let reportVC = ReportViewController()
        let reportNC = UINavigationController(rootViewController: reportVC)
        
        let historyVC = HistoryViewController()
        let historyNC = UINavigationController(rootViewController: historyVC)
        
        let settingVC = SettingViewController(firstSet: false)
        let settingNC = UINavigationController(rootViewController: settingVC)
        
        let tabs = [newsNC, reportNC, measureNC, historyNC, settingNC]
        
        let tabBarController = UITabBarController()
        tabBarController.setViewControllers(tabs, animated: true)
        UITabBar.appearance().barTintColor = UIColor(white: 0.95, alpha: 1.0)
        UITabBar.appearance().tintColor = UIColor.strongOrangeColor()
        
        tabBarController.tabBar.items?[2].title = "測定"
        
        UINavigationBar.appearance().barTintColor = UIColor.navBarOrangeColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        
        return tabBarController
    }
    
}

//
//  EditMemoViewController.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/11.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit

class EditMemoViewController: UIViewController {
 
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.view.backgroundColor = UIColor.whiteColor()
    }
    
    private func setupNavigationBar() {
        // NavigationBarの下にViewが埋まってしまうことを回避
        edgesForExtendedLayout = UIRectEdge.None;
        title = "メモ編集"
        
        // NavigationBarに完了ボタンを追加.
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "完了", style: UIBarButtonItemStyle.Plain, target:self, action: "onTouchCompliteButton")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        
    }
    func setMemoText(memo:String) {
        (self.view as! EditMemoView).setInitMemoTest(memo)
    }
    func onTouchCompliteButton(){
        
        let index:Int = (self.navigationController?.viewControllers.count)!-2
        
        if let measureResultVC:MeasureResultViewController = self.navigationController?.viewControllers[index] as? MeasureResultViewController {
            measureResultVC.memo = (self.view as! EditMemoView).getInputMemo()
            self.navigationController?.popToViewController(measureResultVC, animated: true)
        }
    }
    
}

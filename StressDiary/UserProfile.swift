//
//  UserProfile.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/18.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import SVProgressHUD

/* ユーザープロフィールの実処理 */
struct UserProfile {
    
    /*
    * UserPrifileModelの取得.
    */
    static func getUserProfile()->Promise<UserProfileData>{
        
        let profile:UserProfileData = UserProfileData()
        
        return Promise { fulfill, reject in
            
            let request = Alamofire.request(UserProfileRouter.Find())
            request.response { request, response, data, error in
                print("UserInfo.find")
                print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
                
                if let _ = error {
                    ErrorProcess.communicationError()
                    reject(CommunicationError.NoCommunicationLine)
                    return
                }
                
                do {
                    let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                    
                    if let result = json.objectForKey("result") as? NSDictionary {
                        profile.setUserProfile(result)

                        fulfill(profile)
                    }
                }
            }
        }
    }
    
    /*
    * UserPrifileModelの更新.
    */
    static func updateUserProfile(profile:UserProfileData) -> Promise<Bool>{
        
        return Promise { fulfill, reject in
            
            let request = Alamofire.request(UserProfileRouter.Put(data:profile))
            request.response { request, response, data, error in
                print("UserInfo.put")
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                
                if let _ = error {
                    fulfill(false)
                }else {
                    fulfill(true)
                    UserProfileManager.saveUserProfile(profile)
                }
            }
        }
    }
}
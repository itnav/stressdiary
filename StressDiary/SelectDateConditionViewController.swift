//
//  SelectDateConditionViewController.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/08.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

/* 日付選択のカレンダーのVC */
class SelectDateConditionViewController: UIViewController {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init() {
        super.init(nibName:nil, bundle: nil)
        setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        
        // NavigationBarの下にViewが埋まってしまうことを回避
        edgesForExtendedLayout = UIRectEdge.None;
        
        self.title = "日付を選択する"
        
        // NavigationBarに完了ボタンを追加.
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "完了", style: UIBarButtonItemStyle.Plain, target:self, action: "onTouchCompliteButton")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setOldDate(NSDate.getDate(2015, month: 12, day: 1))
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        (self.view as! SelectDateConditionView).dateCollectionView.setContentOffset(CGPointMake(0, (self.view as! SelectDateConditionView).dateCollectionView.contentSize.height-self.view.frame.height+(self.tabBarController?.tabBar.frame.height)!), animated: true)
    }
    
    func setOldDate(date:NSDate){
        (self.view as! SelectDateConditionView).setOldDate(date)
    }
    func onTouchCompliteButton(){
        let list = (self.view as! SelectDateConditionView).getSelectedDateList()
        let historyViewController:HistoryViewController = self.navigationController?.viewControllers[0] as! HistoryViewController
        historyViewController.setDateCondition(list)
        self.navigationController?.popToViewController(historyViewController, animated: true)
    }
    internal func setSelectedDate(data:[NSDate]){
        (self.view as! SelectDateConditionView).selectedDateList = data
    }
}
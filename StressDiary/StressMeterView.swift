//
//  StressMeterView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/07.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

class StressMeterView:UIView {
    
    let xibName:String = "StressMeterView"
    
    @IBOutlet weak var meterBarImage:UIImageView!
    @IBOutlet weak var meterImage: UIImageView!
    @IBOutlet weak var meterBarView: UIView!
    
    // meterの表示区分管理用enum.
    enum MeterSection {
        case Minimum
        case Low
        case Moderate
        case High
        case Maximum
        
        func getMeterRotate() -> CGFloat {
            switch self {
            case .Minimum:
                return -90.0
            case .Low:
                return -67.5
            case .Moderate:
                return -45.0
            case .High:
                return -22.5
            case .Maximum:
                return 0.0
            }
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupXib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupXib()
    }
    
    /*
    * xibのレイアウトファイルの読み込み.
    */
    private func setupXib(){
        //        print("setupXib")
        
        if let view:UIView = NSBundle.mainBundle().loadNibNamed(xibName, owner:self, options: nil).first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
        }
        //self.setMeterValue(0, animation: false, duration: 0)
        //        self.meterBarImage.center.y -= self.meterBarImage.center.y/2
    }
    
    private func getMeterAngle(stressIndex:Double)->CGFloat{
        
        // 一度元に戻す.
        //        meterBarView.transform = CGAffineTransformIdentity
        
        // メーターの量によってangleを生成.
        var angle:CGFloat = 0.0
        
        if stressIndex < 0.0 {
            // 0以下だったら左.
            angle = 0.0
        }else if stressIndex > 100.0 {
            // 0以下だったら右.
            angle = (1.8*CGFloat(100.0))*CGFloat(M_PI/180.0)
            angle -= 90 * CGFloat(M_PI/180.0)
        }else {
            angle = (1.8*CGFloat(stressIndex))*CGFloat(M_PI/180.0)
            angle -= 90 * CGFloat(M_PI/180.0)
        }
        
        return angle
    }
    
    internal func setMeterValue(stressIndex:Double,animation:Bool,duration:NSTimeInterval) {
        // 回転の中心軸を変更.
        
        let verticalTwistAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        verticalTwistAnimation.toValue = getMeterAngle(stressIndex)
        verticalTwistAnimation.duration = animation ? duration : 0
        verticalTwistAnimation.fillMode = kCAFillModeForwards
        verticalTwistAnimation.removedOnCompletion = false
        
        meterBarView.layer.addAnimation(verticalTwistAnimation, forKey: "moonSaltoAnimation")
        
    }
}

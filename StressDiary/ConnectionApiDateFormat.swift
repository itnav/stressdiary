//
//  ConnectionApiDateFormat.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/19.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation

// TODO : あとでExtensionにする。

/*
* API側で受け入れてくれる形式にNSDateをフォーマット.
*/
internal func getSendAPIDateFormat(date:NSDate) -> NSString{
    
    let dateFormat:NSDateFormatter = NSDateFormatter()
    dateFormat.dateFormat = "yyyy/MM/dd HH:mm:ss"
    //    dateFormat.timeZone = NSTimeZone(abbreviation: "JST")
    
    return dateFormat.stringFromDate(NSDate(timeInterval: -9*60*60, sinceDate: date))
}

/*
* APIからのDateをフォーマット.
*/
internal func convertAPIDateToNSDate(dateInterval:NSTimeInterval) -> NSDate{
    
    let dateFormat:NSDateFormatter = NSDateFormatter()
    dateFormat.dateFormat = "yyyy/MM/dd HH:mm:ss"
    dateFormat.timeZone = NSTimeZone(abbreviation: "JST");
    
    return dateFormat.dateFromString(dateFormat.stringFromDate(NSDate(timeIntervalSince1970: NSTimeInterval(dateInterval/1000))))!
}

/*
*　テーブルで表示できる形式にフォーマット
*/

internal func getShowSettingDateFormat(date:NSDate) -> NSString{
    let dateFormat:NSDateFormatter = NSDateFormatter()
    //    dateFormat.timeZone = NSTimeZone(abbreviation: "JST")
    dateFormat.dateFormat = "yyyy/MM/dd"
    return dateFormat.stringFromDate(date)
}

/*
*　MeasureProgressからMeasureResultにWatchからの計測時間を渡すとき
*/
internal func getMeasureDate(measureDateString: String) -> NSDate{
    let dateFormat: NSDateFormatter = NSDateFormatter()
    //dateFormat.locale     = NSLocale(localeIdentifier: "ja_JP")
    dateFormat.timeZone = NSTimeZone.defaultTimeZone()
    dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return dateFormat.dateFromString(measureDateString)!
}

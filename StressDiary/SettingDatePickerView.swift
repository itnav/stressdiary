//
//  SettingDatePickerView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/19.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

protocol SettingDatePickerViewDekegate:class {
    func selectNewDate(date:NSDate)
}

class SettingDatePickerView: UIView{
    private let xibName:String = "SettingDatePickerView"
    
    @IBOutlet weak var doneButton:UIButton!
    @IBOutlet weak var pickerView:UIDatePicker!
    @IBOutlet weak var cancelButton:UIButton!
    
    
    var delegate:SettingDatePickerViewDekegate?
    
    var selectData:[AnyObject] = []
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame:CGRect){
        super.init(frame: frame)
        self.backgroundColor = UIColor.whiteColor()
        
        setupXib()
        pickerView.setDate(NSDate(), animated: true)
        pickerView.datePickerMode = UIDatePickerMode.Date
        
    }
    @IBAction func doneSelect(){
        
        delegate?.selectNewDate(pickerView.date)
        self.removeFromSuperview()
        
    }
    @IBAction func cencel(){
        self.removeFromSuperview()
    }
    /*
    * xibのレイアウトファイルの読み込み.
    */
    private func setupXib(){
        if let view:UIView = NSBundle.mainBundle().loadNibNamed(xibName, owner:self, options: nil).first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
        }
        
        self.pickerView.maximumDate = NSDate()
    }

}

//
//  HistoryViewController.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/10/28.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit
import RATreeView
import SVProgressHUD

class HistoryViewController: UIViewController, RATreeViewDelegate, RATreeViewDataSource, SelectDataOptionDelegate{
    
    @IBOutlet weak var tagView: SelectDataOptionButton!
    @IBOutlet weak var dayView: SelectDataOptionButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var treeView: RATreeView!
    
    let cellIdentifier = "HistoryViewCell"
    
    private var dateConditionList:[NSDate] = []
    private var tagConditionList:[String] = []
    
    var searchDataArray: [HeartbeatLogData] = []
    
    // 読み込み中のフラグ.
    var loadingFlag:Bool = false
    
    // 最新の検索データ.
    var latestSearchResult:SearchResultData?
    
    var latestSelectIndexPath:NSIndexPath!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.tabBarItem.image = UIImage(named: "icon_menu_history")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
    }
    
    // MARK: -setup
    private func setupNavigationBar() {
        // NavigationBarの下にViewが埋まってしまうことを回避
        edgesForExtendedLayout = UIRectEdge.None;
        
        title = "履歴"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        SVProgressHUD.show()
        //        loadingFlag = true
        //        HeartBeatLog.FindAll().then{data in
        //            self.loadData(data,reset: true)
        //        }
        
        setupNavigationBar()
        treeView.delegate = self
        treeView.dataSource = self
        
        treeView.registerNib(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        tagView.layer.cornerRadius = 10
        tagView.delegate = self
        dayView.layer.cornerRadius = 10
        dayView.delegate = self
        searchButton.layer.cornerRadius = 20
        
        if dateConditionList.count == 0 && tagConditionList.count == 0 {
            searchButton.backgroundColor = UIColor.lightGrayColor()
        }else {
            searchButton.backgroundColor = UIColor.strongOrangeColor()
        }
    }
    
    // MARK: -RATreeViewDataSource
    // ①
    func treeView(treeView: RATreeView!, numberOfChildrenOfItem item: AnyObject!) -> Int {
        
        if item == nil {
            return searchDataArray.count
        }
        
        let indexPath:NSIndexPath = item as! NSIndexPath
        
        if indexPath.item == 0 {
            
            let data: HeartbeatLogData = searchDataArray[indexPath.section]
            
            if data.datas != nil {
                return data.datas!.count
            }else {
                return 0
            }
            
        }else {
            return 0
        }
    }
    
    // ②
    func treeView(treeView: RATreeView!, child index: Int, ofItem item: AnyObject!) -> AnyObject! {
        //        print("treeView:child:ofItem:")
        //        print("index = \(index)")
        //        print("item = \(item)")
        
        
        if item == nil {
            // 一番上の階層のとき.
            // Itemが0のときは親のデータ.
            return NSIndexPath(forItem: 0, inSection:index)
            //            return self.searchDataArray[index]
        }else if let indexPath = item as? NSIndexPath {
            // 0は親なので、子は1から始まるように+1しておく.
            return NSIndexPath(forItem: index+1, inSection: indexPath.section)
        }else {
            return 0
        }
    }
    
    // ③
    func treeView(treeView: RATreeView!, cellForItem item: AnyObject!) -> UITableViewCell! {
        
        let cell = treeView.dequeueReusableCellWithIdentifier(cellIdentifier) as! HistoryViewCell
        //        print("treeView:cellForItem:")
        if let indexPath:NSIndexPath = item as? NSIndexPath {
            
            let logData:HeartbeatLogData!
            
            if indexPath.item == 0 {
                logData = searchDataArray[indexPath.section]
            }else {
                logData = searchDataArray[indexPath.section].datas![indexPath.item-1]
            }
            
            if indexPath.item == 0 {
                cell.tag = 0
                cell.userInteractionEnabled = true
                cell.marginLabel.text = ""
                cell.arrowImageView.hidden = false
                let df = NSDateFormatter().getFormatWidthOutTime()
                //セルの矢印表示画像を、ツリーを開いているか否かで分岐させる
                if (treeView.isCellForItemExpanded(item)) {
                    cell.arrowImageView.image = UIImage(named: "icon_arrow02")
                }else {
                    cell.arrowImageView.image = UIImage(named: "icon_arrow01")
                }
                //ここまで
                cell.dateLabel.text = df.stringFromDate(logData.measureDatetime)
                
            } else {
                cell.tag = 1
                cell.userInteractionEnabled = true
                cell.marginLabel.text = "　"
                cell.marginLabel.sizeToFit()
                cell.arrowImageView.hidden = true
                
                let df = NSDateFormatter().getTimeWidthOutSecoundFormat()
                cell.dateLabel.text = df.stringFromDate(logData.measureDatetime)
            }
            
            cell.heartRateLabel.text = String(logData.rateBpm)
            
            /*if HeartbeatLogData.calculationStressindex(logData.rateBpm) > 100 {
                logData.stressIndex = 100
            } else */
//            if HeartbeatLogData.calculationStressindex(logData.rateBpm) < 0 {
            if logData.stressIndex < 0 {
                logData.stressIndex = 0
            }
            cell.stressDegreeLabel.text = String(Int(logData.stressIndex))
        }
        return cell
    }
    
    func treeView(treeView: RATreeView!, heightForRowForItem item: AnyObject!) -> CGFloat {
        return 40
    }
    
    func treeView(treeView: RATreeView!, estimatedHeightForRowForItem item: AnyObject!) -> CGFloat {
        return 40
    }
    //MARK: -RATreeViewDelegate
    func treeView(treeView: RATreeView!, didSelectRowForItem item: AnyObject!) {
        
        if let indexPath = item as? NSIndexPath {
            
            let cell = treeView.cellForItem(item)
            
            if cell.tag == 0 {
                
                treeView.deselectRowForItem(item, animated: true)
                
                if treeView.isCellForItemExpanded(item) == true {
                    (treeView.cellForItem(item) as! HistoryViewCell).arrowImageView.image = UIImage(named: "icon_arrow01")
                } else {
                    (treeView.cellForItem(item) as! HistoryViewCell).arrowImageView.image = UIImage(named: "icon_arrow02")
                }
                
            }else {
                
                treeView.deselectRowForItem(item, animated: true)
                
                latestSelectIndexPath = indexPath
                
                let data = searchDataArray[indexPath.section].datas![indexPath.item-1]
                
                let measureResultVC:MeasureResultViewController = MeasureResultViewController()
                measureResultVC.setHeartRateData(data)
                self.navigationController?.pushViewController(measureResultVC, animated: true)
                
            }
        }
    }
    
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if self.treeView.contentOffset.y > self.treeView.contentSize.height-self.treeView.bounds.size.height {
            if loadingFlag == false && latestSearchResult?.terminal == false {
                // 検索の残りが存在していたら、さらに読み込む.
                loadingFlag = true
                SVProgressHUD.show()
                HeartBeatLog.FindByCursor((latestSearchResult?.cursor)!, filter: (latestSearchResult?.filter)!, sorts: (latestSearchResult?.sorts)!).then { data in
                    self.loadData(data,reset: false, isEnableProgressDismiss: true)
                }
            }
        }
    }
    
    /*
    * 日付選択画面から戻ってきたときのデータ受け取り部分.
    */
    internal func setDateCondition(data:[NSDate]){
        
        dateConditionList = data
        dayView.setSelectData(.calendar, data: data)
        
        if dateConditionList.count == 0 && tagConditionList.count == 0 {
            searchButton.backgroundColor = UIColor.lightGrayColor()
        }else {
            searchButton.backgroundColor = UIColor.strongOrangeColor()
        }
    }
    
    /*
    * タグ選択画面から戻ってきたときのデータ受け取り部分.
    */
    internal func setTagCondition(data:[String]){
        
        tagConditionList = data
        tagView.setSelectData(.tag, data: data)
        
        if dateConditionList.count == 0 && tagConditionList.count == 0 {
            searchButton.backgroundColor = UIColor.lightGrayColor()
        }else {
            searchButton.backgroundColor = UIColor.strongOrangeColor()
        }
    }
    /*
    * データ更新後戻って来たときのデータ受け取り部分.
    */
    internal func setNewData(data:HeartbeatLogData){
        if let _:HeartbeatLogData = searchDataArray[latestSelectIndexPath.section].datas![latestSelectIndexPath.item-1]{
            searchDataArray[latestSelectIndexPath.section].datas![latestSelectIndexPath.item-1] = data
        }
    }
    
    
    /*
    * 検索を開始.
    */
    @IBAction func startSearchHistory() {
        
        if dateConditionList.count > 0 || tagConditionList.count > 0 {
            let searchDate = getSearchDate()
            print("max:\(searchDate.max),min:\(searchDate.min)")
            SVProgressHUD.show()
            loadingFlag = true
            HeartBeatLog.FindBy(searchDate.min, to: searchDate.max, tags: tagConditionList).then{ data -> Void in
                self.loadData(data,reset: true, isEnableProgressDismiss: false)
                self.loadMoreData()
            }
        }
    }
    
    private func loadMoreData() {
        let tableMax = Int(self.treeView.frame.height / 40)
        if(self.latestSearchResult == nil || self.searchDataArray.count >= tableMax || self.latestSearchResult?.terminal == true) {
            completeLoading()
        } else if !(latestSearchResult?.terminal)! {
            HeartBeatLog.FindByCursor((self.latestSearchResult?.cursor)!, filter: (self.latestSearchResult?.filter)!, sorts: (self.latestSearchResult?.sorts)!).then { data -> Void in
                self.loadData(data,reset: false, isEnableProgressDismiss: false)
                self.loadingFlag = true
                self.loadMoreData()
            }
        }
    }
    
    func getSearchDate() -> (max:NSDate,min:NSDate){
        
        if dateConditionList.count == 0 {
            
            return (NSDate(timeIntervalSinceNow: 0),NSDate(timeIntervalSince1970: 0))
            
        }else if dateConditionList.count == 1 {
            
            let date = NSDate.getDate(dateConditionList[0].getYear(), month: dateConditionList[0].getMonth(), day: dateConditionList[0].getDay())
            
            return (NSDate(timeInterval: 24*60*60, sinceDate: date),NSDate(timeInterval:0, sinceDate: date))
            
        }else {
            var tmp_Max:NSDate = dateConditionList[0]
            var tmp_Min:NSDate = dateConditionList[0]
            
            for i in 0..<dateConditionList.count {
                if tmp_Max.compare(dateConditionList[i]) == NSComparisonResult.OrderedAscending {
                    tmp_Max = dateConditionList[i]
                }
                if tmp_Min.compare(dateConditionList[i]) == NSComparisonResult.OrderedDescending {
                    tmp_Min = dateConditionList[i]
                }
            }
            // UTCに変換するために生成しなおす.
            // Maxは選択した日分も読み込むために+1日しておく.
            tmp_Max = NSDate(timeInterval: 24*60*60, sinceDate: NSDate.getDate(tmp_Max.getYear(), month: tmp_Max.getMonth(), day: tmp_Max.getDay()))
            return (tmp_Max,NSDate.getDate(tmp_Min.getYear(), month: tmp_Min.getMonth(), day: tmp_Min.getDay()))
        }
    }
    
    func onTapButton(sender: SelectDataOptionButton) {
        switch sender.tag {
        case 1:
            let calendarVC:SelectDateConditionViewController = SelectDateConditionViewController()
            calendarVC.setSelectedDate(dateConditionList)
            //            dateConditionList.removeAll()
            navigationController?.pushViewController(calendarVC, animated: true)
        case 2:
            let tagVC:SelectTagConditionViewController = SelectTagConditionViewController()
            tagVC.setSelectedTag(tagConditionList)
            //            dateConditionList.removeAll()
            navigationController?.pushViewController(tagVC, animated: true)
        default:
            break
        }
    }
    
    func loadData(data:SearchResultData,reset:Bool,isEnableProgressDismiss:Bool) {
        
        latestSearchResult = data
        
        var datas:[HeartbeatLogData] = []
        
        if reset {
            searchDataArray = []
        }
        
        // 親のデータリストを生成.
        for d in data.result  {
            
            // データの日付が有効かどうかのフラグ.
            var dateFlag:Bool = false
            
            let date = d.measureDatetime
            
            // もしも日付指定がされていたら、そこに含まれた日付しかsearchDataArrayに追加しない.
            // 日付指定が入っていないときは全て同じ.
            if dateConditionList.count > 0 {
                // dateConditionは9時間ずれているので、そこを考慮して確認する.
                if let _ = dateConditionList.indexOf({(t:NSDate) -> Bool in
                    let jst:NSDate = NSDate(timeInterval: 9*60*60, sinceDate: t)
                    return jst.getYear() == date.getYear() && jst.getMonth() == date.getMonth() && jst.getDay() == date.getDay()
                }){
                    dateFlag = true
                }
                
            }else {
                dateFlag = true
            }
            
            if dateFlag{
                
                if let index = searchDataArray.indexOf({ (log:HeartbeatLogData) -> Bool in
                    return log.measureDatetime.getYear() == date.getYear() && log.measureDatetime.getMonth() == date.getMonth() && log.measureDatetime.getDay() == date.getDay()
                }){
                    searchDataArray[index].datas?.append(d)
                }else if let index = datas.indexOf({ (log:HeartbeatLogData) -> Bool in
                    return log.measureDatetime.getYear() == date.getYear() && log.measureDatetime.getMonth() == date.getMonth() && log.measureDatetime.getDay() == date.getDay()
                }){
                    // datasの中に存在していたら、そこに自分のデータを入れておく.
                    datas[index].datas?.append(d)
                }else {
                    // datasの中にdと同じ日付のデータが存在しなかったら、足しておく.
                    let new_log:HeartbeatLogData = HeartbeatLogData()
                    
                    new_log.measureDatetime = NSDate.getDate(date.getYear(), month: date.getMonth(), day: date.getDay())
                    
                    // 自分もこの下にデータを入れておく.
                    new_log.datas = [d]
                    datas.append(new_log)
                }
                
            }
            
        }
        
        //履歴画面でのデータの重複表示を無くす
        var noOverlapDatas = HeartbeatLogData()
        noOverlapDatas.datas = []
        
        
        for dataOfOneDay in datas {
            
            var tempDatasOfOneDay = HeartbeatLogData()
            tempDatasOfOneDay.id = dataOfOneDay.id
            tempDatasOfOneDay.measureDatetime = dataOfOneDay.measureDatetime
            tempDatasOfOneDay.rateBpm = dataOfOneDay.rateBpm
            tempDatasOfOneDay.stressIndex = dataOfOneDay.stressIndex
            tempDatasOfOneDay.tags = dataOfOneDay.tags
            tempDatasOfOneDay.memo = dataOfOneDay.memo
            tempDatasOfOneDay.datas = []
            
            for (var i = 0; i < dataOfOneDay.datas!.count; i++) {
                
                let tempData = dataOfOneDay.datas![i]
                var exist = false
                for (var j = 0; j < tempDatasOfOneDay.datas!.count; j++) {
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
                    
                    let overlapData = tempDatasOfOneDay.datas![j]
                    let overlapDataString = dateFormatter.stringFromDate(overlapData.measureDatetime)
                    let tempDataString = dateFormatter.stringFromDate(tempData.measureDatetime)
                    
                    if (overlapDataString == tempDataString) {
                        //同じ時分のデータが存在する場合
                        exist = true
                        break
                    }
                    
                }
                
                if exist == false {
                    
                    tempDatasOfOneDay.datas?.append(tempData)
                }
            }
            noOverlapDatas.datas?.append(tempDatasOfOneDay)
        }
        datas = noOverlapDatas.datas!
        //ここまで
        
        for d in datas {
            
            // データの平均値を計算する.
            var sum = 0
            for i:Int in 0..<(d.datas?.count)! {
                sum += d.datas![i].rateBpm
            }
            d.rateBpm = Int(sum/(d.datas?.count)!)
            d.stressIndex = HeartbeatLogData.calculationStressindex(d.rateBpm)
            
            if HeartbeatLogData.calculationStressindex(d.rateBpm) > 100 {
                d.stressIndex = 100
            } else if HeartbeatLogData.calculationStressindex(d.rateBpm) < 0 {
                d.stressIndex = 0
            }
            
            searchDataArray.append(d)
        }
        
        self.treeView.reloadData()
        
        if (isEnableProgressDismiss) {
            completeLoading()
        }
    }
    
    func treeView(treeView: RATreeView!, canEditRowForItem item: AnyObject!) -> Bool {
        return false
    }
    
    private func completeLoading() {
        SVProgressHUD.dismiss()
        loadingFlag = false
    }
}
//
//  AppDelegate.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/10/28.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit
import HealthKit
import KeychainAccess
import SVProgressHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        SVProgressHUD.setDefaultMaskType(.Gradient)
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [UIUserNotificationType.Sound, UIUserNotificationType.Alert, UIUserNotificationType.Badge], categories: nil))
        
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        if UserDefaultsManager.isFirstLogin() == true {
            let loginVC = LoginViewController()
            window?.rootViewController = loginVC
            
            // 以前アプリをインストールしていた時のデータが残っている可能性があるので
            // Keychainに登録されている可能性のある値をリセットする
            KeychainManager.removeAllID()
            
            // 初回起動時の定期的なNotification登録。初期設定は毎日21時のNotification
            UserDefaultsManager.setPeriodicNotificationTime(21, minutes: 0)
            PeriodicNotification.registerPeriodicNotification()
            
//            //HealthKitへのアクセス許可を取りに行く
//            // 書き込みを許可する型.
//            let typeOfWrites = Set([HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!])
//            
//            // 読み込みを許可する型.
//            // HKCharacteristicTypeIdentifierDateOfBirthは、readしかできない.
//            let typeOfReads = Set([HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!])
//            
//            //  HealthStoreへのアクセス承認をおこなう.
//            let healthStore = HKHealthStore()
//            healthStore.requestAuthorizationToShareTypes(typeOfWrites, readTypes: typeOfReads, completion: {
//                success, error in
//                if success {
//                    print("Success!")
//                } else {
//                    print("Error!")
//                    print(error.debugDescription);
//                }
//            })

            
        } else if BaseHeartRateManager.getBaseHeartRate() == nil {
            let measureVC = MeasureViewController(isBaseHeartRate: true)
            let measureNC = UINavigationController(rootViewController: measureVC)
            
            UINavigationBar.appearance().barTintColor = UIColor.navBarOrangeColor()
            UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
            
            window?.rootViewController = measureNC
        } else {
            let tabBarController = setupTabBarController(UserDefaultsManager.isFirstLogin())
            window?.rootViewController = tabBarController
            
            // 二回目以降の起動時にはNotificationを登録し直す
            PeriodicNotification.registerPeriodicNotification()
        }

        window?.makeKeyAndVisible()

        return true
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
    
    func applicationShouldRequestHealthAuthorization(application: UIApplication) {
        let healthStore = HKHealthStore()
        guard HKHealthStore.isHealthDataAvailable() else {
            return
        }
        
        let dataTypes = Set([HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!])
        healthStore.requestAuthorizationToShareTypes(nil, readTypes: dataTypes, completion: { (result, error) -> Void in
        })
    }
}


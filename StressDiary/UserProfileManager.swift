//
//  UserProfileManager.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/25.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation

/* ユーザープロフィールの管理 */
struct UserProfileManager {
    
    enum ProfileType {
        case birth
        case gender
        case weight
        case height
        case area
        case tag
    }
    // サーバーに最新のデータをリクエスト.
    static func requestLatestUpdate(){
        
        print("requestLatestUpdate")
        UserProfile.getUserProfile().then { data in
            saveUserProfile(data)
        }
    }
    
    static func saveUserProfile(profile:UserProfileData){
        
        print("saveUserProfile")
        NSUserDefaults.standardUserDefaults().setValuesForKeysWithDictionary(
            [
                "birth" : profile.birthday,
                "gender": profile.gender,
                "weight": profile.weightKg,
                "height": profile.heightCm,
                "area": profile.prefectureCode,
                "tag": profile.defaultTags
            ]
        )
    }
    
    // UserDefaultに保存されている最新のプロフィールデータを取得(一部).
    static func getUserProfile(type:ProfileType)->AnyObject?{
        
        switch type {
        case .birth:
            return NSUserDefaults.standardUserDefaults().objectForKey("birth")
        case .gender:
            return NSUserDefaults.standardUserDefaults().objectForKey("gender")
        case .weight:
            return NSUserDefaults.standardUserDefaults().objectForKey("weight")
        case .height:
            return NSUserDefaults.standardUserDefaults().objectForKey("height")
        case .area:
            return NSUserDefaults.standardUserDefaults().objectForKey("area")
        case .tag:
            return NSUserDefaults.standardUserDefaults().objectForKey("tag")
        }
        
        
    }
    // UserDefaultに保存されている最新のプロフィールデータを取得(全て).
    // 保存されていなければ、init状態のデータを渡す.
    static func getUserAllProfile()->UserProfileData?{
        let profile:UserProfileData = UserProfileData()
        profile.birthday = NSUserDefaults.standardUserDefaults().objectForKey("birth") as! NSDate
        profile.gender = NSUserDefaults.standardUserDefaults().objectForKey("gender") as! String
        profile.weightKg = NSUserDefaults.standardUserDefaults().objectForKey("weight") as! Double
        profile.heightCm = NSUserDefaults.standardUserDefaults().objectForKey("height") as! Double
        profile.prefectureCode = NSUserDefaults.standardUserDefaults().objectForKey("area") as! String
        profile.defaultTags = NSUserDefaults.standardUserDefaults().objectForKey("tag") as! [String]
        
        return profile
    }
    // サーバーの更新&UserDefaultの更新.
    static func updateUserProfile(type:ProfileType,value:AnyObject){
        
        // 保存されている最新を取得.
        let profile:UserProfileData = getUserAllProfile()!
        
        switch type {
        case .birth:
            profile.birthday = value as! NSDate
        case .gender:
            profile.gender = value as! String
        case .weight:
            profile.weightKg = value as! Double
        case .height:
            profile.heightCm = value as! Double
        case .area:
            profile.prefectureCode = value as! String
        case .tag:
            profile.defaultTags = value as! [String]
        }
        
        UserProfile.updateUserProfile(profile)
    }
    
}

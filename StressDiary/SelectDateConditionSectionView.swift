//
//  SelectDateConditionSectionView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/20.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

class SelectDateConditionSectionView: UICollectionReusableView {
    
    @IBOutlet weak var titleLabel:UILabel!
    
    private let xibName:String = "SelectDateConditionSectionView"
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        if let view:UIView = NSBundle.mainBundle().loadNibNamed(xibName, owner:self, options: nil).first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
        }
        
    }
}

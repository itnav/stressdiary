//
//  SearchInfoData.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/23.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation

/* 検索結果のデータモデル */
class SearchResultData {
    
    var cursor:String!
    var filter:String!
    var sorts:String!
    var terminal:Bool!
    var result:[HeartbeatLogData]!
    
    init() {
        self.cursor = ""
        self.filter = ""
        self.sorts = ""
        self.terminal = false
        self.result = []
    }
    
    init(data:SearchResultData){
        self.cursor = data.cursor
        self.filter = data.filter
        self.sorts = data.sorts
        self.terminal = data.terminal
        self.result = data.result
    }
    
    internal func setSearchInfoData(data:NSDictionary){
        
        self.cursor = data.objectForKey("cursor") as! String
        self.filter = data.objectForKey("filter") as! String
        self.sorts = data.objectForKey("sorts") as! String
        self.terminal = data.objectForKey("terminal") as! Bool
        
        // 結果の初期化
        self.result = []
        
        // resultの中に入れ子になっているresultを分解してHeartBeatLogDataに.
        for d:NSDictionary in (data.objectForKey("result") as? [NSDictionary])! {
            let newData:HeartbeatLogData = HeartbeatLogData()
            newData.setHeartbeatLog(d)
            self.result.append(newData)
        }
    }
    
    internal func appendSearchResult(data:SearchResultData){
        
        // 今のデータに追加する.
        // 次の検索を行うためのプロパティは更新.
        self.cursor = data.cursor
        self.filter = data.filter
        self.sorts = data.sorts
        self.terminal = data.terminal
        
        // resultはappend
        self.result? += data.result

    }
}
//
//  AdviceMessage.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/19.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import SVProgressHUD

/* 測定結果のアドバイス取得の実処理 */
struct AdviceMessage {
    
    static func FindByStressIndex(stressIndex:Double)->Promise<AdviceMessageData>{
        
        let advice:AdviceMessageData = AdviceMessageData()
        
        return Promise { fulfill, reject in
            
            let request = Alamofire.request(AdviceMessageRouter.FindByStressIndex(stressIndex: stressIndex))
            request.response { request, response, data, error in
                print("adviceMessage.findByStressIndex")
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                
                if let _ = error {
                    ErrorProcess.communicationError()
                    reject(CommunicationError.NoCommunicationLine)
                } else {
                    do {
                        if let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary {
                            if json.objectForKey("error") as? String == nil {
                                if let result = json.objectForKey("result") as? NSDictionary {
                                    advice.setAdviceMessageData(result)
                                }
                            }
                            fulfill(advice)
                        }
                    } catch {
                        reject(CommunicationError.InvalidJSON)
                    }
                }
            }
        }
    }
}
//
//  SelectTagConditionView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/20.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit
import UICollectionViewLeftAlignedLayout

class SelectTagConditionView: UIView,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,SelectTagConditionCellViewDelegate{
    
    @IBOutlet weak var tagCollectionView:UICollectionView!
    @IBOutlet weak var EditTagButton:UIButton!
    
    
    internal var tagList:[String] = []
    
    internal var selectTags:[String] = []
    
    internal var parentViewControlelr:UIViewController!
    
    
    internal var editMode:Bool = false
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tagCollectionView.delegate = self
        tagCollectionView.dataSource = self
        tagCollectionView.layer.cornerRadius = 10
        
        let leftLayout:UICollectionViewLeftAlignedLayout = UICollectionViewLeftAlignedLayout()
        
        tagCollectionView.setCollectionViewLayout(leftLayout, animated: true)
        
        tagCollectionView.registerNib(UINib.init(nibName: "SelectTagConditionViewCell" , bundle: nil), forCellWithReuseIdentifier: "cell")
        tagCollectionView.registerNib(UINib.init(nibName: "EditTagButtonCell" , bundle: nil), forCellWithReuseIdentifier: "add")
        
        tagCollectionView.allowsMultipleSelection = true
    }
    
    internal func setEditTagMode(){
        
        // すべてのcellを編集モードにする.
        let cells = tagCollectionView.visibleCells()
        
        editMode = !editMode
        
        if editMode {
            EditTagButton.setTitle("編集完了", forState: .Normal)
        }else {
            EditTagButton.setTitle("タグを編集", forState: .Normal)
        }
        for cell in cells{
            if let c:SelectTagConditionCellView = cell as? SelectTagConditionCellView {
                c.setEditMode(editMode, anim: true)
            }else if let c:EditTagButtonCell = cell as? EditTagButtonCell {
                c.hidden = !editMode
            }
        }
    }
    internal func setUserTagList(data:[String]) {
        self.tagList = data
        tagCollectionView.reloadData()
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 2
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return tagList.count
        }else {
            return 1
        }
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        // sectionが2のときには通常のタグを表示.
        // 2のときには一つだけ、編集ボタンを入れておく.
        if indexPath.section == 0 {
            
            let cell:SelectTagConditionCellView = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! SelectTagConditionCellView
            cell.dateLabel.text = "\(tagList[indexPath.row])"
            cell.delegate = self
            
            if selectTags.indexOf(tagList[indexPath.row]) != nil {
                tagCollectionView.selectItemAtIndexPath(indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.None)
                cell.selectCell()
            }else {
                cell.deSelectCell()
            }
            return cell
        }else{
            let cell:EditTagButtonCell = collectionView.dequeueReusableCellWithReuseIdentifier("add", forIndexPath: indexPath) as! EditTagButtonCell
            cell.hidden = !editMode
            return cell
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if editMode == false {
            changeCellSelect(indexPath)
        }else if let _:EditTagButtonCell = collectionView.cellForItemAtIndexPath(indexPath) as? EditTagButtonCell {
            showAddNewTagAlert()
        }
    }
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        
        if editMode == false {
            changeCellSelect(indexPath)
        }else if let _:EditTagButtonCell = collectionView.cellForItemAtIndexPath(indexPath) as? EditTagButtonCell {
            showAddNewTagAlert()
        }
    }
    func changeCellSelect(indexPath:NSIndexPath){
        if let cell:SelectTagConditionCellView = self.tagCollectionView.cellForItemAtIndexPath(indexPath) as? SelectTagConditionCellView {
            if selectTags.indexOf(tagList[indexPath.row]) == nil {
                selectTags.append(tagList[indexPath.row])
                cell.selectCell()
            }else {
                selectTags.removeAtIndex(selectTags.indexOf(tagList[indexPath.row])!)
                cell.deSelectCell()
            }
        }
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        if let c:SelectTagConditionCellView = cell as? SelectTagConditionCellView {
            c.setEditMode(editMode, anim:false)
        }
    }
    func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        if let c:SelectTagConditionCellView = cell as? SelectTagConditionCellView {
            
            // 一度初期位置に戻す.
            c.setEditMode(false, anim: false)
        }
    }
    
     
    /*
    * UICollectionViewDelegateFlowLayoutのメソッド.
    */
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        if indexPath.section == 0 {
            // 文字の長さからLabelの横幅をだいたい取得.
            let label:UILabel = UILabel()
            label.text = tagList[indexPath.row]
            label.sizeToFit()
            
            if label.frame.width < 50 {
                return CGSizeMake(label.frame.width+80,50)
            }else if label.frame.width > self.frame.width-20 {
                // タグが横に大きすぎる場合たてを少し大きく.
                //だいたいの行数を取得.
                let line = label.frame.width/(self.frame.width-20)
                return CGSizeMake(self.frame.width-20,line*40)
            }else{
                return CGSizeMake(label.frame.width+70,50)
            }
        }else {
            return CGSizeMake(130,50)
        }
        
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(10, 10, 10, 10);
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return -10
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 5
    }
    func selectDeleteTag(deleteTag: String) {
        
        if let index = tagList.indexOf(deleteTag) {
            tagList.removeAtIndex(index)
            // tagが選択されていたらそれも消す.
            if selectTags.indexOf(deleteTag) != nil {
                selectTags.removeAtIndex(selectTags.indexOf(deleteTag)!)
            }
            tagCollectionView.deleteItemsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)])
        }
        
    }
    
    private func showAddNewTagAlert(){
        // すでにタグリストの数が20以上だったら、登録できない旨を伝える.
        if tagList.count >= 20 {
            let alert:UIAlertController = UIAlertController(title:"タグの追加",
                message: "タグの保存上限数は20個です.",
                preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction:UIAlertAction = UIAlertAction(title: "ok",style: UIAlertActionStyle.Default,handler:nil)
            alert.addAction(okAction)
            parentViewControlelr.presentViewController(alert, animated: true, completion: nil)
        }else {
            let alert:UIAlertController = UIAlertController(title:"タグの新規作成",
                message: "新しいタグを入力してください",
                preferredStyle: UIAlertControllerStyle.Alert)
            
            let cancelAction:UIAlertAction = UIAlertAction(title: "Cancel",
                style: UIAlertActionStyle.Cancel,
                handler:{
                    (action:UIAlertAction!) -> Void in
            })
            let okAction:UIAlertAction = UIAlertAction(title: "保存",
                style: UIAlertActionStyle.Default,
                handler:{
                    (action:UIAlertAction!) -> Void in
                    if (alert.textFields![0] as UITextField).text?.stringByReplacingOccurrencesOfString(" ", withString: "") != "" {
                        self.tagList.append((alert.textFields![0] as UITextField).text!)
                        self.tagCollectionView.performBatchUpdates({ () -> Void in
                            
                            self.tagCollectionView.insertItemsAtIndexPaths([NSIndexPath(forRow: self.tagList.count-1, inSection: 0)])
                            }, completion: { (finished) -> Void in
                                if let cell:SelectTagConditionCellView = self.tagCollectionView.cellForItemAtIndexPath(NSIndexPath(forRow: self.tagList.count-1, inSection: 0)) as? SelectTagConditionCellView  {
                                    cell.setEditMode(true, anim: true)
                                }
                        })
                        
                    }
            })
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            
            //tag入力のTextField
            alert.addTextFieldWithConfigurationHandler({(text:UITextField!) -> Void in
                
            })
            
            parentViewControlelr.presentViewController(alert, animated: true, completion: nil)
        }
    }
}
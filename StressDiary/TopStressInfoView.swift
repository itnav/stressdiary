//
//  TopStressInfoView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/08.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

class TopStressInfoView: UIView {
    
    private let xibName:String = "TopStressInfoView"
    
    @IBOutlet weak var outerFrame:UIView!
    @IBOutlet weak var lastMeasureResultView:LastMeasureResultView!
    @IBOutlet weak var baseHeatRateInfoView:BaseHeartRateInfoView!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupXib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupXib()
    }
    
    /*
    * xibのレイアウトファイルの読み込み.
    */
    private func setupXib(){
//        print("setupXib:\(xibName)")
        
        if let view:UIView = NSBundle.mainBundle().loadNibNamed(xibName, owner:self, options: nil).first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
        }
        
        outerFrame.layer.borderColor = UIColor.lightOrangeColor().CGColor
        outerFrame.layer.cornerRadius = 20.0
        outerFrame.layer.borderWidth = 3
    }
    
    internal func setHeartRateData(data:HeartbeatLogData){
        lastMeasureResultView.loadData(data)
        baseHeatRateInfoView.reloadData()
    }
}
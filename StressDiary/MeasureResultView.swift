//
//  MeasureResultView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/12.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

class MeasureResultView: UIView {
    
    private let xibName:String = "MeasureResultView"
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
//        setupXib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
//        setupXib()
    }
    
    /*
    * xibのレイアウトファイルの読み込み.
    */
    private func setupXib(){
//        print("setupXib:\(xibName)")
        
        if let view:UIView = NSBundle.mainBundle().loadNibNamed(xibName, owner:self, options: nil).first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
        }
    }
}
//
//  SNSError.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/12/01.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation

/* SNS連携エラー */
enum SNSError: ErrorType {
    case NoTwitterAccount
    case TwitterAuthentication
    
    case NoFacebookAccount
    case FacebookAuthentication
    case FacebookSocialAuthentication
    
    case NoLineApp
    
    case AccountDuplication
}

//detachError
//accountDuplicationError
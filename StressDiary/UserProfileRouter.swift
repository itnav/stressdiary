//
//  UserProfileRouter.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/18.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import Alamofire

/* ユーザープロフィールのルーター
Routerの設計はAlamofireのサンプルから。
*/
enum UserProfileRouter: URLRequestConvertible {
    case Find()
    case Put(data:UserProfileData)
    
    private var URLString: String {
        return "http://stress-diary.appspot.com/rpc.json"
    }

    private var HTTPBody: NSData? {
        switch self {
        case .Find():
            let values = [
                "method": "userProfile.find",
                "id": 1,
                "params":[[]
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
        case .Put(let data):
            let values = [
                "method": "userProfile.put",
                "id": 1,
                "params":[
                    [
                    "birthday": getSendAPIDateFormat(data.birthday),
                    "gender": data.gender,
                    "weightKg": data.weightKg,
                    "heightCm": data.heightCm,
                    "prefectureCode": data.prefectureCode,
                    "defaultTags": data.defaultTags,
                    ]
                ]
            ]
            
            return try! NSJSONSerialization.dataWithJSONObject(values, options: [])
        }
    }

    // TODO: HeaderはUserDefaults管理。currentAuthIDからとってくる！
    private var HTTPHeaderValue: String? {
        switch self {
        default:
            let currentAuthType = KeychainManager.getCurrentAuthType()!
            var currentAuthID: String = ""
            
            if currentAuthType == "DEVICE" {
                currentAuthID = KeychainManager.getOriginalID()!
            } else if currentAuthType == "FACEBOOK" {
                currentAuthID = KeychainManager.getFacebookID()!
            } else if currentAuthType == "TWITTER" {
                currentAuthID = KeychainManager.getTwitterID()!
            }
            
            let values = [
                "authType": currentAuthType,
                "id": currentAuthID,
                "authHash": "H2mRVD7C\(currentAuthType)Adpy_jEA\(currentAuthID)bgjsewn2".sha256String().sha256String().sha256String()
            ]
            let JSON = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
            return String(data: JSON, encoding: NSUTF8StringEncoding)
        }
    }
    
    var URLRequest: NSMutableURLRequest {
        let mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: URLString)!)
        mutableURLRequest.HTTPMethod = Alamofire.Method.POST.rawValue
        mutableURLRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        mutableURLRequest.HTTPBody = HTTPBody
        
        if let header = HTTPHeaderValue {
            mutableURLRequest.addValue(header, forHTTPHeaderField: "X-Stress-Diary-Authentication")
        }
        
        return mutableURLRequest
    }
}
//
//  LocalWebViewController.swift
//  StressDiary
//
//  Created by Timecapsule115 on 2016/03/19.
//  Copyright © 2016年 itnav. All rights reserved.
//

import UIKit

class LocalWebViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet var webView: UIWebView? = UIWebView()
    var fileName : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // デリゲートを指定する
        self.webView!.delegate = self;
        
        // 全画面表示にする
        self.webView!.frame = self.view.bounds
        
        // サブビューを追加する
        self.view.addSubview(self.webView!)
        
        // index.htmlのパスを取得する
        let path = NSBundle.mainBundle().pathForResource(fileName, ofType: "html")!
        let url = NSURL(string: path)!
        
        // リクエストを生成する
        let urlRequest = NSURLRequest(URL: url)
        
        // 指定したページを読み込む
        self.webView!.loadRequest(urlRequest)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//
//  ViewController.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/10/28.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit
import SVProgressHUD
import HealthKit

class NewsViewController: UIViewController {
    
    @IBOutlet weak var topStressInfoView:TopStressInfoView!
    private var isFirstLoad = true
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.tabBarItem.image = UIImage(named: "icon_memu_news")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        
        // 一件のテストデータの送信テスト
        //        print("now date : \(NSDate())")
        //        HeartBeatLog.Add(HeartbeatLogData(date: NSDate(), bpm:85, tags: [], memo: "testData"))
        
        //        配列でまとめたテストデータの送信テスト
        //         1日あたり10個のデータを過去31日分ランダムに生成.
        //        var datas: [[String: AnyObject]] = []
        //        for var j:Int=0; j >= -31; j-- {
        //            for var i:Int=0; i<10; i++ {
        //                let date:NSDate = NSDate(timeInterval: Double(getRandomNumber(Min: -24*60*60, Max: 0)), sinceDate: NSDate().getDate(j))
        //                print("add data : \(date)")
        //
        //                let rateBpm = Int(getRandomNumber(Min: 80, Max: 100))
        //                var stressIndex = Int(HeartbeatLogData.calculationStressindex(rateBpm))
        //
        //                if stressIndex > 100 {
        //                    stressIndex = 100
        //                } else if stressIndex < 0 {
        //                    stressIndex = 0
        //                }
        //
        //                let dic =
        //                [
        //                    "measureDatetime": getSendAPIDateFormat(date),
        //                    "rateBpm": rateBpm,
        //                    "stressIndex": stressIndex,
        //                    "tags":[],
        //                    "memo":""
        //                ]
        //                datas.append(dic)
        //            }
        //        }
        //        HeartBeatLog.AddAll(datas)
    }
    
    
    func getRandomNumber(Min _Min : Float, Max _Max : Float)->Float {
        
        return ( Float(arc4random_uniform(UINT32_MAX)) / Float(UINT32_MAX) ) * (_Max - _Min) + _Min
    }
    
    
    private func setupNavigationBar() {
        
        // NavigationBarの下にViewが埋まってしまうことを回避
        edgesForExtendedLayout = UIRectEdge.None;
        title = "ニュース"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(KeychainManager.getOriginalID())
        
        setupNavigationBar()
        
        // for test
        /*
        var heartbeatLogs: [[String: AnyObject]] = []
        for _ in 1...500 {
            //            writeData(Double(arc4random_uniform(20)) + 60)
            let measureDate = NSDate()
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let measureDateString = dateFormatter.stringFromDate(measureDate)
            let dic : [String : AnyObject]  =
                [
                    "measureDatetime": measureDateString,
                    "rateBpm": Int(arc4random_uniform(40)),
                    "stressIndex": Int(arc4random_uniform(40)),
                    "tags":[],
                    "memo":""
            ]
            heartbeatLogs.append(dic as! [String : AnyObject])
        }
        
        
        var completed: Bool = false
        HeartBeatLog.AddAll(heartbeatLogs).then { complete in
            completed = complete
            }.always {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                if completed == true {
                    print("SAVED")
                } else {
                    print("SAVE ERROR")
                }
        }*/
   
    
        
    
        BaseHeartRateManager.updateBaseHeartRate {
            
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if isFirstLoad {
            // 一番古いヘルスケアのデータの日付からバッチ処理を走らせたことがなければ
            SVProgressHUD.show()
            if UserDefaultsManager.getLastBatchDateString() == nil {
                HeartBeatLogBatch.getOldestHeartbeatDateString {
                    self.downloadAllData()
                    SVProgressHUD.dismiss()
                }
            } else { // あった場合
                HeartBeatLogBatch.getHeartbeatDateFromLastBatchDate {
                    self.downloadAllData()
                    SVProgressHUD.dismiss()
                }
            }
            isFirstLoad = false
            SVProgressHUD.dismiss()
        } else {
            downloadAllData()
        }
        
        var lastHeartBeartLog = HeartbeatLogData(date: NSDate(), bpm: 60, tags: [], memo: "")
        // 最後にサーバーに保存したデータを取得
        HeartBeatLog.FindLast().then { heartBeatLogData in
            lastHeartBeartLog = heartBeatLogData
        }.always {
            self.configureTopStressInfoView(lastHeartBeartLog)
        }.error { (errorType) in
            // load local health kit data when connection error
            self.applyTopStresInfoViewHeartRateFromHealthKit()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NewsViewController.setUpDataDidCompleteDownload(_:)), name: SetUpDataManager.notificationNameCompleteDownload, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    private func downloadAllData() {
        if SetUpDataManager.sharedInstance.isDownLoadedAllData() == false {
            // 一括データダウンロードのインジケーターを生成.
            // TODO: プログレスが非表示ならない端末があるため一旦けしました
            //            SVProgressHUD.show()
            // データの一括ダウンロード.
            SetUpDataManager.sharedInstance.setUpNewsData()
        }
    }
    
    /// compare lastHeartBeatLog with local HealthKit data and use newest
    private func configureTopStressInfoView(lastHeartBeatLog: HeartbeatLogData) {
        self.requestLatestHeartRateFromHealthKit({ (result) in
            if result?.startDate != nil && NSCalendar.currentCalendar().compareDate((result?.startDate)!, toDate: lastHeartBeatLog.measureDatetime, toUnitGranularity: .Minute) == .OrderedDescending {
                // healthkit data is newer than lastHeartBeatLog and healthkit data is not nil
                self.topStressInfoView.setHeartRateData(HeartbeatLogData(sample: result))
            } else {
                self.applyTopStressInfoViewHeartRateData(lastHeartBeatLog)
            }
        })
    }
    
    private func requestLatestHeartRateFromHealthKit(resultHandler: (HKQuantitySample?) -> Void) {
        let healthStore = HKHealthStore()
        
        let sampleType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!
        let timeSortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        
        let heartRateQuery = HKSampleQuery(sampleType: sampleType, predicate: nil, limit: 1, sortDescriptors: [timeSortDescriptor], resultsHandler: { query, results, error in
            guard let result  = results as? [HKQuantitySample] else {
                fatalError("適切でない数値");
            }
            dispatch_async(dispatch_get_main_queue(), {
                if result.count != 0{
                    resultHandler(result[0])
                }else{
                    resultHandler(nil)
                }
            })
        })
        healthStore.executeQuery(heartRateQuery)
    }
    
    private func applyTopStressInfoViewHeartRateData(logData:HeartbeatLogData) {
        self.topStressInfoView.setHeartRateData(logData)
    }
    
    private func applyTopStresInfoViewHeartRateFromHealthKit() {
        self.requestLatestHeartRateFromHealthKit({ (result) in
            self.topStressInfoView.setHeartRateData(HeartbeatLogData(sample: result))
        })
    }
    
    
    // NSNotification
    func setUpDataDidCompleteDownload(sender: NSNotification) {
        if let latest_data = BaseHeartRateManager.getLatestHeartRate() {
            configureTopStressInfoView(latest_data)
            // reload latest data
            self.topStressInfoView.baseHeatRateInfoView.reloadData()
        }

    }
    
    /// for test
    private func writeData(heart:Double){
        let healthStore = HKHealthStore()
        guard HKHealthStore.isHealthDataAvailable() else {
            return
        }
        let dataTypes = Set([HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!])
        healthStore.requestAuthorizationToShareTypes(dataTypes, readTypes: dataTypes, completion: { (result, error) -> Void in
        })
        
        
        var myHealthStore : HKHealthStore!
        myHealthStore = HKHealthStore()
        
        // Save the user's heart rate into HealthKit.
        let heartRateUnit: HKUnit = HKUnit.countUnit().unitDividedByUnit(HKUnit.minuteUnit())
        let heartRateQuantity: HKQuantity = HKQuantity(unit: heartRateUnit, doubleValue: heart)
        
        let heartRate : HKQuantityType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!
        let nowDate: NSDate = NSDate()
        
        let heartRateSample: HKQuantitySample = HKQuantitySample(type: heartRate
            , quantity: heartRateQuantity, startDate: nowDate, endDate: nowDate)
        
        myHealthStore.saveObject(heartRateSample) { (success, error) in
            if success {
                print("OK")
            } else {
                print("An error occured saving the Heart Rate \(heartRateSample). In your app, try to handle this gracefully. The error was: \(error).")
//                abort()
            }
        }
    }

}


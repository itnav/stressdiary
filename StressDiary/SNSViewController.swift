//
//  SNSViewController.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/11/25.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit
import SVProgressHUD

class SNSViewController: UIViewController {
    
    @IBOutlet weak var facebookSwitch: UISwitch!
    @IBOutlet weak var twitterSwitch: UISwitch!
    
    @IBOutlet weak var facebookView: UIView!
    @IBOutlet weak var twitterView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupSwitches()
        setupViews()
        view.backgroundColor = UIColor.lightOrangeColor()
    }
    
    private func setupNavigationBar() {
        // NavigationBarの下にViewが埋まってしまうことを回避
        edgesForExtendedLayout = UIRectEdge.None;
        title = "SNS連携"
    }
    
    private func setupViews() {
        let color = UIColor.navBarOrangeColor().CGColor
        let size = CGSizeMake(0.5, 0.5)
        let opacity: Float = 1.0
        let radius: CGFloat = 3.0
        
        facebookView.layer.shadowColor = color
        twitterView.layer.shadowColor = color
        
        facebookView.layer.shadowOffset = size
        twitterView.layer.shadowOffset = size
        
        facebookView.layer.shadowOpacity = opacity
        twitterView.layer.shadowOpacity = opacity
        
        facebookView.layer.shadowRadius = radius
        twitterView.layer.shadowRadius = radius
        
//        bgView.layer.shadowColor = UIColor.navBarOrangeColor().CGColor
//        bgView.layer.shadowOffset = CGSizeMake(0.5, 0.5)
//        bgView.layer.shadowOpacity = 1.0
//        bgView.layer.shadowRadius = 3.0
    }
    
    private func setupSwitches() {
        print(isLastLocalID())
        print(KeychainManager.getCurrentAuthType())
        print("FacebookID = \(KeychainManager.getFacebookID())")
        print("TwitterID = \(KeychainManager.getTwitterID())")
        
        if let _ = KeychainManager.getFacebookID() {
            facebookSwitch.on = true
        }
        if let _ = KeychainManager.getTwitterID() {
            twitterSwitch.on = true
        }
    }
    
    @IBAction func switchFacebook(sender: AnyObject) {
        var completion: Bool = false
        
        // off -> onの処理。attach
        if facebookSwitch.on == true {
            var fbID = ""
            
            Login.getFacebookID().then { facebookID in
                fbID = facebookID
            }.then {
                UserInfo.attachAuthInfo("FACEBOOK", id: fbID)
            }.always {
                if fbID != "" {
                    KeychainManager.setFacebookID(fbID)
                    KeychainManager.setCurrentAuthID(fbID)
                    KeychainManager.setCurrentAuthType("FACEBOOK")
                    SVProgressHUD.dismiss()
                }
            }.error { error in
                if (error as NSError).domain == "AuthenticationException: User registered." {
                    ErrorProcess.accountDuplicationError()
                }
                
                KeychainManager.removeFacebookID()
                if let originalID = KeychainManager.getOriginalID() {
                    KeychainManager.setCurrentAuthID(originalID)
                    KeychainManager.setCurrentAuthType("DEVICE")
                } else if let twitterID = KeychainManager.getTwitterID() {
                    KeychainManager.setCurrentAuthID(twitterID)
                    KeychainManager.setCurrentAuthType("TWITTER")
                }
                
                SVProgressHUD.dismiss()
                self.facebookSwitch.setOn(false, animated: true)
            }
        // on -> offの処理。detach
        } else {
            // 最後のIDでなかったら
            if isLastLocalID() == false {
                UserInfo.detachAuthInfo("FACEBOOK", id: KeychainManager.getFacebookID()!).then { _completion in
                    completion = _completion
                }.always {
                    if completion == true {
                        if let originalID = KeychainManager.getOriginalID() {
                            KeychainManager.setCurrentAuthType("DEVICE")
                            KeychainManager.setCurrentAuthID(originalID)
                            KeychainManager.removeFacebookID()
                            print(KeychainManager.getCurrentAuthType())
                        } else if let twitterID = KeychainManager.getTwitterID() {
                            KeychainManager.setCurrentAuthType("TWITTER")
                            KeychainManager.setCurrentAuthID(twitterID)
                            KeychainManager.removeFacebookID()
                            print(KeychainManager.getCurrentAuthType())
                        }
                    }
                }.error { error in
                    SVProgressHUD.dismiss()
                    self.facebookSwitch.setOn(true, animated: true)
                }
            // 最後のIDだったら
            } else {
                ErrorProcess.detachError()
                facebookSwitch.setOn(true, animated: true)
            }
        }
    }
    
    @IBAction func switchTwitter(sender: AnyObject) {
        var completion: Bool = false
        
        // off -> onの処理。attach
        if twitterSwitch.on == true {
            var twID = ""
            
            Login.getTwitterID().then { twitterID in
                twID = twitterID
            }.then {
                UserInfo.attachAuthInfo("TWITTER", id: twID)
            }.always {
                print("twID = \(twID)")
                if twID != "" {
                    KeychainManager.setTwitterID(twID)
                    KeychainManager.setCurrentAuthID(twID)
                    KeychainManager.setCurrentAuthType("TWITTER")
                }
                SVProgressHUD.dismiss()
            }.error { error in
                if (error as NSError).domain == "AuthenticationException: User registered." {
                    // アカウント重複エラー
                    KeychainManager.removeTwitterID()
                    if let originalID = KeychainManager.getOriginalID() {
                        KeychainManager.setCurrentAuthID(originalID)
                        KeychainManager.setCurrentAuthType("DEVICE")
                    } else if let facebookID = KeychainManager.getFacebookID() {
                        KeychainManager.setCurrentAuthID(facebookID)
                        KeychainManager.setCurrentAuthType("FACEBOOK")
                    }
                    
                    ErrorProcess.accountDuplicationError()
                }
                
                SVProgressHUD.dismiss()
                self.twitterSwitch.setOn(false, animated: true)
            }
            // on -> offの処理。detach
        } else {
            // 最後のIDでなかったら
            if isLastLocalID() == false {
                UserInfo.detachAuthInfo("TWITTER", id: KeychainManager.getTwitterID()!).then { _completion in
                    completion = _completion
                }.always {
                    if completion == true {
                        if let facebookID = KeychainManager.getFacebookID() {
                            KeychainManager.setCurrentAuthType("FACEBOOK")
                            KeychainManager.setCurrentAuthID(facebookID)
                            KeychainManager.removeTwitterID()
                        } else if let originalID = KeychainManager.getOriginalID() {
                            KeychainManager.setCurrentAuthType("DEVICE")
                            KeychainManager.setCurrentAuthID(originalID)
                            KeychainManager.removeTwitterID()
                        }
                    }
                }.error { error in
                    SVProgressHUD.dismiss()
                    self.twitterSwitch.setOn(true, animated: true)
                }
            // 最後のIDだったら
            } else {
                ErrorProcess.detachError()
                twitterSwitch.setOn(true, animated: true)
            }
        }
    }
    
    // 端末には必ず一つのIDが紐付いていないといけない
    // 端末に紐付いているIDがその最後の一個であるかどうかを調べる関数
    private func isLastLocalID() -> Bool {
        let originalID = KeychainManager.getOriginalID()
        let facebookID = KeychainManager.getFacebookID()
        let twitterID = KeychainManager.getTwitterID()
        
        var idArray: [String] = []
        
        if originalID != nil {
            idArray.append(originalID!)
        }
        if facebookID != nil {
            idArray.append(facebookID!)
        }
        if twitterID != nil {
            idArray.append(twitterID!)
        }
        
        if idArray.count == 1 {
            return true
        }
        
        print("idArray = \(idArray)")
        
        return false
    }
    
    
}

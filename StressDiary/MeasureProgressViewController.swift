//
//  MeasureProgressView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/12.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit
import WatchConnectivity

class MeasureProgressViewController: UIViewController,QuestionnaireViewDelegate, WCSessionDelegate,MeasureProgressCircleDelegate {
    
    @IBOutlet weak var progressCircleView:MeasureProgressCircleView!
    private var last_select:Int = 0
    var questionnaireData:QuestionnaireData!
    @IBOutlet weak var questionnaireView:QuestionnaireView!
    
    var isBaseHeartRate: Bool = false
    var heartRate = 0
    var measureDate = NSDate()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(isBaseHeartRate: Bool) {
        super.init(nibName: nil, bundle: nil)
        
        self.isBaseHeartRate = isBaseHeartRate
    }
    
    func completeProgress() {
        print("ここでProgressが完全に終了したお知らせを受け取れます.")
        sendQuestionnaireAnswer()
        
        if isBaseHeartRate == false {
            transitionToMeasureResultVC(heartRate, measureDate: measureDate)
        } else if isBaseHeartRate == true {
            transitionToBaseHeartRateResultVC(self.heartRate)
        }
    }
    
    // MARK: -setup
    private func setupNavigationBar() {
        
        // NavigationBarの下にViewが埋まってしまうことを回避
        edgesForExtendedLayout = UIRectEdge.None;
        
        title = "測定中"
        
        let leftButton = UIBarButtonItem(title: "キャンセル", style: .Plain, target: self, action: "sendCancelMeasureEvent")
        leftButton.tintColor = UIColor.blackColor()
        navigationItem.leftBarButtonItem = leftButton
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        progressCircleView.delegate = self
        questionnaireView.delegate = self
        
        if (WCSession.isSupported()) {
            let session = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
        }
        
        questionnaireView.hidden = true
        if isBaseHeartRate == false {
            questionnaireView.hidden = false
            questionnaireView.backgroundColor = UIColor.whiteColor()
            Questionnaire.FindRandomOne().then { data in
                self.setQuestionnaireData(data)
            }
        }
    }
    func setQuestionnaireData(data :QuestionnaireData) {
        self.questionnaireData = data
        self.questionnaireView.setQuestionnaireData(data)
    }
    func selectAnswer(select: Int) {
        last_select = select
    }
    
    func sendQuestionnaireAnswer(){
        // 0のときは選択されていないので送信しない.
        if last_select != 0 {
            Questionnaire.Answer(questionnaireData.id, answerNo: last_select-1)
        }
    }
    
    // Watch側からの通知を受け取って画面遷移する
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        print("didReceiveMessage")
        print("message = \(message)")
        
        dispatch_async(dispatch_get_main_queue(), {
            if message["message"] as? String == "end" {
                self.navigationController?.popToRootViewControllerAnimated(true)
                return
            }
            
            let messageDict : NSDictionary = message
            let heartRate = messageDict.objectForKey("heartRate")?.intValue
            let measureDateString : String = messageDict.objectForKey("measureDate")! as! String
            let measureDate = getMeasureDate(measureDateString)
            
            print("heartRate = ",heartRate)
            print("measureDateString",measureDateString)
            
            
            self.heartRate = Int(heartRate!)
            self.measureDate = measureDate
            self.progressCircleView.completeMeasure()
        })
        
        replyHandler(["reply":"OK"])
    }
    
    private func transitionToMeasureResultVC(heartRate: Int, measureDate: NSDate) {
        print("transitionToMeasureResultVC")
        let measureResultViewController:MeasureResultViewController = MeasureResultViewController()
        measureResultViewController.setHeartRateResult(heartRate, date: measureDate)
        self.navigationController?.pushViewController(measureResultViewController, animated: true)
    }
    
    private func transitionToBaseHeartRateResultVC(heartRate: Int) {
        let baseHeartRateResultVC = BaseHeartRateResultViewController(heartRate: heartRate)
        navigationController?.pushViewController(baseHeartRateResultVC, animated: true)
    }
    
    func sendCancelMeasureEvent() {
        print("sendCancelMeasureEvent")
        if (WCSession.defaultSession().reachable) {
            let message = ["message" : "cancel"]
            WCSession.defaultSession().sendMessage(message,
                replyHandler: { (reply) -> Void in
                    print("reply = \(reply)")
                    dispatch_async(dispatch_get_main_queue(), {
                        self.cancelMeasure()
                    })
                },
                errorHandler: { (error) -> Void in
                    print("error = \(error)")
                }
            )
        }
    }
    
    func cancelMeasure() {
        navigationController?.popViewControllerAnimated(true)
    }
}
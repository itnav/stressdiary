//
//  UserInfo.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/11/06.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import SVProgressHUD

/* ログイン情報の実処理 */
struct UserInfo {
    static func register(authType: String, id: String) -> Promise<Bool> {
        SVProgressHUD.show()
        return Promise { fulfill, reject in
            let request = Alamofire.request(UserInfoRouter.Register(authType: authType, id: id))
            request.response { request, response, data, error in
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                
                if error != nil {
                    if error?.code == -1009 {
                        ErrorProcess.communicationError()
                        SVProgressHUD.dismiss()
                    }
                    return
                }
                
                if let data = data {
                    let dict = try! NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
                    
                    if let error = (dict["error"] as? NSDictionary) {
                        reject(NSError(domain: "アカウントエラー", code: error["code"] as! Int, userInfo: nil))
                        return
                    }
                    
                    fulfill(true)
                }
            }
        }
    }
    
    //    static func getSelf() {
    //        let request = Alamofire.request(UserInfoRouter.GetSelf(authType: KeychainManager.getCurrentAuthType()!, id: KeychainManager.getCurrentAuthID()!))
    //        request.response { request, response, data, error in
    //            print("UserInfo.getSelf")
    //            print(NSString(data: data!, encoding: NSUTF8StringEncoding))
    //        }
    //    }
    
    static func attachAuthInfo(authType: String, id: String) -> Promise<Bool> {
        
        print("")
        print("getCurrentAuthType = \(KeychainManager.getCurrentAuthType())")
        print("getCurrentAuthID = \(KeychainManager.getCurrentAuthID())")
        
        print("parameterType = \(authType)")
        print("parameterID = \(id)")
        print("")
        
        SVProgressHUD.show()
        return Promise { fulfill, reject in
            let request = Alamofire.request(UserInfoRouter.AttachAuthInfo(authType: authType, id: id))
            request.response { request, response, data, error in
                print("UserInfo.attachAuthInfo")
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                
                if let _ = error {
                    ErrorProcess.communicationError()
                    reject(CommunicationError.NoCommunicationLine)
                    return
                }
                
                if let data = data {
                    let dict = try! NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
                    
                    if let error = (dict["error"] as? NSDictionary) {
                        print(error["message"]!)
                        reject(NSError(domain: error["message"] as! String, code: error["code"] as! Int, userInfo: nil))
                        return
                    }
                    
                    fulfill(true)
                }
            }
        }
    }
    
    static func detachAuthInfo(authType: String, id: String) -> Promise<Bool> {
        
        print("")
        print("getCurrentAuthType = \(KeychainManager.getCurrentAuthType())")
        print("getCurrentAuthID = \(KeychainManager.getCurrentAuthID())")
        
        print("parameterType = \(authType)")
        print("parameterID = \(id)")
        print("")
        
        return Promise { fulfill, reject in
            let request = Alamofire.request(UserInfoRouter.DetachAuthInfo(authType: authType, id: id))
            request.response { request, response, data, error in
                print("UserInfo.detachAuthInfo")
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                
                if let _ = error {
                    ErrorProcess.communicationError()
                    reject(CommunicationError.NoCommunicationLine)
                    return
                }
                if let data = data {
                    let dict = try! NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
                    
                    if let error = (dict["error"] as? NSDictionary) {
                        reject(NSError(domain: "アカウントエラー", code: error["code"] as! Int, userInfo: nil))
                        return
                    }
                    
                    fulfill(true)
                }
            }
        }
    }
}



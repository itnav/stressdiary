//
//  NSDate+CustomDate.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/25.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation

extension NSDate {
    
    // 今日の日付情報を取得.
    func getDate(fromNumberOfDiffDay: Int)-> NSDate {
        
        let calendar = NSCalendar.currentCalendar()
        let components = NSDateComponents()
        components.year = NSDate().getYear()
        components.month = NSDate().getMonth()
        components.day = NSDate().getDay()+fromNumberOfDiffDay
        return calendar.dateFromComponents(components)!
        
    }
    
    static func getDate(year:Int,month:Int,day:Int)->NSDate {
        
        let calendar = NSCalendar.currentCalendar()
        let components = NSDateComponents()
        components.year = year
        components.month = month
        components.day = day
//        components.timeZone = NSTimeZone(abbreviation: "JST")
        
        return calendar.dateFromComponents(components)!
    }

    func getMonth()->Int{
        let format:NSDateFormatter = NSDateFormatter()
//        format.timeZone = NSTimeZone(abbreviation: "JST")
        format.dateFormat = "M"
        
        let day = format.stringFromDate(self)
        return Int(day)!
    }
    func getYear()->Int{
        let format:NSDateFormatter = NSDateFormatter()
//        format.timeZone = NSTimeZone(abbreviation: "JST")
        format.dateFormat = "y"
        
        let year = format.stringFromDate(self)
        return Int(year)!
    }
    func getDay()->Int{
        let format:NSDateFormatter = NSDateFormatter()
//        format.timeZone = NSTimeZone(abbreviation: "JST")
        format.dateFormat = "d"
        
        let day = format.stringFromDate(self)
        return Int(day)!
    }
    
    // year年month月の最終日を取得.
    static func getLastDayOfMonth(year:Int,month:Int)->Int{
        let format:NSDateFormatter = NSDateFormatter()
//        format.timeZone = NSTimeZone(abbreviation: "JST")
        format.dateFormat = "yyyy/M"
        return (NSCalendar.currentCalendar().rangeOfUnit(NSCalendarUnit.Day, inUnit: NSCalendarUnit.Month, forDate: format.dateFromString("\(year)/\(month)")!).toRange()?.maxElement())!
    }
    
    // 指定された月の最初の曜日を取得.
    static func getMonthStartIndex(year:Int,month:Int)->Int{
        
        let format:NSDateFormatter = NSDateFormatter()
//        format.timeZone = NSTimeZone(abbreviation: "JST")
        format.dateFormat = "yyyy/M/d"
        return NSCalendar.currentCalendar().ordinalityOfUnit(NSCalendarUnit.Day, inUnit: NSCalendarUnit.WeekOfMonth, forDate: format.dateFromString("\(year)/\(month)/1")!)-1
    }
    
    
    
}
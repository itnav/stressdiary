//
//  SettingListView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/11.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

protocol SettingListViewDelegate:class {
    func pushSettingViewController(viewController: UIViewController)
    func loadWebViewRequest(url:String, title: String, fileName:String)
    func setProfileDataRequest(data:UserProfileData)
    func completeFirstSetting()
}

class SettingListView: UIView,UITableViewDelegate,UITableViewDataSource,SettingDatePickerViewDekegate,SettingPickerViewDelegate{
    
    private let xibName:String = "SettingListView"
    
    @IBOutlet weak var settingList:UITableView!
    @IBOutlet weak var contentView:UIView!
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var nextButton:UIButton!
    @IBOutlet weak var verLabel:UILabel!
    
    private var sectionKey = ["プロフィール","リマインダー/通知","連携","タグ","リンク"]
    
    private var sectionList:NSMutableDictionary = NSMutableDictionary()
    
    private var settingItem:NSDictionary!
    
    private var profile:UserProfileData!
    
    var delegate:SettingListViewDelegate?
    
    internal var showOnlyProfileFlag = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupXib()
        setupData()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // データが取れなかった時の為に初期化.
        self.profile = UserProfileData()
        setupXib()
        setupData()
    }
    
    // SettingListViewは用意したjsonからcellを生成している
    private func setupData(){
        
        // profileのデータをサーバーから取得.
        
        UserProfile.getUserProfile().then { data in
            self.profile = UserProfileData(data: data)
        }
        
        let filePath:String = NSBundle.mainBundle().pathForResource("SettingListItem", ofType: "json")!
        var jsondata:NSString?
        
        do {
            jsondata = try NSString(contentsOfFile: filePath, encoding: NSUTF8StringEncoding)
        } catch {
            jsondata = nil
        }
        
        do {
            settingItem = try NSJSONSerialization.JSONObjectWithData(jsondata!.dataUsingEncoding(NSUTF8StringEncoding)!, options:NSJSONReadingOptions.AllowFragments) as! NSDictionary
        }catch {
            settingItem = NSDictionary()
        }
        
        settingList.reloadData()
        
    }
    /*
    * xibのレイアウトファイルの読み込み.
    */
    private func setupXib(){
        if let view:UIView = NSBundle.mainBundle().loadNibNamed(xibName, owner:self, options: nil).first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
        }
        settingList.separatorColor = UIColor.clearColor()
        
        settingList.delegate = self
        settingList.dataSource = self
        settingList.scrollEnabled = false
        
        settingList.registerNib(UINib(nibName: "SettingListCellView", bundle:nil), forCellReuseIdentifier: "SettingListCellView")
    }
    @IBAction func completeFirstSetting(){
        // 一応最新のデータを保存しておく.
        delegate?.setProfileDataRequest(profile)
        delegate?.completeFirstSetting()
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = UIColor.lightOrangeColor()
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        if showOnlyProfileFlag {
            nextButton.hidden = false
            verLabel.hidden = true
            return 1
        }else {
            return settingItem.allKeys.count
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let item = settingItem.objectForKey(sectionKey[section]) as! NSArray
        return item.count
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionKey[section]
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:SettingListCellView = SettingListCellView(style: UITableViewCellStyle.Default, reuseIdentifier: "SettingListCellView")
        
        let item = settingItem.objectForKey(sectionKey[indexPath.section])! as! NSArray
        
        cell.settingTitleLabel?.text = item[indexPath.row].objectForKey("title")! as? String
        if item[indexPath.row].objectForKey("detail_label")! as! String == "true" {
            
            if item[indexPath.row].objectForKey("type")! as! String == "profile" {
                switch item[indexPath.row].objectForKey("key")! as! String {
                case "birthday":
                    cell.settingDetailLabel.text = getShowSettingDateFormat(profile.birthday) as String!
                case "gender":
                    cell.settingDetailLabel.text = getGenderType(profile.gender).getGenderLabel()
                case "weightKg":
                    cell.settingDetailLabel.text = "\(profile.weightKg)kg"
                case "heightCm":
                    cell.settingDetailLabel.text = "\(profile.heightCm)cm"
                case "prefectureCode":
                    cell.settingDetailLabel.text = prefectureCodeList[Int(profile.prefectureCode)!]
                default:
                    cell.settingDetailLabel.text = ""
                }
            } else {
                switch item[indexPath.row].objectForKey("title")! as! String {
                case "頻度":
                    cell.settingDetailLabel.text = getWeekdayStringFromUserDefaults()
                    break
                case "時間":
                    let hour = UserDefaultsManager.getPeriodicNotificationTimeHour()
                    let minutes = UserDefaultsManager.getPeriodicNotificationTimeMinutes()
                    var minutesString = String()
                    if (minutes == 0) {
                        minutesString = String(format:"00")
                    }else {
                        minutesString = String(format:"%d", minutes)
                    }
                    let periodicNotificationTime = String(format:"%d:%@", hour, minutesString)
                    cell.settingDetailLabel.text = periodicNotificationTime
                    break
                default:
                    break
                }
            }
        }else {
            cell.settingDetailLabel?.text = ""
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        func floorInFirstDecimal(num: Double) -> Double {
            let decimal = num * 10
            return floor(decimal) / 10
        }
        
        let item = settingItem.objectForKey(sectionKey[indexPath.section])! as! NSArray
        let pickerRect:CGRect!
        
        if showOnlyProfileFlag {
            pickerRect = CGRectMake(0, frame.height-300, frame.width, 300)
        }else {
            pickerRect = CGRectMake(0, frame.height-300, frame.width, 200)
        }
        
        // もしpickerが表示されていたらそれを消す.
        for view in self.subviews {
            if let v = view as? SettingDatePickerView {
                v.removeFromSuperview()
            }else if let v = view as? SettingPickerView {
                v.doneSelect()
                v.removeFromSuperview()
            }
        }
        
        switch item[indexPath.row].objectForKey("type")! as! String{
        case "profile":
            if item[indexPath.row].objectForKey("title")! as! String == "生年月日" {
                let picker = SettingDatePickerView(frame: pickerRect)
                picker.pickerView.date = profile.birthday
                picker.delegate = self
                self.addSubview(picker)
            }else {
                var picker:SettingPickerView!
                switch item[indexPath.row].objectForKey("title")! as! String{
                case "性別":
                    picker = SettingPickerView(type:PickerType.gender, frame: pickerRect)
                    picker.setDefaultValue(getGenderType(profile.gender).getGenderLabel(), component: 0)
                case "体重":
                    picker = SettingPickerView(type:PickerType.weight, frame: pickerRect)
                    picker.setDefaultValue("\(Int(profile.weightKg))", component: 0)
                    picker.setDefaultValue("\(Int(round((profile.weightKg-floor(profile.weightKg))*10)))", component: 1)
                case "身長":
                    picker = SettingPickerView(type:PickerType.height, frame: pickerRect)
                    picker.setDefaultValue("\(Int(profile.heightCm))", component: 0)
                    picker.setDefaultValue("\(Int(round((profile.heightCm-floor(profile.heightCm))*10)))", component: 1)
                case "地域":
                    picker = SettingPickerView(type:PickerType.area, frame: pickerRect)
                    picker.setDefaultValue(prefectureCodeList[Int(profile.prefectureCode)!], component: 0)
                default:
                    break
                }
                picker.delegate = self
                self.addSubview(picker)
            }
            break
            
        case "picker":
            var picker:SettingPickerView!
            switch item[indexPath.row].objectForKey("title")! as! String {
            case "頻度":
                picker = SettingPickerView(type: PickerType.frequency, frame: pickerRect)
                picker.setDefaultValue("毎日", component: 0)
                break
            case "時間":
                picker = SettingPickerView(type: PickerType.time, frame: pickerRect)
                picker.setDefaultValue("0", component: 0)
                picker.setDefaultValue("00", component: 1)
                break
            default:
                break
            }
            
            picker.delegate = self
            self.addSubview(picker)
            
        case "web":
            let url = (item[indexPath.row].objectForKey("url")! as? String)!
            let title = (item[indexPath.row].objectForKey("title")! as? String)!
            let fileName = (item[indexPath.row].objectForKey("fileName")! as? String)!
            delegate?.loadWebViewRequest(url, title: title, fileName: fileName)
            break
        case "view":
            var vc = UIViewController()
            
            if (item[indexPath.row].objectForKey("url")! as? String)! == "SNSViewController" {
                vc = SNSViewController()
            }else if (item[indexPath.row].objectForKey("url")! as? String)! == "SelectTagConditionViewController" {
                vc = SelectTagConditionViewController()
            }else if (item[indexPath.row].objectForKey("url")! as? String)! == "HelpSettingHealthKitViewController" {
                vc = HelpSettingHealthKitViewController()
            }else if (item[indexPath.row].objectForKey("url")! as? String)! == "MeasureViewController" {
                vc = MeasureViewController(isBaseHeartRate: true)
            }
            delegate?.pushSettingViewController(vc)
            break
        default:
            break
        }
        
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        //200はtable下のテキストと、空ボックスの大きさの合計.
        scrollView.addConstraint( NSLayoutConstraint(item: contentView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: NSLayoutAttribute.Height, multiplier: 1.0, constant: settingList.contentSize.height+200))
        
    }
    func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    }
    func selectNewDate(date: NSDate) {
        
        profile.birthday = date
        delegate?.setProfileDataRequest(profile)
        settingList.reloadData()
    }
    func selectNewPropaty(type: PickerType, data: AnyObject) {
        
        // 選択した項目をAPIに更新.
        // 成功したら、テーブルの表示を変える.
        switch type {
        case .gender:
            profile.gender = getGenderType(data as! String).getGenderKey()
            delegate?.setProfileDataRequest(profile)
            settingList.reloadData()
            break
        case .height:
            profile.heightCm = Double(data as! String)
            delegate?.setProfileDataRequest(profile)
            settingList.reloadData()
            break
        case .weight:
            profile.weightKg = Double(data as! String)
            delegate?.setProfileDataRequest(profile)
            settingList.reloadData()
            break
        case .area:
            profile.prefectureCode = getPrefectureCode(data as! String)
            delegate?.setProfileDataRequest(profile)
            settingList.reloadData()
            break
        case .frequency:
            let week = data as! String
            if week == "毎日" {
                UserDefaultsManager.setPeriodicNotificationWeekday(nil)
            } else if week == "月曜" {
                UserDefaultsManager.setPeriodicNotificationWeekday(2)
            } else if week == "火曜" {
                UserDefaultsManager.setPeriodicNotificationWeekday(3)
            } else if week == "水曜" {
                UserDefaultsManager.setPeriodicNotificationWeekday(4)
            } else if week == "木曜" {
                UserDefaultsManager.setPeriodicNotificationWeekday(5)
            } else if week == "金曜" {
                UserDefaultsManager.setPeriodicNotificationWeekday(6)
            } else if week == "土曜" {
                UserDefaultsManager.setPeriodicNotificationWeekday(7)
            } else if week == "日曜" {
                UserDefaultsManager.setPeriodicNotificationWeekday(1)
            }
            
            PeriodicNotification.registerPeriodicNotification()
            settingList.reloadData()
            break
        case .time:
            let timeArray: [String] = data as! [String]
            UserDefaultsManager.setPeriodicNotificationTime(Int(timeArray[0])!, minutes: Int(timeArray[1])!)
            PeriodicNotification.registerPeriodicNotification()
            settingList.reloadData()
            break
            
        }
    }
    func getWeekdayStringFromUserDefaults() -> String {
        let weekdayValue : Int? = UserDefaultsManager.getPeriodicNotificationWeekday()
        var weekdayString = String()
        switch weekdayValue {
        case nil:
            weekdayString = "毎日"
            break
        case 2?:
            weekdayString = "月曜"
            break
        case 3?:
            weekdayString = "火曜"
            break
        case 4?:
            weekdayString = "水曜"
            break
        case 5?:
            weekdayString = "木曜"
            break
        case 6?:
            weekdayString = "金曜"
            break
        case 7?:
            weekdayString = "土曜"
            break
        case 1?:
            weekdayString = "日曜"
            break
        default:
            break
        }
        return weekdayString
    }
}
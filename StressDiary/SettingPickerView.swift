//
//  SettingPickerView.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/19.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation
import UIKit

protocol SettingPickerViewDelegate:class {
    func selectNewPropaty(type:PickerType,data:AnyObject)
}
enum PickerType {
    case gender
    case weight
    case height
    case area
    
    case frequency
    case time
    
    /**/
     
     /*
     * 設定の内容ごとに表示するデータの内容を取得.
     */
    func getDataSourse()->[AnyObject]{
        switch self {
        case .gender:
            return [["男性","女性","不明"]]
            
        case .weight:
            var component1:[AnyObject] = []
            var component2:[AnyObject] = []
            for var i:Int=0; i<=200; i++ { component1.append("\(i)") }
            for var j:Int=0; j<10; j++ { component2.append("\(j)") }
            
            return [component1,component2]
            
        case .height:
            var component1:[AnyObject] = []
            var component2:[AnyObject] = []
            for var i:Int=0; i<=200; i++ { component1.append("\(i)") }
            for var j:Int=0; j<10; j++ { component2.append("\(j)") }
            
            return [component1,component2]
            
        case .area:
            return [prefectureCodeList]
            
        case .frequency:
            return [["毎日", "月曜", "火曜", "水曜", "木曜", "金曜", "土曜", "日曜"]]
            
        case .time:
            var hourComponent: [AnyObject] = []
            let minutesComponent: [AnyObject] = ["00", "30"]
            
            for var i = 0; i < 24; i++ {
                hourComponent.append("\(i)")
            }
            
            return [hourComponent, minutesComponent]
        }
    }
}
class SettingPickerView: UIView,UIPickerViewDataSource,UIPickerViewDelegate {
    

    private let xibName:String = "SettingPickerView"
    
    @IBOutlet weak var doneButton:UIButton!
    @IBOutlet weak var pickerView:UIPickerView!
    @IBOutlet weak var cancelButton:UIButton!
    
    var delegate:SettingPickerViewDelegate?
    
    var selectData:[AnyObject] = []
    
    // 最新の選択.
    var latestSelect:NSMutableArray = NSMutableArray()
    
    var type:PickerType!
    
    // 初期入力値
    var defaultInput:AnyObject!
    
    var previousValue:AnyObject!
    
    var firstValue:NSMutableArray = NSMutableArray()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(type:PickerType,frame:CGRect){
        super.init(frame: frame)
        self.backgroundColor = UIColor.whiteColor()
        
        self.type = type
        selectData = type.getDataSourse()
        setupXib()
    }
    
    internal func setDefaultValue(value:AnyObject,component:Int){
        latestSelect[component] = value
        previousValue = value
        firstValue[component] = value
        pickerView.selectRow(selectData[component].indexOfObject(value), inComponent: component, animated: false)
    }
    
    @IBAction func doneSelect(){     
        for obj in selectData.enumerate() {
            latestSelect[obj.index] = selectData[obj.index][pickerView.selectedRowInComponent(obj.index)]
        }
        
        setSelectData()
        
        self.removeFromSuperview()
    }
    
    func setSelectData(){
        // Componentが二つあるもの
        if selectData.count >= 2 && (type == .weight || type == .height) {
            delegate?.selectNewPropaty(type, data: "\(latestSelect[0]).\(latestSelect[1])")
        } else if selectData.count >= 2 && type == .time {
            
            // 00でのやりとりをすると送り先のIntキャストで失敗するので00を0に置換
            if let toZero = latestSelect[1] as? String {
                if toZero == "00" {
                    latestSelect[1] = "0"
                }
            }
            delegate?.selectNewPropaty(type, data: [latestSelect[0], latestSelect[1]])
            print("\(latestSelect[0]) + \(latestSelect[1])")
        } else { // Componentが一つのもの
            delegate?.selectNewPropaty(type, data: latestSelect[0])
        }
    }
    
    @IBAction func cencel(){
        latestSelect = firstValue
        setSelectData()
        self.removeFromSuperview()
    }
    /*
    * xibのレイアウトファイルの読み込み.
    */
    private func setupXib(){
        if let view:UIView = NSBundle.mainBundle().loadNibNamed(xibName, owner:self, options: nil).first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
        }
        pickerView.delegate = self
        pickerView.dataSource = self
        
        // もしも身長か体重のデータの時には、小数点を打つ.
        if type == PickerType.height || type == PickerType.weight {
            let label:UILabel = UILabel(frame:CGRectMake(0,0,10,10))
            label.text = "."
            label.sizeToFit()
            label.center = CGPointMake(UIScreen.mainScreen().bounds.width/2,self.pickerView.frame.height/2)
            pickerView.addSubview(label)
        }

        if type == PickerType.time {
            let label:UILabel = UILabel(frame:CGRectMake(0,0,10,10))
            label.text = ":"
            label.sizeToFit()
            label.center = CGPointMake(UIScreen.mainScreen().bounds.width/2,self.pickerView.frame.height/2)
            pickerView.addSubview(label)
        }
    }

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return selectData.count
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return selectData[component].count
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        let label:UILabel = UILabel(frame: CGRectMake(0,0,pickerView.rowSizeForComponent(component).width*0.93,pickerView.rowSizeForComponent(component).height))
        label.text = "\((selectData as! [[AnyObject]])[component][row])"

        if selectData.count == 1 {
            label.textAlignment = NSTextAlignment.Center
        }else if component == 0 {
            label.textAlignment = NSTextAlignment.Right
        }else {
            label.textAlignment = NSTextAlignment.Left
        }
        
        return label
        
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        latestSelect[component] = selectData[component][row]
        print("didSelectRow")
        print(latestSelect[component])
        setSelectData()
    }
}



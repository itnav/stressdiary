//
//  BaseHeartRateResultViewController.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/12/02.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit

class BaseHeartRateResultViewController: UIViewController {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var heartRateLabel: UILabel!
    
    var heartRate: Int = 0
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(heartRate: Int) {
        super.init(nibName: nil, bundle: nil)
        
        self.heartRate = heartRate
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationBar()
        
        heartRateLabel.attributedText = NSAttributedString.decoratedLightGrayAttributedString(Float(heartRate))
        
        navigationItem.setHidesBackButton(true, animated: false)
        
        let saveButton = UIBarButtonItem(title: "保存", style: .Plain, target: self, action: "saveMeasureResult")
        saveButton.tintColor = UIColor.blackColor()
        navigationItem.rightBarButtonItem = saveButton
        let reMeasureButton = UIBarButtonItem(title: "測り直す", style: .Plain, target: self, action: "reMeasure")
        reMeasureButton.tintColor = UIColor.blackColor()
        navigationItem.leftBarButtonItem = reMeasureButton
    }
    
    private func setupNavigationBar() {
        
        // NavigationBarの下にViewが埋まってしまうことを回避
        edgesForExtendedLayout = UIRectEdge.None;
        
        title = "測定結果"
    }
    
    func saveMeasureResult() {
        BaseHeartRateManager.setBaseHeartRate(heartRate)
        transitionToFirstProfileInputVC()
        print("HeartRate = \(BaseHeartRateManager.getBaseHeartRate())")
    }
    
    func reMeasure() {
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
    private func transitionToFirstProfileInputVC() {
        let settingVC = SettingViewController(firstSet: true)
        
        navigationController?.pushViewController(settingVC, animated: true)
    }
    
}

//
//  EditTagViewController.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/11.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit

/* 不使用のファイル */
class EditTagViewController: UIViewController {
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.view.backgroundColor = UIColor.whiteColor()
    }
    
    private func setupNavigationBar() {
        // NavigationBarの下にViewが埋まってしまうことを回避
        edgesForExtendedLayout = UIRectEdge.None;
        title = "タグ編集"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        
        let editTagView:EditTagView = EditTagView(frame: view.frame)
        self.view.addSubview(editTagView)
        
    }
    
}

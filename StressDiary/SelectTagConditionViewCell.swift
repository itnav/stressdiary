//
//  SelectTagConditionViewCell.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/20.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation

import Foundation
import UIKit

protocol SelectTagConditionCellViewDelegate:class{
    func selectDeleteTag(deleteTag:String)
}

class SelectTagConditionCellView: UICollectionViewCell {
    
    @IBOutlet weak var dateLabel:UILabel!
    @IBOutlet weak var bgView:UIView!
    @IBOutlet weak var deleteButton:UIButton!
    
    var delegate:SelectTagConditionCellViewDelegate?
    var mode:Bool = true
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func awakeFromNib() {
        
        bgView.backgroundColor = UIColor.lightGrayColor()
        bgView.layer.cornerRadius = 25.0
        dateLabel.textColor = UIColor.whiteColor()
        dateLabel.numberOfLines = 0
        
        deleteButton.layer.cornerRadius = deleteButton.bounds.width/2
        
        // 基準の位置に戻す.
        self.bgView.layer.position.x = self.bgView.bounds.width/2
    }
    
    func selectCell(){
        bgView.backgroundColor = UIColor.strongOrangeColor()
    }
    
    func deSelectCell(){
        bgView.backgroundColor = UIColor.lightGrayColor()
    }
    func setEditMode(mode:Bool,anim:Bool){
        // 一度初期の場所に戻しておく
        if mode {
            self.bgView.layer.position.x = self.bgView.bounds.width/2
        }
        
        if anim {
            UIView.animateWithDuration(0.5, // アニメーションの時間
                animations: {() -> Void  in
                    // アニメーションする処理
                    if mode {
                        self.bgView.transform = CGAffineTransformMakeTranslation(self.deleteButton.bounds.width+5, 0)
                            
                            //(self.bgView.bounds.width/2)+self.deleteButton.bounds.width+5
                    }else {
                        self.bgView.transform = CGAffineTransformIdentity
                    }
            })
        }else {
            // 横に移動.
            if mode {
                self.bgView.transform = CGAffineTransformMakeTranslation(self.deleteButton.bounds.width+5, 0)
            }else {
                self.bgView.transform = CGAffineTransformIdentity
            }
        }
    }
    
    @IBAction func deleteTag(){
        delegate?.selectDeleteTag(self.dateLabel.text!)
    }
    
}

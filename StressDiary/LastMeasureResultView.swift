//
//  LastMeasureResultView.swift
//  StressDiary
//
//  Created by 田中賢治 on 2015/10/28.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import UIKit

class LastMeasureResultView: UIView {
    
    let xibName:String = "LastMeasureResultView"
    
    @IBOutlet weak var measureDateLabel:UILabel!
    @IBOutlet weak var heartRateLabel:UILabel!
    @IBOutlet weak var stressIndexLabel:UILabel!
    
    @IBOutlet weak var stressMeterView:StressMeterView!
    
    @IBOutlet weak var memoLabel: UILabel!
    
    var heartRateData:HeartbeatLogData = HeartbeatLogData()
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupXib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupXib()
    }
    
    /*
    * xibのレイアウトファイルの読み込み.
    */
    private func setupXib(){
//        print("setupXib")

        if let view:UIView = NSBundle.mainBundle().loadNibNamed(xibName, owner:self, options: nil).first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
        }
    }
    override func awakeFromNib() {
        stressMeterView.setMeterValue(heartRateData.stressIndex, animation: true, duration: 3.0)
    }
    
    /*
    * 表示データの読み込み.
    */
    internal func loadData(data:HeartbeatLogData){
        
        heartRateData = data
        
        // 表示する桁数が2桁以下だったら、最初の一文字は色を変える.
        if heartRateData.stressIndex > 100 {
            heartRateData.stressIndex = 100
        } else if heartRateData.stressIndex < 0 {
            heartRateData.stressIndex = 0
        }
        stressIndexLabel.attributedText = NSAttributedString.decoratedOrangeAttributedString(Float(heartRateData.stressIndex))
        
        heartRateLabel.attributedText = NSAttributedString.decoratedLightGrayAttributedString(Float(heartRateData.rateBpm))
        "yyyy/MM/dd HH:mm:ss"
        
        let dateformat:NSDateFormatter = NSDateFormatter()
        dateformat.locale = NSLocale(localeIdentifier: "en")
        dateformat.dateFormat = "MM/dd EEE HH:mm"
        measureDateLabel.text = "\(dateformat.stringFromDate(data.measureDatetime).uppercaseString)"
        
        // attributeStringの設定で外れてしまう文字装飾を設定.
        stressIndexLabel.font = UIFont.meterDisplayFont(28)
        
        // 機種に応じてフォントサイズを変更
        let device = DeviceInfo.iOSDevice()
        if device == "iPhone6/6s" {
            stressIndexLabel.font = UIFont.meterDisplayFont(34)
        } else if device == "iPhone6 Plus/6s Plus" {
            stressIndexLabel.font = UIFont.meterDisplayFont(40)
        }
        stressMeterView.setMeterValue(data.stressIndex, animation: true, duration: 3.0)
        
        AdviceMessage.FindByStressIndex(heartRateData.stressIndex).then({
            result in
            let array = result.message.componentsSeparatedByString("\n")
            self.memoLabel.text = array[0]
        })
    }
}

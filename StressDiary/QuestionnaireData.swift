//
//  QuestionnaireModel.swift
//  StressDiary
//
//  Created by WatanabeKimiko on 2015/11/18.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import Foundation

/* アンケートのデータモデル */
class QuestionnaireData {
    
    internal var id:Int!
    internal var question:String!
    internal var answers:NSArray!
 
    /*
    * データの初期化処理.
    */
    init(){
        self.id = 0
        self.question = ""
        self.answers = []
    }
    init(data:QuestionnaireData){
        self.id = data.id
        self.question = data.question
        self.answers = data.answers
    }
    internal func setQuestionnaireData(data:NSDictionary){
        self.id = data.objectForKey("id") as! Int
        self.question = data.objectForKey("question") as! String
        self.answers = data.objectForKey("answers") as! NSArray
    }
}
//
//  InterfaceController.swift
//  StressDiaryWatch Extension
//
//  Created by 田中賢治 on 2015/11/11.
//  Copyright © 2015年 田中賢治. All rights reserved.
//

import WatchKit
import Foundation
import HealthKit
import WatchConnectivity

class InterfaceController: WKInterfaceController, HKWorkoutSessionDelegate, WCSessionDelegate {
    
    @IBOutlet var heartIcon: WKInterfaceImage!
    @IBOutlet var heartBeatLabel: WKInterfaceLabel!
    @IBOutlet var startButton: WKInterfaceButton!
    @IBOutlet var noteLabel: WKInterfaceLabel!
    
    let healthStore = HKHealthStore()
    var workoutSession: HKWorkoutSession?
    var heartRateQuery: HKAnchoredObjectQuery?
    
    var isRunning = false
    var heartRate: Int?
    var measureDate: String?
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        if HKHealthStore.isHealthDataAvailable() != true {
            //            print("not available")
            
            return
        }
        
        let typesToRead = Set([HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!])
        
        // HealthKitへのアクセス許可をユーザーへ求める
        self.healthStore.requestAuthorizationToShareTypes(nil, readTypes: typesToRead, completion: { success, error in
            
        })
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        if (WCSession.isSupported()) {
            let session = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
        }
        
        let font = UIFont.systemFontOfSize(12)
        self.noteLabel.setAttributedText(NSAttributedString(string: "詳細なデータを記録する場合はiPhoneから計測を開始してください。", attributes: [NSFontAttributeName:font]))
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func btnTapped() {
        if self.isRunning == true {
            self.endMeasure()
        }else {
            checkCanReadHealthData()
        }
    }
    
    private func startMeasure() {
        // workoutSessionを設定
        if let _ = workoutSession {
            workoutSession?.delegate = nil
            self.healthStore.endWorkoutSession(self.workoutSession!)
        }
        self.workoutSession = HKWorkoutSession(activityType: HKWorkoutActivityType.Other, locationType: HKWorkoutSessionLocationType.Unknown)
        self.workoutSession!.delegate = self
        
        // workoutSessionを開始
        self.healthStore.startWorkoutSession(self.workoutSession!)
    }
    
    private func endMeasure() {
        // workoutSessionを終了
        self.healthStore.endWorkoutSession(self.workoutSession!)
        sendEndMeasureEvent()
        //        heartRate = nil
    }
    
    /* デリゲートメソッド */
    // workoutSessionの状態が変化した時に呼ばれる
    func workoutSession(workoutSession: HKWorkoutSession, didChangeToState toState: HKWorkoutSessionState, fromState: HKWorkoutSessionState, date: NSDate) {
        dispatch_async(dispatch_get_main_queue()) {
            switch toState {
            case .Running:
                print("workoutSession: .Running")
                self.startButton.setEnabled(true)
                
                let predicate = HKQuery.predicateForSamplesWithStartDate(date, endDate: nil, options: .None)
                let sampleType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!
                
                self.heartRateQuery = HKAnchoredObjectQuery(type: sampleType, predicate: predicate, anchor: nil, limit: 0, resultsHandler: { query, samples, deletedObjects, anchor, error in
                    
                    if (error != nil) {
                        
                        self.showErrorAlert()
                        return
                    }
                    
                })
                
                // アップデートハンドラーを設定
                // 心拍数情報が更新されると呼ばれる
                self.heartRateQuery!.updateHandler = { query, samples, deletedObjects, anchor, error in
                    let heartRateQuantities = samples as? [HKQuantitySample]
                    let endDate: NSDate? = samples?.first?.endDate
                    let heartRateUnit = HKUnit(fromString: "count/min")
                    var quantity = heartRateQuantities!.first?.quantity.doubleValueForUnit(heartRateUnit)
                    
                    //bps単位で数値が帰ってきた場合の処理
                    if (quantity < 10) {
                        quantity = quantity! * 60;
                    }
                    
                    print("quantity = ",quantity)
                    print("endDate = ",endDate)
                    
                    self.heartRate = Int(floor(quantity!))
                    self.heartBeatLabel.setText(String(self.heartRate!) + "bpm")
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    self.measureDate = dateFormatter.stringFromDate(endDate!)
                    
                    self.endMeasure()
                }
                
                self.healthStore.executeQuery(self.heartRateQuery!)
                
                self.heartIcon.setAlpha(1.0)
                self.heartBeatLabel.setText("計測中...")
                self.startButton.setTitle("STOP")
                self.isRunning = true
                
            case .Ended:
                print("workoutSession: .Ended")
                
                self.healthStore.stopQuery(self.heartRateQuery!)
                
                self.heartIcon.setAlpha(0.0)
                if self.heartRate == nil {
                    self.heartBeatLabel.setText("計測を開始できます")
                }
                self.startButton.setTitle("START")
                self.isRunning = false
                
            default:
                print("Unexpected workout session state \(toState)")
            }
        }
    }
    
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        print("didReceiveMessage")
        print("message = \(message)")
        
        // iPhone側からstartを指示されたとき
        if message["message"] as? String == "start" {
            // 次回計測時、キャンセルしたときに心拍値無しの終了が呼べるようにnilをセット
            heartRate = nil
            
            checkAuthorized({ authorized in
                if authorized == true {
                    self.startMeasure()
                    replyHandler(["reply":"StartMeasure"])
                } else {
                    replyHandler(["reply":"notAuthorized"])
                }
            })
        }
        
        // iPhone側からcancelを指示されたとき
        if message["message"] as? String == "cancel" {
            endMeasure()
            heartBeatLabel.setText("計測を開始できます")
            replyHandler(["reply":"EndMeasure"])
        }
    }
    
    //    private func sendNotAuthorizationEvent() {
    //        if (WCSession.defaultSession().reachable) {
    //            let message = ["message": "notAuthorized"]
    //
    //            WCSession.defaultSession().sendMessage(message,
    //                replyHandler: { (reply) -> Void in
    //                    print("reply = \(reply)")
    //                },
    //                errorHandler: { (error) -> Void in
    //                    print("error = \(error)")
    //                }
    //            )
    //        }
    //    }
    
    private func sendTransitionEvent() {
        print("sendTransitionEvent")
        if (WCSession.defaultSession().reachable) {
            let message = ["message": "transition"]
            
            WCSession.defaultSession().sendMessage(message,
                replyHandler: { (reply) -> Void in
                    print("reply = \(reply)")
                },
                errorHandler: { (error) -> Void in
                    print("error = \(error)")
                }
            )
        }
    }
    
    private func sendEndMeasureEvent() {
        print("---------------------")
        print("sendEndMeasureEvent")
        if (WCSession.defaultSession().reachable) {
            let message: [String: AnyObject]
            if let heartRate = self.heartRate, measureDate = self.measureDate {
                message = ["heartRate" : heartRate, "measureDate" : measureDate]
                
                print(message)
            } else {
                message = ["message" : "end"]
            }
            
            WCSession.defaultSession().sendMessage(message,
                replyHandler: { (reply) -> Void in
                    print("reply = \(reply)")
                },
                errorHandler: { (error) -> Void in
                    print("error = \(error)")
                }
            )
        }
    }
    
    // エラーが発生した時に呼ばれる
    func workoutSession(workoutSession: HKWorkoutSession, didFailWithError error: NSError) {
        print("ERROR!!! = \(error)")
        if error.code != 3 {
            showAlertAction("心拍数の取得に失敗しました", message: "心拍数の取得に失敗しました。再度測定を行ってください。")
        }
    }
    
    func checkAuthorized(completionHandler: (authorized: Bool) -> Void) {
        // ヘルスケアへのアクセス認証が降りているか、Queryを投げて確認する
        let sampleType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!
        self.heartRateQuery = HKAnchoredObjectQuery(type: sampleType, predicate: nil, anchor: nil, limit: 0, resultsHandler: { query, samples, deletedObjects, anchor, error in
            if let _ = error {
                completionHandler(authorized: false)
                return
            }
            completionHandler(authorized: true)
        })
        self.healthStore.executeQuery(self.heartRateQuery!)
    }
    
    //心拍数データの読み出しが許可されているか、読み出したデータの個数で判定する
    func checkCanReadHealthData() {
        startButton.setEnabled(false)
        let healthStore = HKHealthStore()
        
        let sampleType: HKQuantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!
        let timeSortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: true)
        
        let heartRateQuery = HKSampleQuery(sampleType: sampleType, predicate: nil, limit: 0, sortDescriptors: [timeSortDescriptor], resultsHandler: { query, results, error in
            if (error != nil) {
                print(error)
                self.showErrorAlert()
                self.startButton.setEnabled(true)
            }else {
                
                //                if (results?.count != 0) {
                //一つ以上ヘルスケアデータが返ってきたら計測開始判定処理を行う
                if self.isRunning == false {
                    self.checkAuthorized({ authorized in
                        if authorized == true {
                            self.startMeasure()
                        } else {
                            self.heartBeatLabel.setText("ヘルスケアへのアクセスを許可してください")
                        }
                    })
                }
                //                }else {
                //                    //ヘルスケアデータが返ってこない場合は、データが一つも無いか読み出し許可を切っている
                //                    self.showErrorAlert()
                //                }
                
            }
            
        })
        
        healthStore.executeQuery(heartRateQuery)
    }
    
    //ヘルスケア読み出し許可の注意表示用メソッド
    func showErrorAlert () {
        let title = "認証エラー"
        let message = "ヘルスケア内に心拍数データが存在しないか、ヘルスケアへのアクセスが許可されていません。許可されていない場合は「ヘルスケア」->「ソース」->「StressDiary」から「心拍数」をOnにしてください。読み出し許可の反映には時間がかかる場合があります。"
        self.showAlertAction(title, message: message)
    }
    
    //アラート表示用メソッド
    func showAlertAction (title:String, message:String) {
        let action = WKAlertAction(title: "了解", style: WKAlertActionStyle.Default, handler: { () -> Void in
            
        })
        self.presentAlertControllerWithTitle(title, message: message, preferredStyle: WKAlertControllerStyle.ActionSheet, actions: [action])
    }
}
